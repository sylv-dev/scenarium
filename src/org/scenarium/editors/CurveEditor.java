/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.editors;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.beanmanager.editors.PropertyEditor;
import org.scenarium.struct.curve.BoxMarker;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.curve.Curvei;
import org.scenarium.struct.curve.EvolvedCurveSeries;

public class CurveEditor<T> extends PropertyEditor<T> {
	@Override
	public String getAsText() {
		return null;
	}

	private static Curve getCurve(ByteBuffer bb) {
		String name = getString(bb);
		int type = bb.getInt();
		int nbData = bb.getInt();
		if (type == 0) {
			int[][] data = new int[2][nbData];
			int[] x = data[0];
			int[] y = data[1];
			for (int i = 0; i < nbData; i++)
				x[i] = bb.getInt();
			for (int i = 0; i < nbData; i++)
				y[i] = bb.getInt();
			int valuesSize = bb.getInt();
			int[] values = null;
			if (valuesSize != -1) {
				values = new int[valuesSize];
				for (int i = 0; i < valuesSize; i++)
					values[i] = bb.getInt();
			}
			return new Curvei(data, values, name.length() == 0 ? null : name);
		}
		double[][] data = new double[2][nbData];
		double[] x = data[0];
		double[] y = data[1];
		for (int i = 0; i < nbData; i++)
			x[i] = bb.getDouble();
		for (int i = 0; i < nbData; i++)
			y[i] = bb.getDouble();
		int valuesSize = bb.getInt();
		double[] values = null;
		if (valuesSize != -1) {
			values = new double[valuesSize];
			for (int i = 0; i < valuesSize; i++)
				values[i] = bb.getDouble();
		}
		return new Curved(data, values, name.length() == 0 ? null : name);
	}

	private static ArrayList<Curve> getCurves(ByteBuffer bb) {
		ArrayList<Curve> curveList = new ArrayList<>();
		while (bb.remaining() != 0)
			curveList.add(getCurve(bb));
		return curveList;
	}

	private static int getCurveSize(Curve curve) {
		if (curve instanceof Curvei)
			return curve.getName().getBytes().length + Integer.BYTES * 4 + Integer.BYTES * ((Curvei) curve).getData()[0].length * (((Curvei) curve).getValues() == null ? 2 : 3);
		return curve.getName().getBytes().length + Integer.BYTES * 4 + Double.BYTES * ((Curved) curve).getData()[0].length * (((Curved) curve).getValues() == null ? 2 : 3);
	}

	private static String getString(ByteBuffer bb) {
		byte[] stringBytes = new byte[bb.getInt()];
		bb.get(stringBytes);
		return new String(stringBytes);
	}

	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	private static void putCurve(ByteBuffer buf, Curve curve) {
		if (curve instanceof Curvei) {
			int[][] data = ((Curvei) curve).getData();
			int[] x = data[0];
			int[] y = data[1];
			int[] values = ((Curvei) curve).getValues();
			putString(buf, curve.getName() == null ? "" : curve.getName());
			buf.putInt(0);
			buf.putInt(x.length);
			for (int j : x)
				buf.putInt(j);
			for (int j : y)
				buf.putInt(j);
			if (values != null) {
				buf.putInt(values.length);
				for (int j : values)
					buf.putInt(j);
			} else
				buf.putInt(-1);
		} else {
			double[][] data = ((Curved) curve).getData();
			double[] x = data[0];
			double[] y = data[1];
			double[] values = ((Curved) curve).getValues();
			putString(buf, curve.getName());
			buf.putInt(1);
			buf.putInt(x.length);
			for (double j : x)
				buf.putDouble(j);
			for (double j : y)
				buf.putDouble(j);
			if (values != null) {
				buf.putInt(values.length);
				for (double j : values)
					buf.putDouble(j);
			} else
				buf.putInt(-1);
		}
	}

	private static void putString(ByteBuffer buf, String value) {
		byte[] nameb = value.getBytes();
		buf.putInt(nameb.length);
		buf.put(nameb);
	}

	public Object readCurveValue(DataInput raf) throws IOException {
		int type = raf.readInt();
		// ByteBuffer bb = ByteBuffer.allocate(raf.readInt());
		byte[] data = new byte[raf.readInt()];
		raf.readFully(data);
		ByteBuffer bb = ByteBuffer.wrap(data);
		if (type != 0) { // EvolvedCurveSeries
			if (type == 2) {
				int nbBoxMarker = bb.getInt();
				ArrayList<BoxMarker> bml = null;
				if (nbBoxMarker != -1) {
					bml = new ArrayList<>();
					for (int i = 0; i < nbBoxMarker; i++)
						bml.add(new BoxMarker(bb.getDouble(), bb.getDouble(), bb.getDouble(), bb.getDouble(), getString(bb)));
				}
				return new EvolvedCurveSeries(bml, getString(bb), getString(bb), getString(bb), bb.getDouble(), bb.getDouble(), getCurves(bb));
			}
			return new CurveSeries(getCurves(bb));
		}
		return getCurve(bb);
	}

	@Override
	public void setAsText(String text) {}

	@Override
	public void writeValue(DataOutput raf, T value) throws IOException {
		// FileChannel fc = raf.getChannel();
		ByteBuffer buf;
		if (value instanceof CurveSeries) {
			CurveSeries curveSeries = (CurveSeries) value;
			int buffSize = 0;
			for (Curve curve : curveSeries.getCurves())
				buffSize += getCurveSize(curve);
			if (value instanceof EvolvedCurveSeries) {
				EvolvedCurveSeries evolvedCurveSeries = (EvolvedCurveSeries) value;
				ArrayList<BoxMarker> bm = evolvedCurveSeries.getBoxmarkers();
				buffSize += Integer.BYTES;
				if (bm != null)
					for (BoxMarker boxMarker : bm)
						buffSize += 2 * 2 * Double.BYTES + boxMarker.name.getBytes().length + Integer.BYTES;
				buffSize += evolvedCurveSeries.getName().getBytes().length + Integer.BYTES;
				buffSize += evolvedCurveSeries.getxLabel().getBytes().length + Integer.BYTES;
				buffSize += evolvedCurveSeries.getyLabel().getBytes().length + Integer.BYTES;
				buffSize += Double.BYTES * 2;
				buf = ByteBuffer.allocate(buffSize + Integer.BYTES * 2);// fc.map(FileChannel.MapMode.READ_WRITE, 0, buffSize);
				buf.putInt(2);
				buf.putInt(buffSize);
				if (bm != null) {
					buf.putInt(bm.size());
					for (BoxMarker boxMarker : bm) {
						buf.putDouble(boxMarker.x);
						buf.putDouble(boxMarker.y);
						buf.putDouble(boxMarker.width);
						buf.putDouble(boxMarker.height);
						putString(buf, boxMarker.name);
					}
				} else
					buf.putInt(-1);
				putString(buf, evolvedCurveSeries.getName());
				putString(buf, evolvedCurveSeries.getxLabel());
				putString(buf, evolvedCurveSeries.getyLabel());
				buf.putDouble(evolvedCurveSeries.getBegin());
				buf.putDouble(evolvedCurveSeries.getEnd());
			} else {
				buf = ByteBuffer.allocate(buffSize + Integer.BYTES * 2);// fc.map(FileChannel.MapMode.READ_WRITE, 0, buffSize);
				buf.putInt(1);
				buf.putInt(buffSize);
			}
			for (Curve curve : curveSeries.getCurves())
				putCurve(buf, curve);
		} else {
			int buffSize = getCurveSize((Curvei) value);
			buf = ByteBuffer.allocate(buffSize + Integer.BYTES * 2);
			buf.putInt(0);
			buf.putInt(buffSize);
			putCurve(buf, (Curvei) value);
		}
		raf.write(buf.array());
	}
}
