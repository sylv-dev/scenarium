/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.editors;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.beanmanager.BeanDesc;
import org.scenarium.display.toolBar.Operators;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;

class ComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {
	private static final long serialVersionUID = 1L;
	private static final ImageIcon OPERATORINSTICON = new ImageIcon(Operators.class.getResource("/Operator.gif"));
	private static final ImageIcon BEANICON = new ImageIcon(Operators.class.getResource("/bean.gif"));

	public ComboBoxRenderer() {
		setOpaque(true);
	}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
		setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
		setIcon(value instanceof BeanDesc ? OperatorManager.isOperator(((BeanDesc<?>) value).bean) ? OPERATORINSTICON : BEANICON : null);
		setText(value instanceof BeanDesc ? value.toString() : " " + value.toString());
		return this;
	}
}
