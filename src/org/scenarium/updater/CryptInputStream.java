/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.updater;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CryptInputStream extends FilterInputStream {
	private final InputStream input;
	private final byte[] ibuffer = new byte[512];
	private boolean done = false;
	private byte[] obuffer;
	private int ostart = 0;
	private int ofinish = 0;
	private final Encryptor encryptor;

	public CryptInputStream(InputStream is, EncryptionAlgorithm encryptionAlgorithm) {
		super(is);
		this.input = is;
		this.encryptor = new Encryptor(encryptionAlgorithm);
	}

	@Override
	public int available() throws IOException {
		return this.ofinish - this.ostart;
	};

	@Override
	public void close() throws IOException {
		this.input.close();
		this.encryptor.doFinal(null, 0, 0);
		this.ostart = 0;
		this.ofinish = 0;
	}

	private int getMoreData() throws IOException {
		if (this.done)
			return -1;
		int readin = this.input.read(this.ibuffer);
		if (readin == -1) {
			this.done = true;
			this.obuffer = this.encryptor.doFinal(null, 0, 0);
			if (this.obuffer == null)
				return -1;
			this.ostart = 0;
			this.ofinish = this.obuffer.length;
			return this.ofinish;
		}
		try {
			this.obuffer = this.encryptor.update(this.ibuffer, 0, readin);
		} catch (IllegalStateException e) {
			this.obuffer = null;
		}
		this.ostart = 0;
		if (this.obuffer == null)
			this.ofinish = 0;
		else
			this.ofinish = this.obuffer.length;
		return this.ofinish;
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public int read() throws IOException {
		if (this.ostart >= this.ofinish) {
			// we loop for new data as the spec says we are blocking
			int i = 0;
			while (i == 0)
				i = getMoreData();
			if (i == -1)
				return -1;
		}
		return this.obuffer[this.ostart++] & 0xff;
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (this.ostart >= this.ofinish) {
			// we loop for new data as the spec says we are blocking
			int i = 0;
			while (i == 0)
				i = getMoreData();
			if (i == -1)
				return -1;
		}
		if (len <= 0)
			return 0;
		int available = this.ofinish - this.ostart;
		if (len < available)
			available = len;
		if (b != null)
			System.arraycopy(this.obuffer, this.ostart, b, off, available);
		this.ostart = this.ostart + available;
		return available;
	}

	@Override
	public long skip(long n) throws IOException {
		int available = this.ofinish - this.ostart;
		if (n > available)
			n = available;
		if (n < 0)
			return 0;
		this.ostart += n;
		return n;
	}
}
