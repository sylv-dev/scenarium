/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

import org.beanmanager.BeanRenameListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.InputLinksChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

public interface RemoteBlock {

	default void addBlockNameChangeListener(BeanRenameListener listener) {
		throw new UnsupportedOperationException();
	};

	default void addDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void addDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void addInputLinksChangeListener(InputLinksChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void addStructChangeListener(StructChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void fireDeclaredInputChanged(String[] names, Class<?>[] types) {
		throw new UnsupportedOperationException();
	};

	default void fireDeclaredOutputChanged(String[] names, Class<?>[] types) {
		throw new UnsupportedOperationException();
	};

	default void fireStructChanged() {
		throw new UnsupportedOperationException();
	};

	default String getBlockName() {
		throw new UnsupportedOperationException();
	};

	default int getInputIndex(String inputName) {
		throw new UnsupportedOperationException();
	};

	default String[] getInputsName() {
		throw new UnsupportedOperationException();
	};

	default long getMaxTimeStamp() {
		throw new UnsupportedOperationException();
	};

	default int getNbInput() {
		throw new UnsupportedOperationException();
	};

	default int getNbOutput() {
		throw new UnsupportedOperationException();
	};

	default int getOutputIndex(String outputName) {
		throw new UnsupportedOperationException();
	};

	default String[] getOutputLinkToInputName() {
		throw new UnsupportedOperationException();
	};

	default Class<?>[] getOutputLinkToInputType() {
		throw new UnsupportedOperationException();
	};

	default String[] getOutputsName() {
		throw new UnsupportedOperationException();
	};

	default long getTimeOfIssue(int indexOfInput) {
		throw new UnsupportedOperationException();
	};

	default long getTimeStamp(int indexOfInput) {
		throw new UnsupportedOperationException();
	};

	default String getWarning() {
		throw new UnsupportedOperationException();
	};

	default boolean isDefaulting() {
		throw new UnsupportedOperationException();
	};

	default boolean isPropertyAsInput(String propertyName) {
		throw new UnsupportedOperationException();
	};

	default void onStart(Runnable runnable) {
		throw new UnsupportedOperationException();
	};

	default void onResume(Runnable runnable) {
		throw new UnsupportedOperationException();
	};

	default void onPause(Runnable runnable) {
		throw new UnsupportedOperationException();
	};

	default void onStop(Runnable runnable) {
		throw new UnsupportedOperationException();
	};

	default void removeBlockNameChangeListener(BeanRenameListener listener) {
		throw new UnsupportedOperationException();
	};

	default void removeDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void removeDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void removeStructChangeListener(StructChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		throw new UnsupportedOperationException();
	};

	default void runLater(Runnable runnable) {
		throw new UnsupportedOperationException();
	};

	default void setDefaulting(boolean defaulting) {
		throw new UnsupportedOperationException();
	};

	default boolean setPropertyAsInput(String propertyName, boolean asInput) {
		throw new UnsupportedOperationException();
	};

	default void setWarning(String warning) {
		throw new UnsupportedOperationException();
	};

	default boolean triggerOutput(Object outputValue) {
		throw new UnsupportedOperationException();
	};

	default boolean triggerOutput(Object[] outputValue) {
		throw new UnsupportedOperationException();
	};

	default boolean triggerOutput(Object outputValue, long timeStamp) {
		throw new UnsupportedOperationException();
	};

	default boolean triggerOutput(Object[] outputValue, long timeStamp) {
		throw new UnsupportedOperationException();
	};

	default boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
		throw new UnsupportedOperationException();
	};

	default boolean updateInputs(String[] names, Class<?>[] types) {
		throw new UnsupportedOperationException();
	};

	default boolean updateOutputs(String[] names, Class<?>[] types) {
		throw new UnsupportedOperationException();
	}

	// default void addIONameChangeListener(NameChangeListener listener) {
	// throw new UnsupportedOperationException();
	// }
	//
	// default void removeIONameChangeListener(NameChangeListener listener) {
	// throw new UnsupportedOperationException();
	// }
	//
	// default void fireIONameChanged(boolean input, int indexOfInput, String newName) {
	// throw new UnsupportedOperationException();
	// }
}
