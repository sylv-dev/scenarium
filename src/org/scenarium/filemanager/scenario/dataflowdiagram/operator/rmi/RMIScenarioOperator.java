/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBeanCallBackImpl;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.timescheduler.SchedulerInterface;

public class RMIScenarioOperator extends RMIOperator implements RMIScenarioOperatorImpl {
	private static final long serialVersionUID = 1L;
	private long timeStamp;

	public RMIScenarioOperator(Class<?> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(type, identifier, callBack);
		((Scenario) this.bean).setScheduler(new SchedulerInterface(null) {
			@Override
			public long getTimeStamp() {
				return RMIScenarioOperator.this.timeStamp;
			}
		});
	}

	@Override
	public void update(long callingThreadId, int taskIdentifier, long timePointer, long timeStamp) throws RemoteException {
		mapCallingThreadIdToRmiThread(callingThreadId);
		this.timeStamp = timeStamp;
		try {
			((Scenario) this.bean).getTaskFromId(taskIdentifier).update(timePointer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		unMapCallingThreadIdToRmiThread();
	}

}
