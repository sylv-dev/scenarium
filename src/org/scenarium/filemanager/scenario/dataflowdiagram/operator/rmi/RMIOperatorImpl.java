/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBeanImpl;

public interface RMIOperatorImpl extends RMIBeanImpl {
	public void birth(long callingThreadId) throws Throwable;

	public void death(long callingThreadId) throws Throwable;

	public void fireBlockNameChanged(long callingThreadId, String beanDescName, String beanDescLocal, String oldName, int id)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public void fireDeclaredInputChanged(long callingThreadId, String[] names, Class<?>[] types)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public void fireDeclaredOutputChanged(long callingThreadId, String[] names, Class<?>[] types)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public void fireInputLinksChanged(long callingThreadId, int indexOfInput)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public void fireStructChanged(long callingThreadId) throws RemoteException;

	public void fireVarArgsInputChanged(long callingThreadId, int indexOfInput, int type)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public boolean needToBeSaved(long callingThreadId) throws RemoteException;

	public void onStartCallBack(long callingThreadId) throws RemoteException;

	public void onResumeCallBack(long callingThreadId) throws RemoteException;

	public void onPauseCallBack(long callingThreadId) throws RemoteException;

	public void onStopCallBack(long callingThreadId) throws RemoteException;

	public Object[] process(long callingThreadId, Object[] args, Object[] additionalInputs, boolean[] linkedStaticOutput) throws Throwable;

	public Object[] processWithNewInputs(long callingThreadId, String[] inputsClassName, Object[] inputs, String[] additionalInputsClassName, Object[] additionalInputs, boolean[] linkedStaticOutput)
			throws Throwable;

	public void runLaterCallBack(long callingThreadId, int taskId) throws RemoteException;

	// public boolean isBlockReady(long callingThreadId, boolean[] inputs) throws RemoteException;

	public void setMainThreadId(long mainThreadId) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public boolean updateInputs(String[] names, Class<?>[] types) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;

	public boolean updateOutputs(String[] names, Class<?>[] types) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;
}
