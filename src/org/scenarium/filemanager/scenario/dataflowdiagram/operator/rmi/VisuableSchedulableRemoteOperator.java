/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBeanImpl;
import org.scenarium.timescheduler.VisuableSchedulable;

public class VisuableSchedulableRemoteOperator extends RemoteOperator implements VisuableSchedulable {

	public VisuableSchedulableRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack,
			boolean eraseCallBackIfPresent) throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent);
	}

	public VisuableSchedulableRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack,
			boolean eraseCallBackIfPresent, int timeout) throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, timeout);
	}

	@Override
	public boolean needToBeSchedule() {
		return false;
	}

	@Override
	public void paint() {
		throw new IllegalAccessError(VisuableSchedulableRemoteOperator.class.getSimpleName() + " must not be schedule, the paint method should therefore not be call");
	}

	@Override
	public void setAnimated(boolean animated) {
		RMIBeanImpl remoteBean = this.remoteBean;
		if (remoteBean != null)
			try {
				((RMIVisuableSchedulableOperatorImpl) remoteBean).setAnimated(Thread.currentThread().getId(), animated);
			} catch (RemoteException e) { // ConnectException
				e.printStackTrace();
			}
	}
}
