/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanManager;
import org.scenarium.filemanager.scenariomanager.RecordingListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.SourceChangeListener;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;
import org.scenarium.operator.AbstractRecorder;

/******************************************************************************* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
public class FlowDiagram implements Serializable, InputLinkChangeListener, SourceChangeListener, CpuUsageMeasurable, IOComponent, DiagramIONameChangeListener, PropertyChangeListener {
	private static final long serialVersionUID = 1L;
	private List<Block> blocks = Collections.unmodifiableList(new ArrayList<>());
	private List<FlowDiagramInput> inputs = Collections.unmodifiableList(new ArrayList<>());
	private List<FlowDiagramOutput> outputs = Collections.unmodifiableList(new ArrayList<>());
	private final EventListenerList listeners = new EventListenerList();
	private boolean managingCpuUsage;
	private float cpuUsage = -1;
	private BiConsumer<List<Block>, Runnable> timeOutTask;
	private final HashMap<Block, StructChangeListener> structChangeListenerMap = new HashMap<>();
	// private HashMap<Block, NameChangeListener> nameChangeListenerMap = new HashMap<>();

	public FlowDiagram() {}

	public boolean addBlock(Block block) {
		Object op = block.operator;
		if (contains(op) != -1)
			return false;
		ArrayList<Block> blocks = new ArrayList<>(this.blocks);
		blocks.add(block);
		this.blocks = blocks;
		if (op instanceof Scenario)
			((Scenario) op).addSourceChangeListener(this);
		block.getInputs().forEach(input -> input.addLinkChangeListener(this));
		StructChangeListener scl = () -> {
			block.getInputs().forEach(input -> input.addLinkChangeListenerIfNotPresent(this));
			fireDiagramChanged(block, "struct", ModificationType.CHANGE);
			for (Block otherBlock : blocks) {
				List<BlockInput> oinputs = otherBlock.getInputs();
				for (int i = 0; i < oinputs.size(); i++) {
					BlockInput input = oinputs.get(i);
					Link link = input.getLink();
					if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == otherBlock) {
						List<BlockOutput> oOutput = otherBlock.getOutputs();
						int j = 0;
						for (; j < oOutput.size(); j++)
							if (oOutput.get(j).getName().equals(link.getOutput().getName())) { // si même nom, je recrer lien
								// if(link.getOutput() != blockChanged.getOutput(j)) //Ajout du 23/08/07 pas besoin de remettre le lien si c'est le même...
								input.setLink(new Link(link.getInput(), oOutput.get(j))); // mon block n'a peut être pas encore la patte
								break;
							}
						if (j == oOutput.size()) {
							input.setLink(null); // et si je décalle les var args, je rate une patte
							i = -1;
						}
					}
				}
			}
			for (FlowDiagramInput fdi : this.inputs) {
				Link link = fdi.getLink();
				if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == block) {
					List<BlockOutput> outputs = block.getOutputs();
					int j = 0;
					for (; j < outputs.size(); j++)
						if (outputs.get(j).getName().equals(link.getOutput().getName())) { // si même nom, je recrer lien
							fdi.setLink(new Link(link.getInput(), outputs.get(j))); // mon block n'a peut être pas encore la patte
							break;
						}
					if (j == outputs.size())
						fdi.setLink(null);
				}
			}
		};
		this.structChangeListenerMap.put(block, scl);
		if (op instanceof EvolvedOperator)
			((EvolvedOperator) op).addStructChangeListener(scl);
		else
			block.addPropertyStructChangeListener(scl);
		if (op instanceof AbstractRecorder) {
			AbstractRecorder ar = (AbstractRecorder) op;
			ar.addPropertyChangeListener(this);
			boolean isPreviouslyRecording = this.blocks.stream().filter(b -> b != block && b.getOperator() instanceof AbstractRecorder)
					.anyMatch(b -> ((AbstractRecorder) b.getOperator()).isRecording());
			if (ar.isRecording() && !isPreviouslyRecording)
				fireRecordingChange(true);
		}
		fireDiagramChanged(block, null, ModificationType.NEW);
		return true;
	}

	public void addFlowDiagramChangeListener(FlowDiagramChangeListener listener) { // TODO check listener
		this.listeners.add(FlowDiagramChangeListener.class, listener);
	}

	public void addInput(FlowDiagramInput input) {
		ArrayList<FlowDiagramInput> inputs = new ArrayList<>(this.inputs);
		inputs.add(input);
		this.inputs = inputs;
		input.addLinkChangeListener(this);
		input.addIONameChangeListener(this);
		fireDiagramChanged(input, null, ModificationType.NEW);
	}

	public void addManagingCpuUsage(ManagingCpuUsageListener listener) {
		this.listeners.add(ManagingCpuUsageListener.class, listener);
	}

	public void addOutput(FlowDiagramOutput output) {
		ArrayList<FlowDiagramOutput> outputs = new ArrayList<>(this.outputs);
		outputs.add(output);
		this.outputs = outputs;
		output.addIONameChangeListener(this);
		fireDiagramChanged(output, null, ModificationType.NEW);
	}

	public void beanRename(Object bean) {
		for (Block block : this.blocks)
			if (block.operator == bean) {
				fireDiagramChanged(block, "name", ModificationType.CHANGE);
				break;
			}
	}

	// @Override
	// public void depictionChange() {
	// fireFlowDiagramDepictionChanged();
	// }

	public void blockModeChanged(Block block, ModificationType modificationType) {
		fireDiagramChanged(block, null, modificationType);
	}

	@Override
	public boolean canTriggerOrBeTriggered() {
		return true;
	}

	@Override
	public Object clone() {
		return this;
	}

	public int contains(Object operator) {
		List<Block> blocks = this.blocks;
		for (int i = 0; i < blocks.size(); i++)
			if (blocks.get(i).operator == operator)
				return i;
		return -1;
	}

	public void death() {
		for (Block block : this.blocks) {
			Object operator = block.operator;
			if (operator instanceof Scenario)
				((Scenario) operator).removeSourceChangeListener(this);
			if (operator instanceof EvolvedOperator) {
				((EvolvedOperator) operator).removeStructChangeListener(this.structChangeListenerMap.remove(block));
				((EvolvedOperator) operator).setBlock(null);
				((EvolvedOperator) operator).setTrigger(null);
			} else {
				block.removePropertyStructChangeListener(this.structChangeListenerMap.remove(block));
				block.setRunLaterFunction(null);
			}
			if (operator instanceof AbstractRecorder)
				((AbstractRecorder) operator).removePropertyChangeListener(this);
			block.getInputs().forEach(input -> input.removeLinkChangeListener(this));
			block.dispose();
		}
		this.blocks = new ArrayList<>();
		// blocks.clear(); //Pk??? pas possible la liste n'est pas modifiable....
	}

	private void fireDiagramChanged(Object element, String property, ModificationType modificationType) {
		for (Block block : this.blocks)
			block.resetIndex();
		for (FlowDiagramChangeListener listener : this.listeners.getListeners(FlowDiagramChangeListener.class))
			listener.flowDiagramChanged(element, property, modificationType);
	}

	public void fireManagingCpuUsageChanged() {
		for (ManagingCpuUsageListener listener : this.listeners.getListeners(ManagingCpuUsageListener.class))
			listener.managingCpuChanged(this.managingCpuUsage);
	}

	public ArrayList<Link> getAllLinks() {
		ArrayList<Link> links = new ArrayList<>();
		for (Block block : this.blocks)
			for (BlockInput input : block.getInputs()) {
				Link link = input.getLink();
				if (link != null)
					links.add(link);
			}
		for (FlowDiagramInput input : this.inputs) {
			Link link = input.getLink();
			if (link != null)
				links.add(link);
		}
		return links;
	}

	// @Override
	// public void iOTypeChange(BlockIO io) {
	// if (io instanceof BlockOutput) {
	// for (Block block : blocks) {
	// int nbInput = block.getNbInput();
	// for (int i = 0; i < nbInput; i++) {
	// BlockInput input = block.getInput(i);
	// Link link = input.getLink();
	// if (link != null && link.getOutput() == io && !input.type.isAssignableFrom(io.type))
	// input.setLink(null);
	// }
	// }
	// }
	// fireFlowDiagramDepictionChanged();
	// }

	// public void addDynamicFlowDiagramDepictionChangeListener(DynamicFlowDiagramDepictionChangeListener listener) {
	// listeners.add(DynamicFlowDiagramDepictionChangeListener.class, listener);
	// }
	//
	// public void removeDynamicFlowDiagramDepictionChangeListener(DynamicFlowDiagramDepictionChangeListener listener) {
	// listeners.remove(DynamicFlowDiagramDepictionChangeListener.class, listener);
	// }
	//
	// public void fireDynamicFlowDiagramDepictionChanged() {
	// for (DynamicFlowDiagramDepictionChangeListener listener : listeners.getListeners(DynamicFlowDiagramDepictionChangeListener.class))
	// listener.dynamicFlowDiagramChanged();
	// }

	public List<Block> getBlocks() {
		return this.blocks;
	}

	public Block getBlock(String name) {
		for (Block block : this.blocks)
			if (block.getName().equals(name))
				return block;
		return null;
	}

	public float getCpuUsage() {
		return this.cpuUsage;
	}

	@Override
	public IOLinks[] getIndex() {
		// TODO Auto-generated method stub
		return null;
	}

	// public HashMap<Output, ArrayList<Input>> getIndex() {
	// if (index == null) {
	// index = new HashMap<>();
	// for (Block block : blocks) {
	// int nbInput = block.getNbInput();
	// for (int j = 0; j < nbInput; j++) {
	// Link link = block.getInput(j).getLink();
	// if (link != null) {
	// Output output = link.getOutput();
	// if (output instanceof Output) {
	// if (!index.containsKey(output))
	// index.put((Output) output, new ArrayList<Input>());
	// index.get(output).add(block.getInput(j));
	// }
	// }
	// }
	// }
	// for (FlowDiagramInput fdi : inputs) {
	// Link link = fdi.getLink();
	// if (link != null) {
	// Output output = link.getOutput();
	// if (output instanceof Output) {
	// if (!index.containsKey(output))
	// index.put((Output) output, new ArrayList<Input>());
	// index.get(output).add(fdi);
	// }
	// }
	// }
	// }
	// return index;
	// }

	public FlowDiagramInput getInput(int index) {
		return this.inputs.get(index);
	}

	@Override
	public List<FlowDiagramInput> getInputs() {
		return this.inputs;
	}

	@Override
	public Object[] getOutputBuff() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long[] getOutputBuffTs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FlowDiagramOutput> getOutputs() {
		return this.outputs;
	}

	public BiConsumer<List<Block>, Runnable> getTimeOutTask() {
		return this.timeOutTask;
	}

	public boolean isManagingCpuUsage() {
		return this.managingCpuUsage;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public void linkChange(Link link, ModificationType modificationType) {
		fireDiagramChanged(link, null, modificationType);
	}

	@Override
	public void nameChanged(IO io) {
		fireDiagramChanged(io, "name", ModificationType.CHANGE);
	}

	public boolean removeBlock(Block block, boolean needToKillOperator) {
		ArrayList<Block> blocks = new ArrayList<>(this.blocks);
		if (!blocks.remove(block))
			return false;
		this.blocks = blocks;
		block.getInputs().forEach(input -> input.removeLinkChangeListener(this));
		Object operator = block.operator;
		if (operator instanceof EvolvedOperator)
			((EvolvedOperator) operator).removeStructChangeListener(this.structChangeListenerMap.remove(block));
		else
			block.removePropertyStructChangeListener(this.structChangeListenerMap.remove(block));
		if (operator instanceof AbstractRecorder) {
			AbstractRecorder ar = (AbstractRecorder) operator;
			ar.removePropertyChangeListener(this);
			if (ar.isRecording() && !isRecording())
				fireRecordingChange(false);
		}
		block.dispose();
		fireDiagramChanged(block, null, ModificationType.DELETE);
		try {
			if (needToKillOperator) {
				Method declaredMethod = operator.getClass().getMethod("death", new Class<?>[0]);
				declaredMethod.setAccessible(true);
				MethodHandle mh = MethodHandles.lookup().unreflect(declaredMethod);
				mh.invoke(operator);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return true;
	}

	public ArrayList<Object> removeElements(ArrayList<Object> selectedElements, boolean needToKillOperator, boolean purgeBean) {
		ArrayList<Object> removedElements = new ArrayList<>();
		for (Object selectedElement : selectedElements)
			if (selectedElement instanceof Block) {
				for (Block block : this.blocks) {
					boolean hasRemovedLink = true;
					while (hasRemovedLink) {
						hasRemovedLink = false;
						for (BlockInput input : block.getInputs()) {
							Link link = input.getLink();
							if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == selectedElement) {
								input.setLink(null);
								hasRemovedLink = true;
								break;
							}
						}
					}
				}
				boolean hasRemovedLink = false;
				do {
					hasRemovedLink = false;
					for (BlockInput input : ((Block) selectedElement).getInputs())
						if (input.getLink() != null) {
							input.setLink(null);
							hasRemovedLink = true;
							break;
						}
					if (!hasRemovedLink)
						break;
				} while (hasRemovedLink);
				for (Input input : this.inputs) {
					Link link = input.getLink();
					if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == selectedElement)
						input.setLink(null);
				}
				if (removeBlock((Block) selectedElement, needToKillOperator))
					removedElements.add(selectedElement);
				if (purgeBean)
					BeanManager.purgeBean(((Block) selectedElement).operator, true, purgeBean);
			} else if (selectedElement instanceof Link) {
				boolean isRemoved = false;
				for (Input input : this.inputs)
					if (selectedElement == input.getLink()) {
						input.setLink(null);
						isRemoved = true;
						break;
					}
				if (!isRemoved)
					linkRemoved: for (Block block : this.blocks)
						for (BlockInput input : block.getInputs())
							if (selectedElement == input.getLink()) {
								input.setLink(null);
								isRemoved = true;
								break linkRemoved;
							}
				if (isRemoved)
					removedElements.add(selectedElement);
			} else if (selectedElement instanceof FlowDiagramInput) {
				ArrayList<FlowDiagramInput> inputs = new ArrayList<>(this.inputs);
				if (inputs.remove(selectedElement))
					removedElements.add(selectedElement);
				this.inputs = inputs;
				((FlowDiagramInput) selectedElement).setLink(null);
				((FlowDiagramInput) selectedElement).removeLinkChangeListener(this);
				((FlowDiagramInput) selectedElement).removeIONameChangeListener(this);
				fireDiagramChanged(selectedElement, null, ModificationType.DELETE);
			} else if (selectedElement instanceof FlowDiagramOutput) {
				boolean hasRemovedLink = false;
				do {
					hasRemovedLink = false;
					for (Block block : this.blocks)
						for (BlockInput input : block.getInputs()) {
							Link link = input.getLink();
							if (link != null && selectedElement == link.getOutput()) {
								input.setLink(null);
								hasRemovedLink = true;
								break;
							}
						}
					if (!hasRemovedLink)
						break;
				} while (hasRemovedLink);
				ArrayList<FlowDiagramOutput> outputs = new ArrayList<>(this.outputs);
				if (outputs.remove(selectedElement))
					removedElements.add(selectedElement);
				this.outputs = outputs;
				((FlowDiagramOutput) selectedElement).removeIONameChangeListener(this);
				fireDiagramChanged(selectedElement, null, ModificationType.DELETE);
			} else if (selectedElement instanceof BlockInput) {
				BlockInput bi = (BlockInput) selectedElement;
				if (bi.block.operator instanceof EvolvedOperator)
					if (((EvolvedOperator) bi.block.operator).setPropertyAsInput(bi.getName(), false))
						removedElements.add(selectedElement);
					else if (bi.block.setPropertyAsInput(bi.getName(), false))
						removedElements.add(selectedElement);
			}
		return removedElements;
	}

	public void removeFlowDiagramChangeListener(FlowDiagramChangeListener listener) { // TODO diagramDrawerFx ne supprime pas son listener, on a des events meme après un reload
		this.listeners.remove(FlowDiagramChangeListener.class, listener); // Il le fait si setDrawableElement ou alors si death, mais ca arrive trop tard....
	}

	public void removeManagingCpuUsageListener(ManagingCpuUsageListener listener) {
		this.listeners.remove(ManagingCpuUsageListener.class, listener);
	}

	@Override
	public void setCpuUsage(float cpuUsage) {
		if (cpuUsage > 1)
			cpuUsage = 1;
		this.cpuUsage = cpuUsage;
	}

	public void setManagingCpuUsage(boolean managingCpuUsage) {
		this.managingCpuUsage = managingCpuUsage;
		fireManagingCpuUsageChanged();
	}

	public void setStopTimeOutTask(BiConsumer<List<Block>, Runnable> timeOutTask) {
		this.timeOutTask = timeOutTask;
	}

	@Override
	public void sourceChanged() {
		fireDiagramChanged(null, null, ModificationType.CHANGE);
	}

	@Override
	public String toString() {
		return "Blocks: " + this.blocks.toString() + " NbInputs: " + this.inputs.size() + " NbOutputs: " + this.outputs.size();
	}

	public boolean isRecording() {
		return this.blocks.stream().filter(b -> b.getOperator() instanceof AbstractRecorder).anyMatch(b -> ((AbstractRecorder) b.getOperator()).isRecording());
	}

	public void setRecording(boolean recording) {
		this.blocks.stream().filter(b -> b.getOperator() instanceof AbstractRecorder).forEach(b -> ((AbstractRecorder) b.getOperator()).setRecording(recording));
	}

	public void addRecordingListener(RecordingListener listener) {
		this.listeners.add(RecordingListener.class, listener);
	}

	public void removeRecordingListener(RecordingListener listener) {
		this.listeners.remove(RecordingListener.class, listener);
	}

	private void fireRecordingChange(boolean recording) {
		for (RecordingListener listener : this.listeners.getListeners(RecordingListener.class))
			listener.recordingPropertyChanged(recording);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("recording"))
			fireRecordingChange(isRecording());
	}
}
