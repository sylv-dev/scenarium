/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public class BrokerPropertyException extends Exception {
	private static final long serialVersionUID = 1L;

	public BrokerPropertyException(String propertyName, String message) {
		super("The property input: " + propertyName + " is broken due to: " + message);
	}

}
