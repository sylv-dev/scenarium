/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public abstract class Output extends IO {
	protected Input[] inputs;

	public Output(Class<?> type, String name) {
		super(type, name);
	}

	void addLink(Link link) {
		if (this.inputs == null)
			this.inputs = new Input[] { link.getInput() };
		else {
			Input[] newInputs = new Input[this.inputs.length + 1];
			int i = 0;
			for (; i < this.inputs.length; i++)
				newInputs[i] = this.inputs[i];
			newInputs[i] = link.getInput();
			this.inputs = newInputs;
		}
	}

	public Input[] getLinkInputs() {
		return this.inputs;
	}

	void removeLink(Link link) {
		Input linkedInput = link.getInput();
		if (this.inputs.length == 1)
			this.inputs = null;
		else {
			Input[] newInputs = new Input[this.inputs.length - 1];
			int index = 0;
			for (Input input : this.inputs)
				if (input != linkedInput)
					newInputs[index++] = input;
			this.inputs = newInputs;
		}
	}
}
