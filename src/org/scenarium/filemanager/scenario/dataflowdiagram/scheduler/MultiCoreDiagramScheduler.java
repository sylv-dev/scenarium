/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOData;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLink;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLinks;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.ManagingCpuUsageListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.Output;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;

public class MultiCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	private final ArrayList<ThreadedBlock> threadedBlocks = new ArrayList<>();
	private final HashMap<Block, Condition> incomingDataOrTaskSignals = new HashMap<>();
	private volatile boolean dead = false;
	private CPUUsageTask cpuUsageTask;
	private final Object cpuUsageTaskLock = new Object();
	private final boolean started;

	public MultiCoreDiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks) {
		super(fd, schedulableBlocks);
		fd.setCpuUsage(-1);
		List<Block> blocks = fd.getBlocks();
		AtomicInteger nbThreadedBlocksLeft = new AtomicInteger(blocks.size());
		fd.addManagingCpuUsage(this);
		ThreadedBlock[] threads = new ThreadedBlock[blocks.size()];
		for (int i = 0; i < blocks.size(); i++)
			threads[i] = createBlock(blocks.get(i), nbThreadedBlocksLeft);

		Arrays.sort(threads, (a, b) -> Integer.compare(b.block.getStartPriority(), a.block.getStartPriority()));
		int i = 0;
		ArrayList<ThreadedBlock> launchedThreadedBlocks = new ArrayList<>(threads.length);
		while (i != threads.length) {
			int priority = threads[i].block.getStartPriority();
			while (i != threads.length && threads[i].block.getStartPriority() == priority) {
				ThreadedBlock t = threads[i++];
				t.start();
				launchedThreadedBlocks.add(t);
			}
			waitThreadedBlock(launchedThreadedBlocks, tb -> tb.isStarted);
			launchedThreadedBlocks.clear();
		}
		this.started = true;
	}

	private ThreadedBlock createBlock(Block block, AtomicInteger nbThreadedBlocksLeft) {
		if (this.dead)
			return null;
		DiagramScheduler.initBlock(block, this);
		ThreadedBlock blockThread = new ThreadedBlock(block, nbThreadedBlocksLeft, this);
		this.incomingDataOrTaskSignals.put(block, blockThread.incomingDataOrTask);
		this.threadedBlocks.add(blockThread);
		if (this.fd.isManagingCpuUsage())
			blockThread.initCpuUsageTask();
		return blockThread;
	}

	@Override
	public void removeBlock(Block block) {
		if (this.dead)
			return;
		for (ThreadedBlock threadedBlock : this.threadedBlocks)
			if (block == threadedBlock.getBlock())
				threadedBlock.interrupt();
	}

	@Override
	public void addBlock(Block block) {
		if (this.dead)
			return;
		ThreadedBlock tb = createBlock(block, null);
		tb.start();
		initOperator(block);
	}

	@Override
	public boolean isAlive() {
		return !this.dead;
	}

	@Override
	public boolean isStarted() {
		return this.started;
	}

	@Override
	protected void blockNameChanged(Block block) {
		for (ThreadedBlock threadedBlock : this.threadedBlocks)
			if (threadedBlock.block == block) {
				triggerTask(block, () -> threadedBlock.setName(block.getName()));
				break;
			}
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (this.dead)
			return;
		if (managingCpu)
			for (ThreadedBlock threadedBlock : this.threadedBlocks)
				threadedBlock.initCpuUsageTask();
		else
			for (ThreadedBlock threadedBlock : this.threadedBlocks)
				threadedBlock.closeCpuUsageTask();
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		if (this.started)
			runnable.run();
		else
			addOnEvent(source, tb -> tb.onStart(runnable));
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onResume(runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onPause(runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onStop(runnable));
	}

	private void addOnEvent(Object source, Consumer<ThreadedBlock> consumer) {
		if (this.dead)
			throw new IllegalAccessError("The scheduler is dead. The stopped event cannot therefore occur");
		for (ThreadedBlock threadedBlock : this.threadedBlocks)
			if (threadedBlock.getBlock() == source) {
				consumer.accept(threadedBlock);
				return;
			}
		throw new IllegalArgumentException("The source: " + source + " is not managed by this scheduler");
	}

	@Override
	public void start() {
		for (ThreadedBlock threadedBlock : this.threadedBlocks)
			triggerTask(threadedBlock.getBlock(), threadedBlock::consumeOnStartTasks);
	}

	@Override
	public void resume() {
		this.threadedBlocks.forEach(tb -> tb.consumeOnResumeTasks());
	}

	@Override
	public void pause() {
		this.threadedBlocks.forEach(tb -> tb.consumeOnPauseTasks());
	}

	@Override
	public void preStop() {
		this.threadedBlocks.forEach(tb -> tb.consumeOnStopTasks());
	}

	void stackOutputsAndTrigger(IOComponent block, Object[] outputs, long[] outputsTs, FlowDiagram fd) {
		if (!block.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = block.getIndex();
		if (block instanceof FlowDiagram)
			System.out.println("sortie de diagram de flux");
		for (IOLinks ioLinks : index) {
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered()) {
				Condition incomingDataSignal = this.incomingDataOrTaskSignals.get(linkedComp);
				if (incomingDataSignal != null) { // Sinon, c'est probablement un trigger après la fin d'un thread
					ReentrantLock ioLock = ((Block) linkedComp).getIOLock();
					ioLock.lock();
					try {
						boolean blockReady = false;
						for (IOLink ioLink : ioLinks.getLinks()) {
							int outputIndex = ioLink.getOutputIndex();
							Object outputObject = outputs[outputIndex];
							if (outputObject == null)
								continue;
							blockReady = true;
							if (ioLink.getInput().getBuffer().push(outputsTs[outputIndex], timeOfIssue, ioLink.isNeedToCopy() ? clone(outputObject) : outputObject))
								DiagramScheduler.showBufferOverflow(block, ioLink.getInput());
							Link link = ioLink.getInput().getLink();
							if (link != null)
								link.consume();
						}
						if (blockReady)
							incomingDataSignal.signal();
					} finally {
						ioLock.unlock();
					}
				} else
					System.err.println(block + ": trigger a data after the death of " + linkedComp);
			}
		}
	}

	@Override
	public void stop() {
		if (this.dead)
			return;
		synchronized (this) {
			this.dead = true;
			this.threadedBlocks.sort((a, b) -> Integer.compare(b.block.getStopPriority(), a.block.getStopPriority()));
			int i = 0;
			ArrayList<ThreadedBlock> stoppedThreadedBlocks = new ArrayList<>(this.threadedBlocks.size());
			while (i != this.threadedBlocks.size()) {
				int priority = this.threadedBlocks.get(i).block.getStopPriority();
				while (i != this.threadedBlocks.size() && this.threadedBlocks.get(i).block.getStopPriority() == priority) {
					ThreadedBlock t = this.threadedBlocks.get(i++);
					t.dead = true;
					Block block = t.block;
					if (block.birthFailed())
						block.reset();
					else {
						ReentrantLock lock = block.getIOLock();
						lock.lock();
						try {
							this.incomingDataOrTaskSignals.get(block).signal();
						} finally {
							lock.unlock();
						}
					}
					stoppedThreadedBlocks.add(t);
				}
				waitThreadedBlock(stoppedThreadedBlocks, tb -> !tb.isAlive());
				stoppedThreadedBlocks.clear();
			}
			this.threadedBlocks.clear();
			this.incomingDataOrTaskSignals.clear();
			this.fd.setCpuUsage(-1);
			this.fd.removeManagingCpuUsageListener(this);
		}
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		if (isAlive())
			if (!this.started)
				System.err.println("Early trigger of: " + source + " data removed");
			else
				stackOutputsAndTrigger((IOComponent) source, new Object[] { outputValue }, new long[] { timeStamp }, this.fd);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		if (isAlive()) {
			if (!this.started)
				System.err.println("Early trigger of: " + source + " data removed");
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null)
					outputValues[i] = null;
				else if (outputValues[i] != null) {
					outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
				}
			long[] timeStamps = new long[outputValues.length];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = timeStamp;
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps, this.fd);
		}
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		if (isAlive()) {
			if (!this.started)
				System.err.println("Early trigger of: " + source + " data removed");
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null) {
					outputValues[i] = null;
					timeStamps[i] = -1;
				} else if (outputValues[i] != null) {
					outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
				}
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps, this.fd);
		}
	}

	@Override
	public void triggerTask(Object source, Runnable task) {
		for (ThreadedBlock threadedBlock : this.threadedBlocks)
			if (threadedBlock.getBlock() == source) {
				if (Thread.currentThread() == threadedBlock
						|| threadedBlock.getBlock().getRemoteOperator() != null && threadedBlock.getBlock().getRemoteOperator().isFromThreadedBlock(threadedBlock.getId()))
					task.run();
				else if (this.dead || threadedBlock.getBlock().birthFailed()) {
					try {
						threadedBlock.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					task.run();
				} else {
					ReentrantLock lock = ((Block) source).getIOLock();
					if (lock.isHeldByCurrentThread())
						task.run();
					else {
						lock.lock();
						Condition taskCompleted = lock.newCondition();
						threadedBlock.tasksCompleted.add(taskCompleted);
						threadedBlock.tasks.add(task);
						try {
							this.incomingDataOrTaskSignals.get(source).signal();
							try {
								taskCompleted.await();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						} finally {
							lock.unlock();
						}
					}
				}
				return;
			}
		task.run(); // Impossible de trouver la tache..., je run
	}

	private void waitThreadedBlock(ArrayList<ThreadedBlock> threadedBlocks, Predicate<ThreadedBlock> blockCondition) {
		int timeOutTime = 1000;
		for (ThreadedBlock tb : threadedBlocks) {
			ProcessMode pm = tb.block.getProcessMode();
			if (pm == ProcessMode.ISOLATED) {
				timeOutTime = 2000;
				break;
			} else if (pm == ProcessMode.REMOTE)
				timeOutTime = 1500;
		}
		long endOfTimeout = System.currentTimeMillis() + timeOutTime;
		boolean timeOut = false;
		LinkedList<ThreadedBlock> blockingThreads = new LinkedList<>(threadedBlocks);
		while (!blockingThreads.isEmpty()) {
			long millis = endOfTimeout - System.currentTimeMillis();
			if (millis < 0) {
				timeOut = true;
				break;
			}
			try {
				Thread.sleep(Math.min(50, millis));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			blockingThreads.removeIf(blockCondition);
		}
		if (timeOut) {
			List<Block> aliveSynchroThreads = Collections.synchronizedList(blockingThreads.stream().map(t -> t.getBlock()).collect(Collectors.toList()));
			BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
			Thread stopTimeOutThread = null;
			if (timeOutTask != null) {
				stopTimeOutThread = new Thread(() -> timeOutTask.accept(aliveSynchroThreads, () -> blockingThreads.forEach(tb -> tb.interrupt())));
				stopTimeOutThread.start();
			}
			while (!blockingThreads.isEmpty()) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for (Iterator<ThreadedBlock> iterator = blockingThreads.iterator(); iterator.hasNext();) {
					ThreadedBlock tb = iterator.next();
					if (blockCondition.test(tb)) {
						iterator.remove();
						aliveSynchroThreads.remove(tb.getBlock());
					}
				}
			}
			if (stopTimeOutThread != null)
				stopTimeOutThread.interrupt();
		}
	}

	class ThreadedBlock extends Thread {
		private final Object onTaskLock = new Object();
		private ArrayList<Runnable> onStartTasks;
		private ArrayList<Runnable> onPauseTasks;
		private ArrayList<Runnable> onResumeTasks;
		private ArrayList<Runnable> onStopTasks;
		private final Block block;
		private final MultiCoreDiagramScheduler scheduler;
		private AtomicInteger nbThreadedBlocksLeft;
		private Object[] inputs = new Object[0];
		private long[] ts;
		private long[] toi;
		private boolean cpuUsageMonitored;
		private final ReentrantLock ioLock;
		private final Condition incomingDataOrTask;
		public ArrayList<Condition> tasksCompleted = new ArrayList<>();
		public ArrayList<Runnable> tasks = new ArrayList<>();
		public boolean isStarted;
		public boolean dead;

		public ThreadedBlock(Block block, AtomicInteger nbThreadedBlocksLeft, MultiCoreDiagramScheduler scheduler) {
			this.block = block;
			this.ioLock = block.getIOLock();
			this.incomingDataOrTask = this.ioLock.newCondition();
			this.scheduler = scheduler;
			this.nbThreadedBlocksLeft = nbThreadedBlocksLeft;
			setPriority(block.getThreadPriority());
			setName(block.getName());
		}

		public void onStart(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onStartTasks = this.onStartTasks;
				if (onStartTasks == null)
					onStartTasks = new ArrayList<>();
				onStartTasks.add(runnable);
				this.onStartTasks = onStartTasks;
			}
		}

		public void onResume(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onResumeTasks = this.onResumeTasks;
				if (onResumeTasks == null)
					onResumeTasks = new ArrayList<>();
				onResumeTasks.add(runnable);
				this.onResumeTasks = onResumeTasks;
			}
		}

		public void onPause(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onPauseTasks = this.onPauseTasks;
				if (onPauseTasks == null)
					onPauseTasks = new ArrayList<>();
				onPauseTasks.add(runnable);
				this.onPauseTasks = onPauseTasks;
			}
		}

		public void onStop(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onStopTasks = this.onStopTasks;
				if (onStopTasks == null)
					onStopTasks = new ArrayList<>();
				onStopTasks.add(runnable);
				this.onStopTasks = onStopTasks;
			}
		}

		public void consumeOnStartTasks() {
			consumeOnEventTasks(this.onStartTasks);
		}

		public void consumeOnResumeTasks() {
			consumeOnEventTasks(this.onResumeTasks);
		}

		public void consumeOnPauseTasks() {
			consumeOnEventTasks(this.onPauseTasks);
		}

		public void consumeOnStopTasks() {
			consumeOnEventTasks(this.onStopTasks);
		}

		private void consumeOnEventTasks(ArrayList<Runnable> tasks) {
			if (tasks != null)
				tasks.forEach(t -> triggerTask(this.block, t));
		}

		private void consumeTasks() {
			for (int i = 0; i < this.tasks.size(); i++) {
				Runnable task = this.tasks.get(i);
				Condition taskCompleted = this.tasksCompleted.get(i);
				try {
					task.run();
				} catch (Throwable e) {
					this.block.setDefaulting(true);
					System.err.println("Failed to run task of " + this.block.getName());
					e.printStackTrace();
				}
				try {
					taskCompleted.signal(); // IllegalMonitorStateException
				} catch (IllegalMonitorStateException e) {
					System.err.println("Happen to " + ProcessHandle.current().pid());
					e.printStackTrace();
				}
			}
			this.tasks.clear();
			this.tasksCompleted.clear();
		}

		public Block getBlock() {
			return this.block;
		}

		public void initCpuUsageTask() {
			if (!this.cpuUsageMonitored) {
				synchronized (this.scheduler.cpuUsageTaskLock) {
					if (MultiCoreDiagramScheduler.this.cpuUsageTask == null)
						try {
							MultiCoreDiagramScheduler.this.cpuUsageTask = CPUUsageTask.createCPUUsageTask();
						} catch (UnsupportedOperationException e) {
							System.err.println(e.getMessage());
							return;
						}
					MultiCoreDiagramScheduler.this.cpuUsageTask.addTask(getId(), this.block);
				}
				this.cpuUsageMonitored = true;
			}
		}

		public void closeCpuUsageTask() {
			if (this.cpuUsageMonitored)
				synchronized (this.scheduler.cpuUsageTaskLock) {
					if (MultiCoreDiagramScheduler.this.cpuUsageTask.removeTask(getId()))
						MultiCoreDiagramScheduler.this.cpuUsageTask = null;
					this.cpuUsageMonitored = false;
				}
		}

		@Override
		public synchronized void run() {
			this.scheduler.initOperator(this.block);
			if (this.nbThreadedBlocksLeft != null) {
				this.nbThreadedBlocksLeft.decrementAndGet();
				this.nbThreadedBlocksLeft = null;
			}
			this.isStarted = true;
			if (this.block.birthFailed())
				return;
			this.block.setThread(Thread.currentThread());
			dead: while (!canBeStopped()) {
				this.ioLock.lock();
				try {
					while (true) {
						if (canBeStopped())
							break dead;
						if (!this.tasks.isEmpty())
							consumeTasks();
						if (this.block.isReady())
							break;
						this.incomingDataOrTask.await();
					}
					int nbInput = this.block.getNbInput();
					if (this.inputs.length != nbInput) {
						this.inputs = new Object[nbInput];
						this.ts = new long[nbInput];
						this.toi = new long[nbInput];
					}
					List<BlockInput> blockInputs = this.block.getInputs();
					for (int i = 0; i < blockInputs.size(); i++) {
						IOData ioData = blockInputs.get(i).pop();
						if (ioData != null) {
							this.inputs[i] = ioData.getValue();
							this.ts[i] = ioData.getTs();
							this.toi[i] = ioData.getToi();
						} else {
							this.inputs[i] = null;
							this.ts[i] = -1;
							this.toi[i] = -1;
						}
					}
				} catch (InterruptedException e) {
					break;
				} finally {
					this.ioLock.unlock();
				}
				if (DiagramScheduler.processBlock(this.block, this.inputs, this.ts, this.toi))
					stackOutputsAndTrigger(this.block, this.block.getOutputBuff(), this.block.getOutputBuffTs(), this.scheduler.fd);
			}
			DiagramScheduler.destroyBlock(this.block, this.block.isEnable());
			if (this.cpuUsageMonitored)
				closeCpuUsageTask();
			this.ioLock.lock();
			try {
				consumeTasks();
			} finally {
				this.ioLock.unlock();
			}
		}

		private boolean canBeStopped() {
			return this.dead && (!this.block.isConsumesAllDataBeforeDying() || !this.block.isReady() && this.tasks.isEmpty());
		}

		@Override
		public String toString() {
			return "Thread of: " + this.block;
		}
	}
}