/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.Arrays;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;

public interface Trigger {

	public default boolean isAlive() {
		return true;
	};

	public default boolean isStarted() {
		return true;
	}

	public default void onStart(Object source, Runnable runnable) {
		throw new UnsupportedOperationException("not implemented");
	}

	public default void onResume(Object source, Runnable runnable) {
		throw new UnsupportedOperationException("not implemented");
	}

	public default void onPause(Object source, Runnable runnable) {
		throw new UnsupportedOperationException("not implemented");
	}

	public default void onStop(Block source, Runnable runnable) {
		throw new UnsupportedOperationException("not implemented");
	}

	public default void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	public default void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		Arrays.fill(timeStamps, timeStamp);
		triggerOutput(source, outputValues, timeStamps);
	}

	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps);

	/** Run the specified Runnable on the block's thread if it exists or immediately otherwise. This method will return only after the execution of the Runnable. It prevent any concurrent method call
	 * of birth, process, death method and the specified Runnable.
	 *
	 * @param source
	 * @param runnable */
	public default void triggerTask(Object source, Runnable runnable) {
		runnable.run();
	}
}
