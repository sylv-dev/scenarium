/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import javafx.geometry.Point2D;

public class FlowDiagramInput extends Input implements FlowDiagramIO {
	private final FlowDiagram flowDiagram;

	public FlowDiagramInput(FlowDiagram flowDiagram, Class<?> type, String name, Point2D pos) {
		super(type, name);
		this.flowDiagram = flowDiagram;
		setPosition(pos);
	}

	@Override
	public IOComponent getComponent() {
		return this.flowDiagram;
	}

	void setType(Class<?> type) {
		this.type = type;
	}

	// PK??? le nom n'est pas bon car il peut changer...
	// @Override
	// public boolean equals(Object obj) {
	// return getName().equals(((FlowDiagramInput) obj).getName()) && ((getType() == null && ((FlowDiagramInput) obj).getType() == null) || getType().equals(((FlowDiagramInput) obj).getType()));
	// }
	//
	// @Override
	// public int hashCode() {
	// return getName().hashCode();
	// }
}
