/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import javafx.geometry.Point2D;

public class FlowDiagramOutput extends Output implements FlowDiagramIO {

	public FlowDiagramOutput(Class<?> type, String name, Point2D pos) {
		super(type, name);
		setPosition(pos);
	}

	void setType(Class<?> type) {
		this.type = type;
	}
}
