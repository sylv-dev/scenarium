/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.FileChannel;
import java.util.LinkedHashMap;
import java.util.Objects;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.timescheduler.Scheduler;

public class PrimitiveData extends TimedScenario/* implements IOTypeChanger */ {
	private long nbDatas = -1;
	private DataInputStream dataDis;
	private FileChannel dataChannel;
	private FileInputStream dataFis;
	private DataInputStream indexDis;
	private FileChannel indexFc;
	private ObjectInputStream ois;
	private PropertyEditor<?> sEditor;
	private Class<?> type;
	// private final EventListenerList listeners = new EventListenerList();
	private long beginFilePointeur;
	private int cpt = 100;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	// @Override
	// public boolean needToBeScheduled() {
	// return true;
	// }

	@Override
	protected synchronized boolean close() {
		super.close();
		try {
			if (this.dataDis != null)
				this.dataDis.close();
			if (this.dataFis != null)
				this.dataFis.close();
			if (this.ois != null)
				this.ois.close();
			this.scenarioData = null;
			this.dataDis = null;
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return this.type;
	}

	// @Override
	// public long getEndTime() {
	// return (long) (getNbGap()* getPeriod());
	// }

	@Override
	public long getNbFrame() {
		if (this.nbDatas == -1 && this.file != null)
			try {
				load(this.file, false);
				close();
			} catch (IOException | ScenarioException e) {
				return -1;
			}
		return this.nbDatas;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "ppd" };
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMER_SCHEDULER;
	}

	// @Override
	// public boolean isTimeRepresentation() {
	// return false;
	// }

	@Override
	public synchronized void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		String ligne;
		try (RandomAccessFile rafData = new RandomAccessFile(scenarioFile, "r")) {
			ligne = rafData.readLine(); // TODO dangereux... il faut passer par read string plutot
			this.beginFilePointeur = rafData.getFilePointer();
		}
		FileInputStream rafDataFis = new FileInputStream(scenarioFile);
		this.dataChannel = rafDataFis.getChannel();
		try {
			if (ligne.equals("dataStructure.can.CanTrame"))
				this.type = CanTrame.class;
			else
				this.type = BeanManager.getClassFromDescriptor(ligne);
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(this.type, "");
			this.file = scenarioFile;
			this.dataDis = new DataInputStream(rafDataFis);
			this.sEditor = editor;
			int pitch = this.sEditor.getPitch();
			if (pitch != -1)
				this.nbDatas = (scenarioFile.length() - this.beginFilePointeur) / pitch;
			else {
				FileInputStream rafIndexFis = getFileIndexInputStream(scenarioFile);
				this.indexFc = rafIndexFis.getChannel();
				this.indexDis = new DataInputStream(rafIndexFis);
				this.nbDatas = new File(getFileIndexPath(scenarioFile.getAbsolutePath())).length() / Long.BYTES;
			}
			this.dataChannel.position(this.beginFilePointeur);
			this.scenarioData = this.sEditor.readValue(this.dataDis);
		} catch (IOException | ClassNotFoundException e) {
			if (e instanceof IOException)
				System.err.println("Error while loading: " + scenarioFile + "\nCaused by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
			else
				System.err.println("Cannot find the class: " + ligne);
		}
		fireLoadChanged();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {}

	@Override
	public synchronized void process(Long timePointer) throws IOException, ClassNotFoundException, ScenarioException {
		long frame = (long) (timePointer / getPeriod());
		try {
			if (frame >= this.nbDatas) {
				this.scenarioData = null;
				return;
			}
			if (!this.dataChannel.isOpen() || this.indexFc != null && !this.indexFc.isOpen()) {
				this.dataChannel.close();
				FileInputStream rafDataFis = new FileInputStream(this.file);
				this.dataChannel = rafDataFis.getChannel();
				if (this.ois == null) {
					this.dataDis = new DataInputStream(rafDataFis);
					if (this.indexFc != null) {
						this.indexFc.close();
						FileInputStream rafIndexFis = getFileIndexInputStream(this.file);
						this.indexFc = rafIndexFis.getChannel();
						this.indexDis = new DataInputStream(rafIndexFis);
					}
				} else {
					this.indexDis = new DataInputStream(getFileIndexInputStream(this.file));
					this.dataFis = rafDataFis;
					this.dataFis.skip(this.beginFilePointeur);
					this.ois = new ObjectInputStream(this.dataFis);
				}
			}
			if (this.indexDis == null) {
				this.dataChannel.position(this.beginFilePointeur + frame * this.sEditor.getPitch());
				this.scenarioData = this.sEditor.readValue(this.dataDis);// ((PrimitiveCell) scenarioData).setValue(sEditor.readValue(raf));
			} else {
				this.indexFc.position(frame * Long.BYTES);
				if (this.ois == null) { // Pour les objects flux
					this.dataChannel.position(this.indexDis.readLong());
					this.scenarioData = this.sEditor.readValue(this.dataDis);
				} else { // Pour les objects flux
					this.dataFis.getChannel().position(this.indexDis.readLong());
					this.scenarioData = this.ois.readUnshared();
					// Memory leak sinon...
					if (this.cpt == 0) {
						this.ois.close();
						this.dataFis = new FileInputStream(this.file);
						this.dataFis.skip(this.beginFilePointeur);
						this.dataChannel = this.dataFis.getChannel();
						this.ois = new ObjectInputStream(this.dataFis);
						this.cpt = 100;
					} else
						this.cpt--;
				}
			}
		} catch (ClosedByInterruptException e) {
			// death();
			// load(file, false); //Pk c'est normal une ClosedByInterruptException si interruptedException...
		} catch (EOFException e) {
			e.printStackTrace();
		}
	}

	private static FileInputStream getFileIndexInputStream(File file) throws FileNotFoundException {
		return new FileInputStream(getFileIndexPath(file.getAbsolutePath()));
	}

	private static String getFileIndexPath(String filePath) {
		return filePath.substring(0, filePath.lastIndexOf(".") + 1) + "pindex";
	}

	@Override
	public void save(File file) throws IOException {
		return;
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		if (file == null) {
			super.setFile(null);
			return;
		}
		Class<?> newType = null;
		try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
			String ligne = raf.readLine();
			try {
				newType = BeanManager.getClassFromDescriptor(ligne);
			} catch (ClassNotFoundException e) {
				if (ligne.equals("dataStructure.can.CanTrame"))
					newType = CanTrame.class;
				else
					System.err.println("Cannot find the class: " + ligne + " indicated in the primitive data file: " + file.getCanonicalPath());
			}
		} catch (IOException ex) {
			if (!file.toString().isEmpty())
				System.err.println("Error while setting file: " + file + " caused by: " + ex.getClass().getSimpleName());
		}
		boolean isNewType = false;
		if (newType != this.type) {
			this.type = newType;
			isNewType = true;
		}
		if (isNewType)
			try {
				initStruct();
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Cannot init the struct, " + file.getName());
			}
		super.setFile(file);
	}
}
