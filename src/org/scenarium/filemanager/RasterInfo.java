/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.scenarium.struct.raster.ByteRaster;

public class RasterInfo {

	private RasterInfo() {}

	public static void populate(HashMap<String, String> info, ByteRaster raster) {
		info.put("Horizontal frame size", Integer.toString(raster.getWidth()));
		info.put("Vertical frame size", Integer.toString(raster.getHeight()));
		info.put("Channels sequence", raster.getStringType());
	}

	public static void populate(LinkedHashMap<String, String> info, BufferedImage bufferedImage) {
		info.put("Horizontal frame size", Integer.toString(bufferedImage.getWidth()));
		info.put("Vertical frame size", Integer.toString(bufferedImage.getHeight()));
		info.put("Channels sequence", bufferedImage.getType() == BufferedImage.TYPE_BYTE_GRAY ? "Gray" : bufferedImage.getType() == BufferedImage.TYPE_3BYTE_BGR ? "BGR" : "RGB");

	}
}
