/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.playlist;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingWorker;

class PlayListAccessory extends JPanel implements PropertyChangeListener {
	private static final long serialVersionUID = 1L;
	private final int width = 300;
	private final int height = 200;
	private SwingWorker<Integer, String> sw;
	private JScrollPane scrollPane;

	public PlayListAccessory(final JFileChooser chooser) {
		chooser.addPropertyChangeListener(this);
		setPreferredSize(new Dimension(this.width, this.height));
	}

	@Override
	public void propertyChange(PropertyChangeEvent changeEvent) {
		if (changeEvent.getPropertyName().equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
			if (this.sw != null && !this.sw.isDone())
				this.sw.cancel(false);
			removeAll();
			revalidate();
			repaint();
			final File file = (File) changeEvent.getNewValue();
			if (file != null && !file.isDirectory()) {
				this.sw = new SwingWorker<>() {
					protected Integer doInBackground() {
						File workFile = file;
						String fileName = workFile.getAbsolutePath();
						String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
						JTextArea computeTextArea = new JTextArea();
						if (fileExt.equals(PlayListManager.ext))
							try {
								for (String line : Files.readAllLines(workFile.toPath()))
									computeTextArea.append(line + "\n");
							} catch (IOException e) {}
						if (isCancelled())
							return null;
						computeTextArea.setEditable(false);
						computeTextArea.setBorder(null);
						PlayListAccessory.this.scrollPane = new JScrollPane(computeTextArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
						PlayListAccessory.this.scrollPane.setPreferredSize(getPreferredSize());
						PlayListAccessory.this.scrollPane.setPreferredSize(new Dimension((int) getPreferredSize().getWidth(), (int) getPreferredSize().getHeight() - 10));
						add(PlayListAccessory.this.scrollPane);
						revalidate();
						repaint();
						return null;
					}
				};
				this.sw.execute();
			}
		}
	}
}
