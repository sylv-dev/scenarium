/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.playlist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import javax.swing.event.EventListenerList;

import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;

public class PlayListManager {
	public static String ext = "ppl";
	private final ArrayList<Object> playList = new ArrayList<>();
	private final EventListenerList listeners = new EventListenerList();

	public void addAll(int index, Collection<Object> scenarioSources) {
		for (Object scenarioSource : scenarioSources)
			index = addInternalToPlayList(index, scenarioSource);
		firePlayListChanged();
	}

	private int addInternalToPlayList(int index, Object scenarioSource) {
		if (index == -1)
			index = this.playList.size();
		if (scenarioSource instanceof File) {
			LinkedList<File> ll = new LinkedList<>();
			ll.push((File) scenarioSource);
			LinkedHashSet<File> toAdd = new LinkedHashSet<>();
			while (!ll.isEmpty()) {
				File file = ll.pop();
				if (file.isDirectory()) {
					File[] lf = file.listFiles();
					if (lf != null)
						ll.addAll(Arrays.asList(lf));
				} else {
					int type = DataLoader.getFileType(file);
					if (type == DataLoader.SCENARIO) {
						File descFile = ScenarioManager.getDescriptorFromScenario(file);
						if (descFile == null)
							continue;
						toAdd.add(ScenarioManager.getDescriptorFromScenario(file));
					} else if (type == DataLoader.PLAYLIST)
						try {
							index = internalLoad(file, index);
						} catch (IOException e) {}
				}
			}
			if (index < 0)
				index = 0;
			if (index > this.playList.size())
				index = this.playList.size();
			for (File file : toAdd)
				this.playList.add(index++, file);
		} else
			this.playList.add(index, scenarioSource);
		return index;
	}

	public void addPlayListChangeListener(PlayListChangeListener listener) {
		this.listeners.add(PlayListChangeListener.class, listener);
	}

	public void addPlayListElements(int index, Collection<PlayListElement> playListElements) {
		for (PlayListElement playListElement : playListElements)
			addInternalToPlayList(index + playListElement.index, playListElement.scenarioDesc.scenarioSource);
		firePlayListChanged();
	}

	public void addToPlayList(int index, Object scenarioSource) {
		addInternalToPlayList(index, scenarioSource);
		firePlayListChanged();
	}

	public void clear() {
		this.playList.clear();
		firePlayListChanged();
	}

	private void firePlayListChanged() {
		for (PlayListChangeListener listener : this.listeners.getListeners(PlayListChangeListener.class))
			listener.playListChange();
	}

	public ArrayList<Object> getPlayList() {
		return new ArrayList<>(this.playList);
	}

	public Object goToFirstScenario() {
		return this.playList.get(0);
	}

	public Object goToNextScenario(Object scenario, boolean reverse) {
		int index = -1;
		for (int i = 0; i < this.playList.size(); i++)
			if (scenario == this.playList.get(i)) { // Pas de equals!!! Je peux avoir deux scenarios identiques l'un à la suite de l'autre
				index = i;
				break;
			}
		if (index == -1)
			return null;
		if (!reverse) {
			if (index >= this.playList.size() - 1)
				return null;
			index++;
		} else {
			if (index <= 0)
				return null;
			index--;
		}
		return this.playList.get(index);
	}

	private int internalLoad(File playListFile, int index) throws IOException {
		for (String line : Files.readAllLines(playListFile.toPath(), StandardCharsets.UTF_8))
			index = addInternalToPlayList(index, new File(line));
		// BufferedReader br = new BufferedReader(new FileReader(playListFile));
		// String ligne;
		// while ((ligne = br.readLine()) != null)
		// index = addInternalToPlayList(index, new File(ligne));
		// br.close();
		return index;
	}

	public void load(File file, int index) throws IOException {
		this.playList.clear();
		if (DataLoader.getFileType(file) == DataLoader.PLAYLIST)
			internalLoad(file, index);
		else
			addInternalToPlayList(-1, file);
		firePlayListChanged();
	}

	public void remove(int index, boolean firePlayListChanged) {
		this.playList.remove(index);
		if (firePlayListChanged)
			firePlayListChanged();
	}

	public void remove(Object scenarioSource, boolean firePlayListChanged) {
		this.playList.removeIf(obj -> obj == scenarioSource);
		if (firePlayListChanged)
			firePlayListChanged();
	}

	public void removeAll(Collection<Object> toRemove) {
		for (Object scenarioSource : toRemove)
			this.playList.removeIf(obj -> obj == scenarioSource);
		firePlayListChanged();
	}

	public void removeMissingFile() {
		Iterator<Object> iterator = this.playList.iterator();
		while (iterator.hasNext())
			if (!ScenarioManager.isAvailable(iterator.next()))
				iterator.remove();
		firePlayListChanged();
	}

	public void removePlayListChangeListener(PlayListChangeListener listener) {
		this.listeners.remove(PlayListChangeListener.class, listener);
	}

	public void reset(Object scenarioSource) {
		this.playList.clear();
		if (scenarioSource != null)
			this.playList.add(scenarioSource);
		firePlayListChanged();
	}

	public void reverse() {
		Collections.reverse(this.playList);
		firePlayListChanged();
	}

	public void save(File playListFile) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(playListFile))) {
			for (Object playListElement : this.playList) {
				bw.write(ScenarioManager.getPath(playListElement));
				bw.newLine();
			}
		}
	}

	public void shuffle() {
		Collections.shuffle(this.playList);
		firePlayListChanged();
	}

	public int size() {
		return this.playList.size();
	}

	public void sortByName() {
		Collections.sort(this.playList, (f1, f2) -> f1.toString().toLowerCase().compareTo(f2.toString().toLowerCase()));
		firePlayListChanged();
	}

	public void sortByPath() {
		Collections.sort(this.playList, (f1, f2) -> ScenarioManager.getPath(f1).toLowerCase().compareTo(ScenarioManager.getPath(f2).toLowerCase()));
		firePlayListChanged();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + this.playList.toString();
	}
}
