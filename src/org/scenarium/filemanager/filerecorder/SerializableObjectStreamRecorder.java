/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;

import org.beanmanager.BeanManager;

//TODO toujouts utile pour les objets uniquement serializable
public class SerializableObjectStreamRecorder extends FileStreamRecorder {
	private ObjectOutputStream oos;
	private DataOutputStream pitchDos;
	private DataOutputStream dataDos;
	private File file;
	private FileChannel channel;

	public SerializableObjectStreamRecorder() {}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (this.pitchDos != null)
			try {
				this.pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		if (this.oos != null)
			try {
				this.oos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		this.channel = null;
		if (exception != null)
			throw exception;
	}

	@Override
	public File getFile() {
		if (this.file == null && this.recordPath != null)
			this.file = new File(this.recordPath.endsWith(".ppsr") ? this.recordPath : this.recordPath + File.separator + this.recordName + ".ppd");
		return this.file;
	}

	@Override
	public void pop(Object value) throws IOException, FileNotFoundException {
		if (this.oos == null) {
			this.file = getFile();
			String filePath = this.file.getAbsolutePath().substring(0, this.file.getAbsolutePath().length() - 4);
			FileOutputStream fos = new FileOutputStream(this.file);
			this.channel = fos.getChannel();
			this.dataDos = new DataOutputStream(fos);
			// StringEditor.writeString(dataDos, value.getClass().getName()); //TODO je dois faire ca normalement...
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(value.getClass()) + System.lineSeparator());
			this.oos = new ObjectOutputStream(this.dataDos);
			this.pitchDos = new DataOutputStream(new FileOutputStream(new File(filePath + ".pindex")));
		}
		if (this.pitchDos != null)
			this.pitchDos.writeLong(this.channel.position()); // dataDos.size() marche, pas, il est en int...
		this.oos.writeUnshared(value);
		this.oos.reset();
		this.timePointer++;
	}
}
