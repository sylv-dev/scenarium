/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditor;

public class PropertyStreamRecorder extends FileStreamRecorder {
	private final PropertyEditor<?> streamableEditor;
	private DataOutputStream dataDos;
	private DataOutputStream pitchDos;
	private File file;
	private FileChannel channel;

	public PropertyStreamRecorder(PropertyEditor<?> streamableEditor) {
		this.streamableEditor = streamableEditor;
	}

	@Override
	public void close() {
		if (this.dataDos != null)
			try {
				this.dataDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (this.pitchDos != null)
			try {
				this.pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		this.channel = null;
	}

	@Override
	public File getFile() {
		if (this.file == null && this.recordPath != null)
			this.file = new File(this.recordPath.endsWith(".ppsr") ? this.recordPath : this.recordPath + File.separator + this.recordName + ".ppd");
		return this.file;
	}

	@Override
	public void pop(Object object) throws IOException, FileNotFoundException {
		if (this.dataDos == null) {
			this.file = getFile();
			FileOutputStream fos = new FileOutputStream(this.file);
			this.channel = fos.getChannel();
			this.dataDos = new DataOutputStream(fos);
			// StringEditor.writeString(dataDos, object.getClass().getName()); //TODO je dois faire ca normalement...
			this.dataDos.writeBytes(BeanManager.getDescriptorFromClass(object.getClass()) + System.lineSeparator());// dépend des plateformes!!!!
			if (this.streamableEditor.getPitch() <= 0)
				this.pitchDos = new DataOutputStream(new FileOutputStream(this.file.getAbsolutePath().substring(0, this.file.getAbsolutePath().length() - 4) + ".pindex"));
		}
		if (this.pitchDos != null)
			this.pitchDos.writeLong(this.channel.position()); // dataDos.size() marche, pas, il est en int...

		this.streamableEditor.writeValueFromObj(this.dataDos, object);
		this.timePointer++;
	}
}
