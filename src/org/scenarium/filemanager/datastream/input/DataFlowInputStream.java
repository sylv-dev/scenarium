/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.input;

import java.io.DataInput;
import java.io.IOException;

public abstract class DataFlowInputStream<T> {
	public DataInput dataInput;

	public abstract T pop() throws IOException;

	public void setDataInput(DataInput dataInput) {
		this.dataInput = dataInput;
	}
}
