/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.event.EventListenerList;

import org.scenarium.filemanager.scenario.DataFlowDiagram;
import org.scenarium.filemanager.scenario.Image;
import org.scenarium.filemanager.scenario.PrimitiveData;
import org.scenarium.filemanager.scenario.Raw;
import org.scenarium.filemanager.scenario.ScenariumScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;

public class ScenarioManager {
	private static HashMap<String, Class<? extends Scenario>> scenarioManager = new HashMap<>();
	private static final EventListenerList SCENARIO_LISTENERS = new EventListenerList();

	static {
		registerScenario(Image.class, new Image().getReaderFormatNames());
		registerScenario(Raw.class, new Raw().getReaderFormatNames());
		registerScenario(DataFlowDiagram.class, new DataFlowDiagram().getReaderFormatNames());
		registerScenario(ScenariumScheduler.class, new ScenariumScheduler().getReaderFormatNames());
		registerScenario(PrimitiveData.class, new PrimitiveData().getReaderFormatNames());
		// registerScenario(new VideoJCodec().getReaderFormatNames(), VideoJCodec.class);
	}

	private ScenarioManager() {}

	public static void registerScenario(Class<? extends Scenario> scenarioClass, String[] extensions) {
		boolean added = false;
		for (String ext : extensions)
			added |= scenarioManager.putIfAbsent(ext.toLowerCase(), scenarioClass) == null;
		if (added) {
			OperatorManager.addInternOperator(scenarioClass);
			fireScenarioLoaded(scenarioClass);
		}
	}

	public static void replaceScenario(Class<? extends Scenario> scenarioClass, String[] extensions) {
		if (extensions.length != 0) {
			for (String ext : extensions)
				scenarioManager.put(ext.toLowerCase(), scenarioClass);
			fireScenarioLoaded(scenarioClass);
		}
	}

	public static void purgeScenarios(Module module) {
		for (Iterator<Class<? extends Scenario>> iterator = scenarioManager.values().iterator(); iterator.hasNext();) {
			Class<? extends Scenario> scenario = iterator.next();
			if (scenario.getModule().equals(module)) {
				iterator.remove();
				fireScenarioUnloaded(scenario);
			}
		}
	}

	public static void addLoadScenarioListener(LoadScenarioListener listener) {
		SCENARIO_LISTENERS.add(LoadScenarioListener.class, listener);
	}

	public static void removeLoadScenarioListener(LoadScenarioListener listener) {
		SCENARIO_LISTENERS.remove(LoadScenarioListener.class, listener);
	}

	private static void fireScenarioLoaded(Class<? extends Scenario> scenarioClass) {
		for (LoadScenarioListener listener : SCENARIO_LISTENERS.getListeners(LoadScenarioListener.class))
			listener.loaded(scenarioClass);
	}

	private static void fireScenarioUnloaded(Class<? extends Scenario> scenarioClass) {
		for (LoadScenarioListener listener : SCENARIO_LISTENERS.getListeners(LoadScenarioListener.class))
			listener.unloaded(scenarioClass);
	}

	@SuppressWarnings("unchecked")
	public static Scenario createScenario(Object source)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Scenario scenario = null;
		if (source instanceof Class<?>)
			scenario = ((Class<? extends LocalScenario>) source).getConstructor().newInstance();
		else {
			Class<? extends Scenario> scenarioType = getScenarioType(source);
			if (scenarioType == null)
				return null;
			scenario = scenarioType.getConstructor().newInstance();
			scenario.setSource(source);
		}
		return scenario;
	}

	public static ScenarioDescriptor getDescriptor(Object sc) {
		return new ScenarioDescriptor(getSourceType(sc).getSimpleName(), getPath(sc));
	}

	public static File getDescriptorFromScenario(File scenarioDescriptor) {
		String fileName = scenarioDescriptor.getName();
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, scenarioDescriptor.getName().length());
		if (fileExt.equals("raw")) {
			File infFile = new File(scenarioDescriptor.getAbsolutePath().substring(0, scenarioDescriptor.getAbsolutePath().length() - 3).concat("inf"));
			return infFile.exists() ? infFile : null;
		} else if (fileExt.equals("inf")) {
			File rawFile = new File(scenarioDescriptor.getAbsolutePath().substring(0, scenarioDescriptor.getAbsolutePath().length() - 3).concat("raw"));
			return rawFile.exists() ? scenarioDescriptor : null;
		} else
			return scenarioDescriptor;
	}

	public static String getErrorMessage(Throwable e) {
		if (e instanceof FileNotFoundException)
			return "Missing file: " + e.getMessage();
		else if (e instanceof IOException)
			return "Corrupt scenario file: " + e.getMessage();
		else if (e instanceof OutOfMemoryError)
			return "Not enough memory for the scenario: ";
		else if (e instanceof ScenarioException)
			return "Scenario format error: " + e.getMessage();
		else if (e instanceof InstantiationException)
			return "Cannot instantiate the scenario: " + e.getMessage();
		else if (e instanceof IllegalAccessException)
			return "Cannot access to the scenario: " + e.getMessage();
		return null;
	}

	public static int getLenght(Object source) {
		if (source instanceof File)
			return LocalScenario.getScenarioLenght((File) source);
		else if (source instanceof StreamScenarioSource)
			return -1;
		return 0;
	}

	public static String getPath(Object source) {
		if (source instanceof File)
			return LocalScenario.getScenarioPath((File) source);
		else if (source instanceof StreamScenarioSource)
			return StreamScenario.getScenarioPath((StreamScenarioSource) source);
		return null;
	}

	public static String[] getReaderFormatNames() {
		ArrayList<String> readerFormatNames = new ArrayList<>();
		scenarioManager.values().stream().distinct().forEach(sc -> {
			try {
				String[] formats = sc.getConstructor().newInstance().getReaderFormatNames();
				StringBuilder rf = new StringBuilder(sc.getSimpleName());
				for (String format : formats)
					rf.append(" " + format);
				readerFormatNames.add(rf.toString());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		});
		return readerFormatNames.toArray(new String[readerFormatNames.size()]);
	}

	public static HashSet<Class<? extends Scenario>> getRegisterScenario() {
		return new HashSet<>(scenarioManager.values());
	}

	public static Class<? extends Scenario> getScenarioType(Object source) {
		if (source instanceof File) {
			String ap = ((File) source).getAbsolutePath();
			return scenarioManager.get(ap.substring(ap.lastIndexOf(".") + 1).toLowerCase());
		} else if (source instanceof StreamScenarioSource)
			return ((StreamScenarioSource) source).type;
		return null;
	}

	// TODO to remove
	public static Object getSource(String type, String value) {
		Object scenarioSource = LocalScenario.getScenarioSource(type, value);
		if (scenarioSource == null)
			scenarioSource = StreamScenario.getScenarioSource(type, value);
		return scenarioSource;
	}

	public static Class<?> getSourceType(Object source) {
		if (source instanceof File)
			return File.class;
		else if (source instanceof StreamScenarioSource)
			return ((StreamScenarioSource) source).type;
		return null;
	}

	public static HashSet<Class<? extends StreamScenario>> getStreamScenario() {
		HashSet<Class<? extends StreamScenario>> streamScenarios = new HashSet<>();
		for (Class<?> type : scenarioManager.values())
			if (StreamScenario.class.isAssignableFrom(type))
				streamScenarios.add(type.asSubclass(StreamScenario.class));
		return streamScenarios;
	}

	public static boolean isAvailable(Object source) {
		if (source instanceof File)
			return LocalScenario.isAvailable((File) source);
		else if (source instanceof StreamScenarioSource)
			return StreamScenario.isAvailable((StreamScenarioSource) source);
		return false;
	}
}
