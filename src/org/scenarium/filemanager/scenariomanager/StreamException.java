/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

public class StreamException extends Exception {
	private static final long serialVersionUID = 1L;

	public StreamException(String message, Throwable cause) {
		super(message, cause);
	}
}
