/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.StringTokenizer;

public class FileWatcher {

	private FileWatcher() {}

	public static void main(String[] args) {
		try {
			WatchService watcher = FileSystems.getDefault().newWatchService();
			Path dir = Paths.get("/home/revilloud/temp");
			dir.register(watcher, ENTRY_CREATE);
			while (true) {
				WatchKey key;
				try {
					key = watcher.take();
				} catch (InterruptedException ex) {
					return;
				}

				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> kind = event.kind();
					if (kind == ENTRY_CREATE) {
						@SuppressWarnings("unchecked")
						Path fileName = dir.resolve(((WatchEvent<Path>) event).context());
						System.out.println(Arrays.toString(readFile(fileName)));
					}
				}

				boolean valid = key.reset();
				if (!valid)
					break;
			}

		} catch (IOException ex) {
			System.err.println(ex);
		}
	}

	private static float[] readFile(Path fileName) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName.toFile()), "UTF8"))) {
			String line;
			int i = 0;
			while ((line = br.readLine()) != null)
				if (i++ == 4) {
					StringTokenizer st = new StringTokenizer(line);
					st.nextToken();
					st.nextToken();
					float max = Float.parseFloat(st.nextToken());
					st.nextToken();
					st.nextToken();
					float freq = Float.parseFloat(st.nextToken());
					// System.out.println("max: " + max + " f: " + freq);
					return new float[] { max, freq };
				}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}
}
