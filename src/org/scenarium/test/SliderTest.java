/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import org.scenicview.ScenicView;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SliderTest extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Slider slider = new Slider(0, 100, 50);
		slider.setShowTickLabels(false);
		slider.setShowTickMarks(true);
		slider.setMajorTickUnit(10);
		slider.setPadding(new Insets(0, 0, -5, 0));
		Image image1 = new Image(getClass().getResourceAsStream("/A.png"));
		ImageView imageView1 = new ImageView(image1);
		Image image2 = new Image(getClass().getResourceAsStream("/B.png"));
		ImageView imageView2 = new ImageView(image2);
		slider.widthProperty().addListener(e -> {
			double thumbWidth = slider.lookup(".thumb").getLayoutBounds().getWidth();
			double halfImageWidth = image1.getWidth() / 2.0;
			double trackLength = slider.getWidth() - thumbWidth;
			double trackStart = slider.getTranslateX() + thumbWidth / 2.0;
			imageView1.setTranslateX(trackStart - halfImageWidth + trackLength * 1);
			imageView2.setTranslateX(trackStart - halfImageWidth * 3 + trackLength * 0.5);
			System.out.println("d1: " + imageView1.getTranslateX() + " d2: " + imageView2.getTranslateX());
		});
		Scene scene = new Scene(new VBox(0, slider, new HBox(imageView1, imageView2)));
		primaryStage.setScene(scene);
		primaryStage.setWidth(640);
		primaryStage.setHeight(480);
		primaryStage.show();
		ScenicView.show(scene);
	}
}
