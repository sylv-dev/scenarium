/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import java.awt.image.BufferedImage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

//Workaround: -Djdk.gtk.version=2		related to: JDK-8156779
public class Fx11Warning extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		BufferedImage bi = new BufferedImage(100, 100, BufferedImage.TYPE_3BYTE_BGR);
		bi.getGraphics(); // Warning
		stage.setScene(new Scene(new Button("patate"), 200, 200));
		stage.show();
	}
}
