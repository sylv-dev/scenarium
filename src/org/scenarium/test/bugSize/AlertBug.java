/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class AlertBug extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene s = new Scene(new Button("cool"), 320, 240);
		primaryStage.setScene(s);
		primaryStage.show();
		ButtonType b1 = new ButtonType("patate", ButtonData.FINISH);
		Alert at = new Alert(AlertType.ERROR, "cool", b1, new ButtonType("patate2", ButtonData.CANCEL_CLOSE));
		at.show();
		new Thread(() -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Platform.runLater(() -> {
				at.close();
				System.out.println("close");
				Platform.runLater(() -> at.close());
			});
		}).start();
	}

}
