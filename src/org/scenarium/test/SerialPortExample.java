/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import net.sf.marineapi.nmea.event.SentenceEvent;
import net.sf.marineapi.nmea.event.SentenceListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.sentence.GGASentence;
import net.sf.marineapi.nmea.sentence.GLLSentence;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.sentence.SentenceValidator;

public class SerialPortExample implements SentenceListener {
	public static void main(String[] args) {
		new SerialPortExample();
	}

	public SerialPortExample() {
		init();
	}

	private static SerialPort getSerialPort() {
		try {
			Enumeration<?> e = CommPortIdentifier.getPortIdentifiers();
			while (e.hasMoreElements()) {
				CommPortIdentifier id = (CommPortIdentifier) e.nextElement();
				if (id.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					SerialPort sp = (SerialPort) id.open("SerialExample", 30);
					sp.setSerialPortParams(4800, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
					@SuppressWarnings("resource")
					InputStream is = sp.getInputStream();
					@SuppressWarnings("resource")
					InputStreamReader isr = new InputStreamReader(is);
					@SuppressWarnings("resource")
					BufferedReader buf = new BufferedReader(isr);
					System.out.println("Scanning port " + sp.getName());
					// try each port few times before giving up
					for (int i = 0; i < 5; i++)
						try {
							String data = buf.readLine();
							if (SentenceValidator.isValid(data)) {
								System.out.println("NMEA data found!");
								return sp;
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					is.close();
					isr.close();
					buf.close();
				}
			}
			System.out.println("NMEA data was not found..");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void init() {
		try {
			SerialPort sp = getSerialPort();
			if (sp != null) {
				InputStream is = sp.getInputStream();
				SentenceReader sr = new SentenceReader(is);
				sr.addSentenceListener(this);
				sr.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void readingPaused() {
		System.out.println("-- Paused --");
	}

	@Override
	public void readingStarted() {
		System.out.println("-- Started --");
	}

	@Override
	public void readingStopped() {
		System.out.println("-- Stopped --");
	}

	@Override
	public void sentenceRead(SentenceEvent event) {
		Sentence s = event.getSentence();
		if ("GLL".equals(s.getSentenceId())) {
			GLLSentence gll = (GLLSentence) s;
			System.out.println("GLL position: " + gll.getPosition());
		} else if ("GGA".equals(s.getSentenceId())) {
			GGASentence gga = (GGASentence) s;
			System.out.println("GGA position: " + gga.getPosition());
		}
		System.out.println(event.getSentence());
	}
}
