/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import java.text.DecimalFormat;
import java.util.Set;

import javax.vecmath.Point2i;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class LineChartTest extends Application {
	private static double[][] datas;

	static {
		datas = new double[3][10000];
		for (int n = 0; n < datas[0].length; n++) {
			datas[0][n] = n / 1000.0;
			datas[1][n] = Math.exp(n / 1000.0);
			datas[2][n] = n / 1000.0;
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	private Data<Number, Number> first;
	private Data<Number, Number> last;
	private Canvas chartCanvas;
	private Canvas selectionCanvas;
	private Region drawRegion;
	private double ratioX;
	private double ratioY;
	private double offsetX;
	private double offsetY;
	private Point2i selectedPoint;
	private Point2i draggPoint;
	private NumberAxis xAxis;
	private NumberAxis yAxis;
	private double minX;
	private double maxX;
	private double minY;
	private double maxY;
	private double minZ;

	private double maxZ;

	private void computeBounds() {
		double[] xs = datas[0];
		double[] ys = datas[1];
		double minX = xs[0];
		double maxX = minX;
		double minY = ys[0];
		double maxY = minY;
		if (datas.length == 2)
			for (int i = xs.length - 1; i-- != 0;) {
				double val = xs[i];
				if (val < minX)
					minX = val;
				else if (val > maxX)
					maxX = val;
				val = ys[i];
				if (val < minY)
					minY = val;
				else if (val > maxY)
					maxY = val;
			}
		else {
			double[] zs = datas[2];
			double minZ = zs[0];
			double maxZ = minZ;
			for (int i = xs.length - 1; i-- != 0;) {
				double val = xs[i];
				if (val < minX)
					minX = val;
				else if (val > maxX)
					maxX = val;
				val = ys[i];
				if (val < minY)
					minY = val;
				else if (val > maxY)
					maxY = val;
				val = zs[i];
				if (val < minZ)
					minZ = val;
				else if (val > maxZ)
					maxZ = val;
			}
			this.minZ = minZ;
			this.maxZ = maxZ;
		}
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	private void drawChart() {
		GraphicsContext g = this.chartCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, this.chartCanvas.getWidth(), this.chartCanvas.getHeight());
		double[] xDatas = datas[0];
		double[] yDatas = datas[1];
		Color color = Color.LIME;
		double oldX = (xDatas[xDatas.length - 1] - this.offsetX) * this.ratioX;
		double oldY = (yDatas[xDatas.length - 1] - this.offsetY) * this.ratioY;
		if (datas.length == 2) {
			g.setStroke(Color.LIME);
			for (int i = xDatas.length - 1; i-- != 0;) {
				double x = (xDatas[i] - this.offsetX) * this.ratioX;
				double y = (yDatas[i] - this.offsetY) * this.ratioY;
				g.strokeLine(oldX, oldY, x, y);
				oldX = x;
				oldY = y;
			}
		} else {
			// float value = (float) values[row][column];
			// if (value < 0)
			// value = 0;
			// return new Color(color.getRed() / 255.0f * value, color.getGreen() / 255.0f * value, color.getBlue() / 255.0f * value);
			double[] zDatas = datas[2];
			double offsetZ = this.minZ;
			double ratioZ = 1 / (this.maxZ - this.minZ);
			double red = color.getRed();
			double green = color.getGreen();
			double blue = color.getBlue();
			for (int i = xDatas.length - 1; i-- != 0;) {
				double x = (xDatas[i] - this.offsetX) * this.ratioX;
				double y = (yDatas[i] - this.offsetY) * this.ratioY;
				double z = (zDatas[i] - offsetZ) * ratioZ;
				if (z < 0)
					z = 0;
				else if (z > 1)
					z = 1;
				g.setStroke(new Color(z * red, z * green, z * blue, 1));
				g.strokeLine(oldX, oldY, x, y);
				oldX = x;
				oldY = y;
			}
		}
	}

	private void drawSelection() {
		GraphicsContext g = this.selectionCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, this.selectionCanvas.getWidth(), this.selectionCanvas.getHeight());
		if (this.selectedPoint != null) {
			g.setStroke(Color.BLACK);
			g.strokeRect(this.selectedPoint.getX() - 0.5, this.selectedPoint.getY() - 0.5, this.draggPoint.getX() - this.selectedPoint.getX(), this.draggPoint.getY() - this.selectedPoint.getY());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) {
		computeBounds();
		// Création des séries.
		final LineChart.Series<Number, Number> series = new LineChart.Series<>();
		series.setName("cool");
		this.first = new LineChart.Data<>(this.minX, this.minY);
		this.last = new LineChart.Data<>(this.minX == this.maxX ? this.maxX + 1 : this.maxX, this.minY == this.maxY ? this.maxY + 1 : this.maxY);
		series.getData().addAll(this.first, this.last);
		// Création du graphique.
		this.xAxis = new NumberAxis(this.first.getXValue().doubleValue(), this.last.getXValue().doubleValue(), 1);
		// xAxis.setAutoRanging(true);
		this.xAxis.setLabel("x");
		this.xAxis.setLowerBound(this.first.getXValue().doubleValue());
		this.xAxis.setUpperBound(this.last.getXValue().doubleValue());
		this.yAxis = new NumberAxis(this.first.getYValue().doubleValue(), this.last.getYValue().doubleValue(), 1);
		// yAxis.setAutoRanging(true);
		this.yAxis.setLabel("y");
		this.yAxis.setLowerBound(this.first.getYValue().doubleValue());
		this.yAxis.setUpperBound(this.last.getYValue().doubleValue());
		updateTickUnit();

		final LineChart<Number, Number> chart = new LineChart<>(this.xAxis, this.yAxis);
		chart.setAnimated(false);
		// chart.setCreateSymbols(false);
		chart.setTitle("Fonction puissance");
		chart.getData().add(series);
		chart.setStyle("CHART_COLOR_1: transparent;");

		Set<Node> items = chart.lookupAll("Label.chart-legend-item");
		for (Node item : items) {
			Label label = (Label) item;
			Line line = new Line(0, 0, 6, 0);
			line.setStroke(Color.MEDIUMSEAGREEN);
			line.setStrokeWidth(2);
			label.setGraphic(line);
		}
		Pane drawPane = (Pane) chart.lookup(".chart-content");
		drawPane.setPadding(new Insets(5, 20, 0, 0));
		this.drawRegion = (Region) drawPane.getChildren().get(0);

		chart.setPadding(new Insets(0));
		final StackPane root = new StackPane();
		this.chartCanvas = new Canvas();
		this.chartCanvas.setManaged(false);
		this.chartCanvas.widthProperty().bind(this.drawRegion.widthProperty());
		this.chartCanvas.heightProperty().bind(this.drawRegion.heightProperty());
		this.selectionCanvas = new Canvas();
		this.selectionCanvas.setManaged(false);
		this.selectionCanvas.widthProperty().bind(this.drawRegion.widthProperty());
		this.selectionCanvas.heightProperty().bind(this.drawRegion.heightProperty());
		this.selectionCanvas.setVisible(false);
		this.selectionCanvas.setFocusTraversable(false);
		this.selectionCanvas.setPickOnBounds(false);
		root.getChildren().addAll(chart, this.chartCanvas, this.selectionCanvas);
		final Scene scene = new Scene(root, 640, 480);

		this.chartCanvas.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY)
				this.selectedPoint = new Point2i((int) e.getX(), (int) e.getY());
			this.selectionCanvas.setVisible(true);
		});
		this.chartCanvas.setOnMouseDragged(e -> {
			this.draggPoint = new Point2i((int) e.getX(), (int) e.getY());
			drawSelection();
		});
		this.chartCanvas.setOnMouseReleased(e -> {
			if (this.selectedPoint != null && this.draggPoint != null) {
				double beginX = this.selectedPoint.x / this.ratioX + this.offsetX;
				double endX = this.draggPoint.x / this.ratioX + this.offsetX;
				double beginY = this.draggPoint.y / this.ratioY + this.offsetY;
				double endY = this.selectedPoint.y / this.ratioY + this.offsetY;
				boolean isReinit = false;
				if (endX - beginX < 0) {
					this.first.setXValue(this.minX);
					this.last.setXValue(this.maxX);
					isReinit = true;
				}
				if (endY - beginY < 0) {
					this.first.setYValue(this.minY);
					this.last.setYValue(this.maxY);
					isReinit = true;
				}
				if (!isReinit) {
					if (endX - beginX > 1E-5) {
						this.first.setXValue(beginX);
						this.last.setXValue(endX);
					} else
						System.err.println("Too small X");
					if (endY - beginY > 1E-5) {
						this.first.setYValue(beginY);
						this.last.setYValue(endY);
					} else
						System.err.println("Too small Y");
				}
				updateTickUnit();
				Platform.runLater(() -> updateCanvasParam());
			}
			this.selectedPoint = null;
			this.draggPoint = null;
			drawSelection();
			this.selectionCanvas.setVisible(false);
		});
		this.drawRegion.localToSceneTransformProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		this.chartCanvas.widthProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		this.chartCanvas.heightProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		primaryStage.setTitle("Test de LineChart");
		primaryStage.setScene(scene);
		primaryStage.show();
		// ScenicView.show(scene);
	}

	private void updateCanvasParam() {
		this.xAxis.setLowerBound(this.first.getXValue().doubleValue());
		this.xAxis.setUpperBound(this.last.getXValue().doubleValue());
		this.yAxis.setLowerBound(this.first.getYValue().doubleValue());
		this.yAxis.setUpperBound(this.last.getYValue().doubleValue());

		// Node node = first.getNode();
		// Point2D pos = node.localToScene(new Point2D(0, 0));
		// Bounds bounds = node.getBoundsInLocal();
		// double xBegin = pos.getX() + bounds.getMaxX() / 2.0;
		// double yBegin = pos.getY() + bounds.getMaxY() / 2.0;
		// node = last.getNode();
		// bounds = node.getBoundsInLocal();
		// pos = node.localToScene(new Point2D(0, 0));
		// double xEnd = pos.getX() + bounds.getMaxX() / 2.0;
		// double yEnd = pos.getY() + bounds.getMaxY() / 2.0;

		boolean needToRepaint = false;
		double oldVal = this.ratioX;
		this.ratioX = this.drawRegion.getWidth() / (this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue());
		if (oldVal != this.ratioX)
			needToRepaint = true;
		oldVal = this.ratioY;
		this.ratioY = -this.drawRegion.getHeight() / (this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue());
		if (oldVal != this.ratioY)
			needToRepaint = true;
		oldVal = this.offsetX;
		this.offsetX = this.first.getXValue().doubleValue();
		if (oldVal != this.offsetX)
			needToRepaint = true;
		oldVal = this.offsetY;
		this.offsetY = this.first.getYValue().doubleValue() - this.chartCanvas.getHeight() / this.ratioY;
		if (oldVal != this.offsetY)
			needToRepaint = true;
		if (needToRepaint)
			drawChart();
	}

	private void updateCanvasPos() {
		Point2D pos = this.drawRegion.localToScene(new Point2D(0, 0));
		if (this.chartCanvas.getLayoutX() != pos.getX())
			this.chartCanvas.setLayoutX(pos.getX());
		if (this.chartCanvas.getLayoutY() != pos.getY())
			this.chartCanvas.setLayoutY(pos.getY());
		if (this.selectionCanvas.getLayoutX() != pos.getX())
			this.selectionCanvas.setLayoutX(pos.getX());
		if (this.selectionCanvas.getLayoutY() != pos.getY())
			this.selectionCanvas.setLayoutY(pos.getY());
		updateCanvasParam();
	}

	private void updateTickUnit() {
		double distX = Math.abs(this.last.getXValue().doubleValue() - this.first.getXValue().doubleValue());
		double distY = Math.abs(this.last.getYValue().doubleValue() - this.first.getYValue().doubleValue());
		this.xAxis.setTickUnit(distX / 10.0);
		this.yAxis.setTickUnit(distY / 10.0);
		this.xAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(this.xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distX;
				String pat = "###";
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "0";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
		this.yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(this.xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distY;
				String pat = "###";
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "#";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
	}
}