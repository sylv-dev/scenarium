/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx.fx3D;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Text3D extends Application {

	public static void main(String[] args) {
		System.setProperty("prism.lcdtext", "false");
		System.setProperty("prism.text", "t2k");
		launch(args);
	}

	private Stage primaryStage;

	private final Group root = new Group();

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Example");
		this.primaryStage.setWidth(500);
		this.primaryStage.setHeight(500);

		Scene scene = new Scene(this.root, 500, 500, true, SceneAntialiasing.BALANCED);
		scene.setFill(Color.WHITESMOKE);

		Text text = new Text();
		text.setText("This is a text sample");
		text.setStyle("-fx-font-size: 20;");
		text.setCache(true);

		BorderPane borderPane = new BorderPane();
		borderPane.setStyle("-fx-border-color: black;-fx-background-color: #66CCFF;");
		borderPane.setTop(text);

		this.root.getChildren().add(borderPane);

		PerspectiveCamera camera = new PerspectiveCamera(true);
		camera.setNearClip(0.1);
		camera.setFarClip(10000.0);
		camera.setTranslateX(100);
		camera.setTranslateZ(-500);

		Xform cameraXform = new Xform();
		Xform cameraXform2 = new Xform();
		Xform cameraXform3 = new Xform();

		cameraXform.getChildren().add(cameraXform2);
		cameraXform2.getChildren().add(cameraXform3);
		cameraXform3.getChildren().add(camera);
		// cameraXform3.setRotateZ(180.0);
		cameraXform.ry.setAngle(400.0); // 320
		cameraXform.rx.setAngle(20.0); // 40

		scene.setCamera(camera);

		this.primaryStage.setScene(scene);
		this.primaryStage.show();
	}

}