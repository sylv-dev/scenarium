/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx.fx3D;

import java.util.ArrayList;
import java.util.Random;

import javax.vecmath.Point3f;

import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.test.fx.FxTest;

import javafx.geometry.Dimension2D;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

public class PointCloudDrawer3DFx extends TheaterPanel {
	private static final double CAMERA_INITIAL_DISTANCE = -450;
	private static final double CAMERA_INITIAL_X_ANGLE = 70.0;
	private static final double CAMERA_INITIAL_Y_ANGLE = 320.0;
	private static final double CAMERA_NEAR_CLIP = 0.1;
	private static final double CAMERA_FAR_CLIP = 10000.0;
	private static final double AXIS_LENGTH = 100.0;
	private static final double CONTROL_MULTIPLIER = 0.1;
	private static final double SHIFT_MULTIPLIER = 10.0;

	private static final double MOUSE_SPEED = 0.1;
	private static final double ROTATION_SPEED = 2.0;
	private static final double TRACK_SPEED = 0.3;

	public static void main(String[] args) {
		new FxTest().launchIHM(args, () -> {
			PointCloudDrawer3DFx cfx = new PointCloudDrawer3DFx();
			return cfx.getRegion();
		});
	}

	final Group root = new Group();
	final Xform axisGroup = new Xform();
	final Xform world = new Xform();
	final PerspectiveCamera camera = new PerspectiveCamera(true);
	final Xform cameraXform = new Xform();
	final Xform cameraXform2 = new Xform();
	final Xform cameraXform3 = new Xform();

	final Xform sceneXform = new Xform();
	double mousePosX;
	double mousePosY;
	double mouseOldX;
	double mouseOldY;
	double mouseDeltaX;
	double mouseDeltaY;

	// private ObjModelImporter omi;

	private void buildAxes() {
		System.out.println("buildAxes()");
		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.DARKRED);
		redMaterial.setSpecularColor(Color.RED);

		final PhongMaterial greenMaterial = new PhongMaterial();
		greenMaterial.setDiffuseColor(Color.DARKGREEN);
		greenMaterial.setSpecularColor(Color.GREEN);

		final PhongMaterial blueMaterial = new PhongMaterial();
		blueMaterial.setDiffuseColor(Color.DARKBLUE);
		blueMaterial.setSpecularColor(Color.BLUE);

		final PhongMaterial blackMaterial = new PhongMaterial();
		blackMaterial.setDiffuseColor(Color.BLACK);
		blackMaterial.setSpecularColor(Color.BLACK);

		final Box xAxis = new Box(AXIS_LENGTH, 1, 1);
		final Box yAxis = new Box(1, AXIS_LENGTH, 1);
		final Box zAxis = new Box(1, 1, AXIS_LENGTH);
		final Box cAxis = new Box(1, 1, 1);

		xAxis.setMaterial(redMaterial);
		yAxis.setMaterial(greenMaterial);
		zAxis.setMaterial(blueMaterial);
		cAxis.setMaterial(blackMaterial);

		xAxis.setTranslateX(-AXIS_LENGTH / 2 - 0.5);
		yAxis.setTranslateY(-AXIS_LENGTH / 2 - 0.5);
		zAxis.setTranslateZ(-AXIS_LENGTH / 2 - 0.5);

		this.axisGroup.getChildren().addAll(xAxis, yAxis, zAxis, cAxis);
		// axisGroup.setTranslateX(100);
		this.axisGroup.setVisible(false);
		this.world.getChildren().addAll(this.axisGroup);
	}

	private void buildCamera() {
		System.out.println("buildCamera()");
		this.root.getChildren().add(this.cameraXform);
		this.cameraXform.getChildren().add(this.cameraXform2);
		this.cameraXform2.getChildren().add(this.cameraXform3);
		this.cameraXform3.getChildren().add(this.camera);
		this.cameraXform3.setRotateZ(180.0);

		this.camera.setNearClip(CAMERA_NEAR_CLIP);
		this.camera.setFarClip(CAMERA_FAR_CLIP);
		this.camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
		this.cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
		this.cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
	}

	private void buildScene() {
		// TODO Auto-generated method stub
		Box[] boxs = new Box[50];

		Random rand = new Random();
		for (int i = 0; i < boxs.length; i++) {
			Box point = new Box(5, 5, 5);
			// Sphere point = new Sphere(5);
			point.setTranslateX(-rand.nextDouble() * 500);
			point.setTranslateY(-rand.nextDouble() * 500);
			point.setTranslateZ(-rand.nextDouble() * 500);

			final PhongMaterial redMaterial = new PhongMaterial();
			redMaterial.setDiffuseColor(Color.YELLOW);
			redMaterial.setSpecularColor(Color.RED);

			point.setMaterial(redMaterial);
			boxs[i] = point;
			// rects[i] = new Rectangle(-rand.nextDouble() * 500, -rand.nextDouble() * 500, 6, 6);

		}

		this.sceneXform.getChildren().addAll(boxs);
		// for (MeshView box : omi.getImport())
		// sceneXform.getChildren().add(box);

		int size = 20;
		float dx = -50;
		float dy = -30;
		float dz = 20;
		float[] points = { -size / 2 + dx, size / 2 + dy, 0 - dz, -size / 2 + dx, -size / 2 + dy, 0 - dz, size / 2 + dx, size / 2 + dy, 0 - dz, size / 2 + dx, -size / 2 + dy, 0 - dz };
		float[] texCoords = { 1, 1, 1, 0, 0, 1, 0, 0 };
		int[] faces = { 2, 2, 1, 1, 0, 0, 2, 2, 3, 3, 1, 1 };
		TriangleMesh tm = new TriangleMesh();
		tm.getPoints().setAll(points);
		tm.getTexCoords().setAll(texCoords);
		tm.getFaces().setAll(faces);

		final MeshView rect = new MeshView(tm);
		rect.setMaterial(new PhongMaterial(Color.LIME));

		// sceneXform.getChildren().add(new MeshView(new Shape3DRectangle(20, 20)));

		ArrayList<Point3f> tetra = new ArrayList<>();
		for (int i = 0; i < 1000000; i++)
			tetra.add(new Point3f(rand.nextFloat() * 10, rand.nextFloat() * 10, rand.nextFloat() * 5));
		System.out.println(System.currentTimeMillis());
		this.sceneXform.getChildren().add(new MeshView(new TetrahedronMesh(10.0, tetra)));
		System.out.println(System.currentTimeMillis());

		// rect.setCullFace(CullFace.NONE);
		// sceneXform.getChildren().add(rect);
		// sceneXform.setRotate(cameraXform.rx.getAngle(), cameraXform.ry.getAngle(), 0);
		// AmbientLight ambient = new AmbientLight(); //Couleur simple
		// ambient.setColor(Color.rgb(0, 255, 0, 0.6));
		this.world.getChildren().addAll(this.sceneXform);
	}

	@Override
	public void fitDocument() {
		// TODO Auto-generated method stub

	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(1024, 768);
	}

	private Region getRegion() {
		// omi = new ObjModelImporter();
		// omi.read(new File("/home/revilloud/stilo.obj"));
		this.root.getChildren().add(this.world);
		this.root.setDepthTest(DepthTest.ENABLE);

		buildCamera();
		buildAxes();
		buildScene();

		SubScene subScene = new SubScene(this.root, 800, 600, true, SceneAntialiasing.BALANCED);
		subScene.setFill(Color.WHITE);
		handleKeyboard(subScene);
		handleMouse(subScene);

		subScene.setCamera(this.camera);
		return new Pane(subScene);
	}

	@Override
	public String[] getStatusBarInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	private void handleKeyboard(SubScene subScene) {
		subScene.setOnKeyPressed(event -> {
			switch (event.getCode()) {
			case Z:
				this.cameraXform2.t.setX(0.0);
				this.cameraXform2.t.setY(0.0);
				this.camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
				this.cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
				this.cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
				break;
			case X:
				this.axisGroup.setVisible(!this.axisGroup.isVisible());
				break;
			case V:
				// moleculeGroup.setVisible(!moleculeGroup.isVisible());
				break;
			default:
				break;
			}
		});
	}

	private void handleMouse(SubScene subScene) {
		subScene.setOnMousePressed(me -> {
			this.mousePosX = me.getSceneX();
			this.mousePosY = me.getSceneY();
			this.mouseOldX = me.getSceneX();
			this.mouseOldY = me.getSceneY();
		});
		subScene.setOnMouseDragged(ev -> {
			this.mouseOldX = this.mousePosX;
			this.mouseOldY = this.mousePosY;
			this.mousePosX = ev.getSceneX();
			this.mousePosY = ev.getSceneY();
			this.mouseDeltaX = this.mousePosX - this.mouseOldX;
			this.mouseDeltaY = this.mousePosY - this.mouseOldY;

			double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;

			if (ev.isPrimaryButtonDown()) {
				this.cameraXform.ry.setAngle(this.cameraXform.ry.getAngle() - this.mouseDeltaX * MOUSE_SPEED * modifier * ROTATION_SPEED);
				this.cameraXform.rx.setAngle(this.cameraXform.rx.getAngle() + this.mouseDeltaY * MOUSE_SPEED * modifier * ROTATION_SPEED);
			} else if (ev.isSecondaryButtonDown()) {
				this.cameraXform2.t.setX(this.cameraXform2.t.getX() + this.mouseDeltaX * MOUSE_SPEED * modifier * TRACK_SPEED);
				this.cameraXform2.t.setY(this.cameraXform2.t.getY() + this.mouseDeltaY * MOUSE_SPEED * modifier * TRACK_SPEED);
			}
			// sceneXform.setRotate(cameraXform.rx.getAngle(), cameraXform.ry.getAngle(), 0);

		});
		subScene.setOnScroll((ev) -> {
			double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;
			if (ev.getDeltaY() < 0)
				this.camera.setTranslateZ(this.camera.getTranslateZ() - modifier * 10);
			else
				this.camera.setTranslateZ(this.camera.getTranslateZ() + modifier * 10);
		});
	}

	@Override
	public void paint(Object dataElement) {
		// TODO Auto-generated method stub
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		return false;
	}
}

class Shape3DRectangle extends TriangleMesh {

	public Shape3DRectangle(float width, float height) {
		float hw = 10 / 2f;
		float hh = 10 / 2f;
		float hd = 10 / 2f;
		float[] points = { hw, hh, hd, hw, hh, -hd, hw, -hh, hd, hw, -hh, -hd, -hw, hh, hd, -hw, hh, -hd, -hw, -hh, hd, -hw, -hh, -hd, };

		float[] tex = { 100, 0, 200, 0, 0, 100, 100, 100, 200, 100, 300, 100, 400, 100, 0, 200, 100, 200, 200, 200, 300, 200, 400, 200, 100, 300, 200, 300 };

		int[] faces = { 0, 10, 2, 5, 1, 9, 2, 5, 3, 4, 1, 9, 4, 7, 5, 8, 6, 2, 6, 2, 5, 8, 7, 3, 0, 13, 1, 9, 4, 12, 4, 12, 1, 9, 5, 8, 2, 1, 6, 0, 3, 4, 3, 4, 6, 0, 7, 3, 0, 10, 4, 11, 2, 5, 2, 5, 4,
				11, 6, 6, 1, 9, 3, 4, 5, 8, 5, 8, 3, 4, 7, 3 };

		this.getPoints().setAll(points);
		this.getTexCoords().setAll(tex);
		this.getFaces().setAll(faces);
	}
}

class Xform extends Group {

	public enum RotateOrder {
		XYZ, XZY, YXZ, YZX, ZXY, ZYX
	}

	public Translate t = new Translate();
	public Translate p = new Translate();
	public Translate ip = new Translate();
	public Rotate rx = new Rotate();

	{
		this.rx.setAxis(Rotate.X_AXIS);
	}

	public Rotate ry = new Rotate();

	{
		this.ry.setAxis(Rotate.Y_AXIS);
	}

	public Rotate rz = new Rotate();

	{
		this.rz.setAxis(Rotate.Z_AXIS);
	}

	public Scale s = new Scale();

	public Xform() {
		super();
		getTransforms().addAll(this.t, this.rz, this.ry, this.rx, this.s);
	}

	public Xform(RotateOrder rotateOrder) {
		super();
		// choose the order of rotations based on the rotateOrder
		switch (rotateOrder) {
		case XYZ:
			getTransforms().addAll(this.t, this.p, this.rz, this.ry, this.rx, this.s, this.ip);
			break;
		case XZY:
			getTransforms().addAll(this.t, this.p, this.ry, this.rz, this.rx, this.s, this.ip);
			break;
		case YXZ:
			getTransforms().addAll(this.t, this.p, this.rz, this.rx, this.ry, this.s, this.ip);
			break;
		case YZX:
			getTransforms().addAll(this.t, this.p, this.rx, this.rz, this.ry, this.s, this.ip); // For Camera
			break;
		case ZXY:
			getTransforms().addAll(this.t, this.p, this.ry, this.rx, this.rz, this.s, this.ip);
			break;
		case ZYX:
			getTransforms().addAll(this.t, this.p, this.rx, this.ry, this.rz, this.s, this.ip);
			break;
		default:
			throw new IllegalArgumentException("RotateOrder: " + rotateOrder + " is not supported");
		}
	}

	public void reset() {
		this.t.setX(0.0);
		this.t.setY(0.0);
		this.t.setZ(0.0);
		this.rx.setAngle(0.0);
		this.ry.setAngle(0.0);
		this.rz.setAngle(0.0);
		this.s.setX(1.0);
		this.s.setY(1.0);
		this.s.setZ(1.0);
		this.p.setX(0.0);
		this.p.setY(0.0);
		this.p.setZ(0.0);
		this.ip.setX(0.0);
		this.ip.setY(0.0);
		this.ip.setZ(0.0);
	}

	public void resetTSP() {
		this.t.setX(0.0);
		this.t.setY(0.0);
		this.t.setZ(0.0);
		this.s.setX(1.0);
		this.s.setY(1.0);
		this.s.setZ(1.0);
		this.p.setX(0.0);
		this.p.setY(0.0);
		this.p.setZ(0.0);
		this.ip.setX(0.0);
		this.ip.setY(0.0);
		this.ip.setZ(0.0);
	}

	public void setPivot(double x, double y, double z) {
		this.p.setX(x);
		this.p.setY(y);
		this.p.setZ(z);
		this.ip.setX(-x);
		this.ip.setY(-y);
		this.ip.setZ(-z);
	}

	public void setRotate(double x, double y, double z) {
		this.rx.setAngle(x);
		this.ry.setAngle(y);
		this.rz.setAngle(z);
	}

	public void setRotateX(double x) {
		this.rx.setAngle(x);
	}

	public void setRotateY(double y) {
		this.ry.setAngle(y);
	}

	public void setRotateZ(double z) {
		this.rz.setAngle(z);
	}

	public void setRx(double x) {
		this.rx.setAngle(x);
	}

	public void setRy(double y) {
		this.ry.setAngle(y);
	}

	public void setRz(double z) {
		this.rz.setAngle(z);
	}

	public void setScale(double scaleFactor) {
		this.s.setX(scaleFactor);
		this.s.setY(scaleFactor);
		this.s.setZ(scaleFactor);
	}

	public void setScale(double x, double y, double z) {
		this.s.setX(x);
		this.s.setY(y);
		this.s.setZ(z);
	}

	public void setSx(double x) {
		this.s.setX(x);
	}

	public void setSy(double y) {
		this.s.setY(y);
	}

	public void setSz(double z) {
		this.s.setZ(z);
	}

	public void setTranslate(double x, double y) {
		this.t.setX(x);
		this.t.setY(y);
	}

	public void setTranslate(double x, double y, double z) {
		this.t.setX(x);
		this.t.setY(y);
		this.t.setZ(z);
	}

	// Cannot override these methods as they are final:
	// public void setTranslateX(double x) { t.setX(x); }
	// public void setTranslateY(double y) { t.setY(y); }
	// public void setTranslateZ(double z) { t.setZ(z); }
	// Use these methods instead:
	public void setTx(double x) {
		this.t.setX(x);
	}

	public void setTy(double y) {
		this.t.setY(y);
	}

	public void setTz(double z) {
		this.t.setZ(z);
	}

	@Override
	public String toString() {
		return "Xform[t = (" + this.t.getX() + ", " + this.t.getY() + ", " + this.t.getZ() + ")  " + "r = (" + this.rx.getAngle() + ", " + this.ry.getAngle() + ", " + this.rz.getAngle() + ")  "
				+ "s = (" + this.s.getX() + ", " + this.s.getY() + ", " + this.s.getZ() + ")  " + "p = (" + this.p.getX() + ", " + this.p.getY() + ", " + this.p.getZ() + ")  " + "ip = ("
				+ this.ip.getX() + ", " + this.ip.getY() + ", " + this.ip.getZ() + ")]";
	}
}
