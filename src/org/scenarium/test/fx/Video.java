/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

public class Video extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Tool Test");

		Media media = new Media(new File("/home/revilloud/Dropbox/Public/LaneDetection.mp4").toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.play();

		MediaView mediaView = new MediaView(mediaPlayer);

		Button playButton = new Button("play");
		playButton.setOnAction(e -> {
			if (mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING)
				mediaPlayer.pause();
			else
				mediaPlayer.play();
		});
		Scene scene = new Scene(new VBox(playButton, mediaView), 640, 480);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.show();

	}
}
