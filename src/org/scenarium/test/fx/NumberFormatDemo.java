/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NumberFormatDemo extends Application {
	/** The main method is only needed for the IDE with limited JavaFX support. Not needed for running from the command line. */
	public static void main(String[] args) {
		launch(args);
	}

	// Combo box for selecting available locales
	private final ComboBox<String> cboLocale = new ComboBox<>();
	// Text fields for interest rate, year, and loan amount
	private final TextField tfInterestRate = new TextField("6.75");
	private final TextField tfNumberOfYears = new TextField("15");
	private final TextField tfLoanAmount = new TextField("107000");
	private final TextField tfFormattedInterestRate = new TextField();
	private final TextField tfFormattedNumberOfYears = new TextField();

	private final TextField tfFormattedLoanAmount = new TextField();
	// Text fields for monthly payment and total payment
	private final TextField tfTotalPayment = new TextField();

	private final TextField tfMonthlyPayment = new TextField();

	// Compute button
	private final Button btCompute = new Button("Compute");

	// Current locale
	private Locale locale = Locale.getDefault();

	// Declare locales to store available locales
	private final Locale[] locales = Calendar.getAvailableLocales();

	/** Compute payments and display results locale-sensitive format */
	private void computeLoan() {
		// Retrieve input from user
		double loan = Double.parseDouble(this.tfLoanAmount.getText());
		double interestRate = Double.parseDouble(this.tfInterestRate.getText()) / 1240.0;
		int numberOfYears = (int) Double.parseDouble(this.tfNumberOfYears.getText());

		// Calculate payments
		double monthlyPayment = loan * interestRate / (1 - Math.pow(1 / (1 + interestRate), numberOfYears * 12));
		double totalPayment = monthlyPayment * numberOfYears * 12;

		// Get formatters
		NumberFormat percentFormatter = NumberFormat.getPercentInstance(this.locale);
		NumberFormat currencyForm = NumberFormat.getCurrencyInstance(this.locale);
		NumberFormat numberForm = NumberFormat.getNumberInstance(this.locale);
		percentFormatter.setMinimumFractionDigits(2);

		// Display formatted input
		this.tfFormattedInterestRate.setText(percentFormatter.format(interestRate * 12));
		this.tfFormattedNumberOfYears.setText(numberForm.format(numberOfYears));
		this.tfFormattedLoanAmount.setText(currencyForm.format(loan));

		// Display results in currency format
		this.tfMonthlyPayment.setText(currencyForm.format(monthlyPayment));
		this.tfTotalPayment.setText(currencyForm.format(totalPayment));
	}

	/** Initialize the combo box */
	public void initializeComboBox() {
		// Add locale names to the combo box
		for (int i = 0; i < this.locales.length; i++)
			this.cboLocale.getItems().add(this.locales[i].getDisplayName());
	}

	@Override // Override the start method in the Application class
	public void start(Stage primaryStage) {
		initializeComboBox();

		// Pane to hold the combo box for selecting locales
		HBox hBox = new HBox(5);
		hBox.getChildren().addAll(new Label("Choose a Locale"), this.cboLocale);

		// Pane to hold the input
		GridPane gridPane = new GridPane();
		gridPane.add(new Label("Interest Rate"), 0, 0);
		gridPane.add(this.tfInterestRate, 1, 0);
		gridPane.add(this.tfFormattedInterestRate, 2, 0);
		gridPane.add(new Label("Number of Years"), 0, 1);
		gridPane.add(this.tfNumberOfYears, 1, 1);
		gridPane.add(this.tfFormattedNumberOfYears, 2, 1);
		gridPane.add(new Label("Loan Amount"), 0, 2);
		gridPane.add(this.tfLoanAmount, 1, 2);
		gridPane.add(this.tfFormattedLoanAmount, 2, 2);

		// Pane to hold the output
		GridPane gridPaneOutput = new GridPane();
		gridPaneOutput.add(new Label("Monthly Payment"), 0, 0);
		gridPaneOutput.add(this.tfMonthlyPayment, 1, 0);
		gridPaneOutput.add(new Label("Total Payment"), 0, 1);
		gridPaneOutput.add(this.tfTotalPayment, 1, 1);

		// Set text field alignment
		this.tfFormattedInterestRate.setAlignment(Pos.BASELINE_RIGHT);
		this.tfFormattedNumberOfYears.setAlignment(Pos.BASELINE_RIGHT);
		this.tfFormattedLoanAmount.setAlignment(Pos.BASELINE_RIGHT);
		this.tfTotalPayment.setAlignment(Pos.BASELINE_RIGHT);
		this.tfMonthlyPayment.setAlignment(Pos.BASELINE_RIGHT);

		// Set editable false
		this.tfFormattedInterestRate.setEditable(false);
		this.tfFormattedNumberOfYears.setEditable(false);
		this.tfFormattedLoanAmount.setEditable(false);
		this.tfTotalPayment.setEditable(false);
		this.tfMonthlyPayment.setEditable(false);

		VBox vBox = new VBox(5);
		vBox.getChildren().addAll(hBox, new Label("Enter Annual Interest Rate, " + "Number of Years, and Loan Amount"), gridPane, new Label("Payment"), gridPaneOutput, this.btCompute);

		// Create a scene and place it in the stage
		Scene scene = new Scene(vBox, 400, 300);
		primaryStage.setTitle("NumberFormatDemo"); // Set the stage title
		primaryStage.setScene(scene); // Place the scene in the stage
		primaryStage.show(); // Display the stage

		// Register listeners
		this.cboLocale.setOnAction(e -> {
			this.locale = this.locales[this.cboLocale.getSelectionModel().getSelectedIndex()];
			computeLoan();
		});

		this.btCompute.setOnAction(e -> computeLoan());
	}
}