/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FxWeb extends Application {
	private static final double RATIO = 0.75;
	private static final String NEWTAB = "Nouvel onglet";

	public static void main(String[] args) {
		launch(args);
	}

	private TabPane tabPane;
	private ProgressIndicator progressIndicator;
	private TextField adressTextField;
	private Stage stage;
	private Button nextButton;

	private Button previousButton;

	private Tab createWebTab() {
		WebView wv = new WebView();
		WebEngine engine = wv.getEngine();
		engine.getLoadWorker().stateProperty().addListener((obs2, oldVal, newVal) -> {
			if (newVal == Worker.State.SUCCEEDED) {
				this.progressIndicator.setVisible(false);
				String title = engine.getTitle();
				this.tabPane.getSelectionModel().getSelectedItem().setText(title == null || title.isEmpty() ? "Sans nom" : title);
				this.stage.setTitle(title == null || title.isEmpty() ? "Navigateur Internet" : title);
				this.adressTextField.setText(engine.getLocation());
			} else {
				this.progressIndicator.setVisible(true);
				this.progressIndicator.progressProperty().bind(engine.getLoadWorker().progressProperty());
			}
		});
		WebHistory history = engine.getHistory();
		history.currentIndexProperty().addListener(e -> updateButton());
		return new Tab(NEWTAB, wv);
	}

	private WebEngine getTabWebViewEngine() {
		return ((WebView) this.tabPane.getSelectionModel().getSelectedItem().getContent()).getEngine();
	}

	@Override
	public void start(Stage primaryStage) {
		this.stage = primaryStage;
		Tab addTab = new Tab("+");
		this.tabPane = new TabPane(createWebTab(), addTab);
		this.tabPane.setTabMaxWidth(250);
		this.tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			if (newTab == addTab) {
				Tab createdTab = createWebTab();
				this.tabPane.getTabs().add(this.tabPane.getTabs().size() - 1, createdTab);
				this.tabPane.getSelectionModel().select(createdTab);
			}
			updateButton();
			this.adressTextField.setText(getTabWebViewEngine().getLocation());
		});

		this.adressTextField = new TextField();
		this.adressTextField.setPromptText("Saisir une adresse");
		TextField rf = new TextField();
		rf.setPromptText("Rechercher");
		this.adressTextField.setOnAction(e -> {
			String url = this.adressTextField.getText();
			if (!url.startsWith("http://") && !url.startsWith("https://")) {
				url = "https://" + url;
				this.adressTextField.setText(url);
			}
			this.nextButton.setDisable(false);
			getTabWebViewEngine().load(url);
		});
		rf.setOnAction(e -> getTabWebViewEngine().load("http://www.google.fr/search?q=" + rf.getText()));
		this.progressIndicator = new ProgressBar();
		this.progressIndicator.setVisible(false);
		this.progressIndicator.minWidthProperty().bind(this.adressTextField.heightProperty());
		this.progressIndicator.prefWidthProperty().bind(this.progressIndicator.minWidthProperty());
		this.progressIndicator.setPadding(new Insets(0, 0, 0, 4));
		this.progressIndicator.prefHeightProperty().bind(this.progressIndicator.widthProperty());
		HBox boxText = new HBox(this.adressTextField, rf, this.progressIndicator);
		boxText.setSpacing(5);
		this.adressTextField.prefWidthProperty().bind(boxText.widthProperty().multiply(RATIO));
		rf.prefWidthProperty().bind(boxText.widthProperty().multiply(1 - RATIO));
		HBox.setHgrow(boxText, Priority.ALWAYS);

		this.nextButton = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/forward.png"))));
		this.previousButton = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/backward.png"))));
		this.nextButton.setOnAction(e -> {
			WebHistory history = getTabWebViewEngine().getHistory();
			this.nextButton.setDisable(history.getCurrentIndex() == history.getEntries().size() - 2);
			this.previousButton.setDisable(false);
			history.go(1);
		});
		this.previousButton.setOnAction(e -> {
			WebHistory history = getTabWebViewEngine().getHistory();
			this.previousButton.setDisable(history.getCurrentIndex() == 1);
			this.nextButton.setDisable(false);
			history.go(-1);
		});
		this.nextButton.prefHeightProperty().bind(boxText.heightProperty());
		this.previousButton.prefHeightProperty().bind(boxText.heightProperty());
		updateButton();

		VBox.setVgrow(this.tabPane, Priority.ALWAYS);
		HBox headerBox = new HBox(new HBox(this.previousButton, this.nextButton), boxText, this.progressIndicator);
		headerBox.setSpacing(4);
		VBox.setVgrow(this.tabPane, Priority.ALWAYS);
		primaryStage.setTitle("Navigateur");
		primaryStage.setScene(new Scene(new VBox(headerBox, this.tabPane), 1024, 768));
		primaryStage.show();
	}

	private void updateButton() {
		WebHistory history = getTabWebViewEngine().getHistory();
		this.previousButton.setDisable(history.getCurrentIndex() == 0);
		this.nextButton.setDisable(history.getCurrentIndex() >= history.getEntries().size() - 1);
	}
}
