/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.incubator;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

public class Compile {

	private Compile() {}

	static final class CharSequenceJavaFileObject extends SimpleJavaFileObject {
		final CharSequence content;

		public CharSequenceJavaFileObject(String className, CharSequence content) {
			super(URI.create("string:///" + className.replace('.', '/') + JavaFileObject.Kind.SOURCE.extension), JavaFileObject.Kind.SOURCE);
			this.content = content;
		}

		@Override
		public CharSequence getCharContent(boolean ignoreEncodingErrors) {
			return this.content;
		}
	}

	static final class ClassFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
		JavaFileObject o;

		ClassFileManager(StandardJavaFileManager m) {
			super(m);
		}

		@Override
		public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) {
			return this.o = new JavaFileObject(className, kind);
		}
	}

	// These are some utility classes needed for the JavaCompiler
	// ----------------------------------------------------------
	static final class JavaFileObject extends SimpleJavaFileObject {
		final ByteArrayOutputStream os = new ByteArrayOutputStream();

		JavaFileObject(String name, JavaFileObject.Kind kind) {
			super(URI.create("string:///" + name.replace('.', '/') + kind.extension), kind);
		}

		byte[] getBytes() {
			return this.os.toByteArray();
		}

		@Override
		public OutputStream openOutputStream() {
			return this.os;
		}
	}

	static Class<?> compile(String className, String content) throws Exception {
		Lookup lookup = MethodHandles.lookup();
		// If we have already compiled our class, simply load it
		try {
			return lookup.lookupClass().getClassLoader().loadClass(className);
		}
		// Otherwise, let's try to compile it
		catch (ClassNotFoundException ignore) {
			return compile0(className, content);
		}
	}

	static Class<?> compile0(String className, String content) throws Exception {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		ClassFileManager manager = new ClassFileManager(compiler.getStandardFileManager(null, null, null));
		List<CharSequenceJavaFileObject> files = new ArrayList<>();
		files.add(new CharSequenceJavaFileObject(className, content));
		compiler.getTask(null, manager, null, null, null, files).call();
		Class<?> result = null;
		// Implement a check whether we're on JDK 8. If so, use
		// protected ClassLoader API, reflectively
		// if (onJava8()) {
		// ClassLoader cl = lookup.lookupClass().getClassLoader();
		// byte[] b = manager.o.getBytes();
		// result = Reflect.on(cl).call("defineClass", className, b, 0, b.length).get();
		// }
		// Lookup.defineClass() has only been introduced in Java 9.
		// It is required to get private-access to interfaces in
		// the class hierarchy
		// else {
		// This method is called by client code from two levels
		// up the current stack frame. We need a private-access
		// lookup from the class in that stack frame in order
		// to get private-access to any local interfaces at
		// that location.
		// Class<?> caller = StackWalker.getInstance(RETAIN_CLASS_REFERENCE).walk(s -> s.skip(2).findFirst().get().getDeclaringClass());
		// If the compiled class is in the same package as the
		// caller class, then we can use the private-access
		// Lookup of the caller class
		// if (className.startsWith(caller.getPackageName())) {
		// result = MethodHandles.privateLookupIn(caller, lookup).defineClass(manager.o.getBytes());
		// }
		// Otherwise, use an arbitrary class loader. This
		// approach doesn't allow for loading private-access
		// interfaces in the compiled class's type hierarchy
		// else {
		result = new ClassLoader() {
			@Override
			protected Class<?> findClass(String name) throws ClassNotFoundException {
				byte[] b = manager.o.getBytes();
				int len = b.length;
				return defineClass(className, b, 0, len);
			}
		}.loadClass(className);
		// }
		// }
		return result;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// Run this code from within the com.example package
		try {
			Constructor<?> c = compile("com.example.CompileTest", "package com.example;\n" + "class CompileTest implements java.util.function.Supplier<String> {\n" + "  public String get() {\n"
					+ "    return \"Hello World!\";\n" + "  }\n" + "}\n").getDeclaredConstructor();
			c.setAccessible(true);
			System.out.println(((Supplier<String>) c.newInstance()).get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}