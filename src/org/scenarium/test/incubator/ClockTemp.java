package org.scenarium.test.incubator;

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ClockTemp {

	private ClockTemp() {}

	public static void main(String[] args) {
		int nbProc = 0;
		try {
			Process p = Runtime.getRuntime().exec("sensors");
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String s;
			while ((s = br.readLine()) != null)
				if (s.contains("Core"))
					nbProc++;
			p.waitFor();
			p.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
		float[] temp = new float[nbProc];
		for (int i = 0; i < 100; i++) {
			try {
				Process p = Runtime.getRuntime().exec("sensors");
				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String s;
				while ((s = br.readLine()) != null)
					if (s.contains("Core"))
						temp[Integer.parseInt(s.substring(s.indexOf("Core") + 5, s.indexOf("Core") + 6))] = Float.parseFloat(s.substring(s.indexOf("+") + 1, s.indexOf("°C")));
				System.out.println(Arrays.toString(temp));
				p.waitFor();
				p.destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
