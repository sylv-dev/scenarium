/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import static java.util.Arrays.asList;

import java.util.List;
import java.util.function.Predicate;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class TreeViewDemo extends Application {

	private static final String SALES_DEPARTMENT = "Sales Depa";
	private static final String IT_SUPPORT = "IT adsfasdfasfdasdfsadfsadfasdf Support";
	private static final String ACCOUNTS_DEPARTMENT = "Accounts Department";

	private final List<Employee> employees = asList(new Employee("Ethan Williams", SALES_DEPARTMENT), new Employee("Emma Jones", SALES_DEPARTMENT),
			new Employee("Michael Brownasdfasdfsadfs", SALES_DEPARTMENT), new Employee("Anna Black", SALES_DEPARTMENT), new Employee("Roer York", SALES_DEPARTMENT),
			new Employee("Susan Collins", SALES_DEPARTMENT), new Employee("Miaaaake Graham", IT_SUPPORT), new Employee("Judy Mayer", IT_SUPPORT), new Employee("Gregy Smith", IT_SUPPORT),
			new Employee("Jacob Smith", ACCOUNTS_DEPARTMENT), new Employee("Isabella Johnson", ACCOUNTS_DEPARTMENT));

	private final FilterableTreeItem<String> rootNode = new FilterableTreeItem<>("MyCompany Human Resources");// , rootIcon);

	public static void main(String[] args) {
		Application.launch(args);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void start(Stage stage) {

		this.rootNode.setExpanded(true);
		final TreeView<String> treeView = new TreeView<>(this.rootNode);
		// rootNode.getInternalChildren().add(new FilterableTreeItem<>(employees.get(0).getDepartment()));
		for (Employee employee : this.employees) {
			FilterableTreeItem<String> empLeaf = new FilterableTreeItem<>(employee.getName());
			boolean found = false;
			for (TreeItem<String> depNode : this.rootNode.getChildren())
				if (depNode.getValue().contentEquals(employee.getDepartment())) {
					((FilterableTreeItem) depNode).getInternalChildren().add(empLeaf);
					found = true;
					break;
				}

			if (!found) {
				FilterableTreeItem<String> depNode = new FilterableTreeItem<>(employee.getDepartment());
				this.rootNode.getInternalChildren().add(depNode);
				depNode.getInternalChildren().add(empLeaf);
			}
		}

		stage.setTitle("Tree View Sample");
		VBox box = new VBox();
		final Scene scene = new Scene(box, 400, 300);
		scene.setFill(Color.LIGHTGRAY);

		treeView.setShowRoot(false);

		TextField filterField = new TextField();

		this.rootNode.predicateProperty().bind(Bindings.createObjectBinding(() -> {
			if (filterField.getText() == null || filterField.getText().isEmpty())
				return null;
			return TreeItemPredicate.create(actor -> actor.toString().contains(filterField.getText()));
		}, filterField.textProperty()));

		box.getChildren().addAll(treeView, filterField);
		VBox.setVgrow(treeView, Priority.ALWAYS);

		stage.setScene(scene);
		stage.show();
		new Thread(() -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Platform.runLater(() -> {
				this.rootNode.getInternalChildren().clear();
			});
		}).start();
	}

	@FunctionalInterface
	public interface TreeItemPredicate<T> {

		boolean test(TreeItem<T> parent, T value);

		static <T> TreeItemPredicate<T> create(Predicate<T> predicate) {
			return (parent, value) -> predicate.test(value);
		}

	}

	public class FilterableTreeItem<T> extends TreeItem<T> {
		private final ObservableList<TreeItem<T>> sourceList = FXCollections.observableArrayList();
		private final FilteredList<TreeItem<T>> filteredList = new FilteredList<>(this.sourceList);

		private final ObjectProperty<TreeItemPredicate<T>> predicate = new SimpleObjectProperty<>();

		public FilterableTreeItem(T value) {
			this(value, null);
		}

		public FilterableTreeItem(T value, Node graphic) {
			super(value, graphic);
			this.filteredList.predicateProperty().bind(Bindings.createObjectBinding(() -> {
				Predicate<TreeItem<T>> p = child -> {
					if (child instanceof FilterableTreeItem) {
						FilterableTreeItem<T> filterableChild = (FilterableTreeItem<T>) child;
						filterableChild.setPredicate(this.predicate.get());
					}
					if (this.predicate.get() == null)
						return true;
					if (child.getChildren().size() > 0)
						return true;
					return this.predicate.get().test(this, child.getValue());
				};
				return p;
			}, this.predicate));
			Bindings.bindContent(getChildren(), getBackingList());
		}

		protected ObservableList<TreeItem<T>> getBackingList() {
			return this.filteredList;
		}

		public ObservableList<TreeItem<T>> getInternalChildren() {
			return this.sourceList;
		}

		public final ObjectProperty<TreeItemPredicate<T>> predicateProperty() {
			return this.predicate;
		}

		public final TreeItemPredicate<T> getPredicate() {
			return this.predicate.get();
		}

		public final void setPredicate(TreeItemPredicate<T> predicate) {
			this.predicate.set(predicate);
		}
	}

	public static class Employee {

		private final SimpleStringProperty name;
		private final SimpleStringProperty department;

		private Employee(String name, String department) {
			this.name = new SimpleStringProperty(name);
			this.department = new SimpleStringProperty(department);
		}

		public String getName() {
			return this.name.get();
		}

		public void setName(String firstName) {
			this.name.set(firstName);
		}

		public String getDepartment() {
			return this.department.get();
		}

		public void setDepartment(String firstName) {
			this.department.set(firstName);
		}
	}
}