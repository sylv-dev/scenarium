/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.versionconverter;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;

import org.beanmanager.editors.basic.StringEditor;
import org.scenarium.filemanager.filerecorder.PropertyStreamRecorder;
import org.scenarium.filemanager.scenario.PrimitiveData;

public class StringDataUpdater {

	private StringDataUpdater() {}

	public static boolean isStringDataFile(String fileDataPath) {
		try {
			try (RandomAccessFile rafData = new RandomAccessFile(new File(fileDataPath), "r")) {
				Class<?> type = Class.forName(rafData.readLine());
				return type == String.class;
			}
		} catch (ClassNotFoundException | IOException e) {

		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		if (isStringDataFile("/home/revilloud/Log/log Satory/Rec_20151112_170309 mire/2_SoketString.ppd"))
			update("/home/revilloud/Log/log Satory/Rec_20151112_170309 mire/2_SoketString.ppd");
	}

	public static void update(String fileDataPath) throws Exception {
		String fileIndexPath = fileDataPath.substring(0, fileDataPath.lastIndexOf(".")) + ".pindex";
		if (!new File(fileIndexPath).exists())
			throw new IllegalArgumentException("The index file: " + fileIndexPath + " is missing");
		// File indexFile = new File(fileIndexPath);
		File dataFile = new File(fileDataPath);
		String path = fileIndexPath.substring(0, fileIndexPath.lastIndexOf(File.separator));
		String name = fileIndexPath.substring(fileIndexPath.lastIndexOf(File.separator) + 1);

		PrimitiveData pd = new PrimitiveData();
		pd.load(dataFile, false);
		long nbData = pd.getEndTime();
		PropertyStreamRecorder psr = new PropertyStreamRecorder(new StringEditor());
		name = name.substring(0, name.lastIndexOf("."));
		psr.setRecordPath(path);
		psr.setRecordName("new" + name);
		System.out.println("process String: " + dataFile);
		for (int i = 0; i < nbData; i++) {
			pd.process((long) i);
			String data = (String) pd.getScenarioData();
			psr.pop(data);
		}
		psr.close();
		// if(indexFile.renameTo(new File(path + File.separator + "old" + name + ".pindex")))
		// new File(path + File.separator + File.separator + "n" + name + ".pindex").renameTo(indexFile);
		// else
		// System.err.println("cannot rename : " + indexFile + " to: " + new File(path + File.separator + "old" + name + ".pindex"));
		// if(dataFile.renameTo(new File(path + File.separator + "old" + name + ".ppd")))
		// new File(path + File.separator + File.separator + "n" + name + ".ppd").renameTo(dataFile);
		// else
		// System.err.println("cannot rename : " + dataFile + " to: " + new File(path + File.separator + "old" + name + ".ppd"));

	}

	public static void updateTree(File rootFile) throws Exception {
		LinkedList<File> fileStack = new LinkedList<>();
		LinkedList<File> ppdStack = new LinkedList<>();
		if (rootFile.isDirectory())
			fileStack.push(rootFile);
		else
			ppdStack.add(rootFile);
		while (!fileStack.isEmpty()) {
			File file = fileStack.pop();
			if (file.isDirectory() && !Files.isSymbolicLink(file.toPath())) {
				File[] files = file.listFiles();
				if (files != null)
					fileStack.addAll(Arrays.asList(file.listFiles()));
			} else if (file.getName().endsWith(".ppd") && !file.getName().startsWith("new") && isStringDataFile(file.getAbsolutePath()))
				if (!ppdStack.contains(new File(file.getAbsolutePath()))) {
					ppdStack.add(new File(file.getAbsolutePath()));
					System.out.println("ppd: " + file.getCanonicalFile());
				}
		}
		// for (File psfFile : psfStack)
		// System.out.println(psfFile.toString());
		// for (File ppdFile : ppdStack)
		// update(ppdFile.getAbsolutePath());
	}
}
