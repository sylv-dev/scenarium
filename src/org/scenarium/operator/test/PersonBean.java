package org.scenarium.operator.test;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

@BlockInfo(author = "victor", info = "le block de la mort qui tue")
public class PersonBean extends EvolvedOperator {
	private String name;
	private boolean major;
	@PropertyInfo(info = "c'est la classe")
	@NumberInfo(min = 4, max = 12, controlType = ControlType.SPINNER_AND_SLIDER, increment = 2)
	private int a = 5;
	private Integer r;
	private Integer t;
	private Timer timer;

	@Override
	public void initStruct() {
		Class<?>[] types = new Class[this.a];
		String[] names = new String[this.a];
		for (int i = 0; i < names.length; i++) {
			names[i] = "patate-" + i;
			types[i] = Integer.class;
		}
		updateInputs(names, types);
	}

	public PersonBean() {}

	@Override
	public void birth() {
		System.out.println("birth");
		onStart(() -> {
			new Thread(() -> {
				this.timer = new Timer();
				this.timer.scheduleAtFixedRate(new TimerTask() {

					@Override
					public void run() {
						Object[] outputs = new Object[getNbOutput()];
						long[] ts = new long[getNbOutput()];
						outputs[0] = Double.valueOf(PersonBean.this.r + PersonBean.this.a);
						outputs[1] = PersonBean.this.t;
						ts[1] = 1532;
						triggerOutput(outputs, ts);
					}
				}, 0, 100);
			}).start();
		});
	}

	public Double process(Integer r, Integer t, Double... vars) {
		new BufferedImage(640, 480, BufferedImage.TYPE_3BYTE_BGR);
		System.out.println(Arrays.toString(getAdditionalInputs()));
		setWarning("c'est la merde");
		System.out.println(getTimeStamp(getInputIndex("r")));
		if (r != null)
			this.r = r;
		if (t != null)
			this.t = t;
		System.out.println(r + " " + t);
		if (this.r != null && this.t != null)
			// double res = (double) (this.r + this.t);
			// this.r = null;
			// this.t = null;
			return null;
		return null;
	}

	public Integer getOutputPatate() {
		return null;
	}

	@Override
	public void death() {
		this.timer.cancel();
		this.timer.purge();
		this.timer = null;
		System.out.println("death");
	}

	// @Override
	// public Region getNode() {
	// return new BorderPane(new Button("connard"));
	// }

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMajor() {
		return this.major;
	}

	public void setMajor(boolean major) {
		this.major = major;
	}

	public int getA() {
		return this.a;
	}

	public void setA(int a) {
		this.a = a;
		initStruct();
	}

	@Override
	public String toString() {
		return "PersonBean [name=" + this.name + ", major=" + this.major + "]";
	}

	@Override
	public PersonBean clone() {
		try {
			return (PersonBean) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
