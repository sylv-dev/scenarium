/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class ArrayGenerator extends EvolvedOperator {

	@Override
	public void birth() throws Exception {
		new Timer().scheduleAtFixedRate(new TimerTask() {
			int cpt = 0;

			@Override
			public void run() {
				double[] tab = new double[(int) (30000 + Math.random() * 10000)];
				int index = 0;
				for (int i = 0; i < tab.length; i++)
					tab[i] = index++ / 100.0 + this.cpt / 100000.0;

				double[][] tab2 = new double[49985][(int) (5 + Math.random() * 3)];
				index = 0;
				for (int i = 0; i < tab2.length; i++)
					for (int j = 0; j < tab2[i].length; j++)
						tab2[i][j] = index++ + this.cpt / 100000.0;
				this.cpt++;
				triggerOutput((Object) tab2);
			}
		}, 0, 10);
	}

	@Override
	public void death() throws Exception {
		// TODO Auto-generated method stub

	}

	public double[][] getOutputData() {
		return null;
	}

	public void process() {
		// TODO Auto-generated method stub

	}
}
