/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class B2 extends EvolvedOperator {

	private Timer timer;

	@Override
	public void birth() {
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				triggerOutput(new Object[] { new BufferedImage(640, 480, BufferedImage.TYPE_3BYTE_BGR) }, new long[] { System.currentTimeMillis() });
			}
		}, 0, 3);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void death() {
		this.timer.cancel();
		this.timer = null;

		try {
			System.out.println("begin wait");
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}

	}

	public BufferedImage process() {
		return null;
	}
}