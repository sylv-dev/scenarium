/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.util.Arrays;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class IOTest extends EvolvedOperator {

	private int nbInput = 2;
	private int nbOutput = 3;

	@Override
	public void birth() throws Exception {
		new Thread(() -> {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			process(0, 0.0, (short) 0);
		}).start();
	}

	@Override
	public void death() throws Exception {

	}

	@Override
	public int getNbInput() {
		return this.nbInput;
	}

	@Override
	public int getNbOutput() {
		return this.nbOutput;
	}

	@Override
	public void initStruct() {
		String[] names = new String[this.nbInput];
		Class<?>[] types = new Class<?>[this.nbInput];
		for (int i = 0; i < names.length; i++) {
			names[i] = "in-" + i;
			types[i] = boolean.class;
		}
		updateInputs(names, types);

		names = new String[this.nbOutput];
		types = new Class<?>[this.nbOutput];
		for (int i = 0; i < names.length; i++) {
			names[i] = "out-" + i;
			types[i] = float.class;
		}
		updateOutputs(names, types);
	}

	public boolean process(Integer a, Double b, Short... c) {
		System.out.println("get data");
		Object[] addInputs = getAdditionalInputs();
		System.out.println(Arrays.toString(addInputs));
		if (addInputs != null && addInputs[0] != null)
			System.out.println("ts: " + getTimeStamp(3));
		Object[] vec = generateOuputsVector();
		vec[getOutputIndex("boolean")] = (boolean) (this.nbInput == 1 ? true : false);
		triggerOutput(vec, System.currentTimeMillis());
		return true;
	}

	public void setNbInput(int nbInput) {
		this.nbInput = nbInput;
		initStruct();
	}

	public void setNbOutput(int nbOutput) {
		this.nbOutput = nbOutput;
		initStruct();
	}
}
