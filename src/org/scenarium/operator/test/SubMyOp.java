/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class SubMyOp {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private int vache = 1;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public int getVache() {
		return this.vache;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void setVache(int vache) {
		int oldVache = this.vache;
		this.vache = vache;
		this.pcs.firePropertyChange("sub", oldVache, vache);
	}
}
