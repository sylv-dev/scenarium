/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.container.BeanInfo;

public class PropertySet1 implements PropertySet {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private int x = 5;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet set1;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet set2;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet set3;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public PropertySet getSet1() {
		return this.set1;
	}

	public PropertySet getSet2() {
		return this.set2;
	}

	public PropertySet getSet3() {
		return this.set3;
	}

	public int getX() {
		return this.x;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void setSet1(PropertySet set1) {
		PropertySet oldValue = this.set1;
		this.set1 = set1;
		this.pcs.firePropertyChange("set1", oldValue, set1);

	}

	public void setSet2(PropertySet set2) {
		PropertySet oldValue = this.set2;
		this.set2 = set2;
		this.pcs.firePropertyChange("set2", oldValue, set2);

	}

	public void setSet3(PropertySet set3) {
		PropertySet oldValue = this.set3;
		this.set3 = set3;
		this.pcs.firePropertyChange("set3", oldValue, set3);

	}

	@Override
	public void setX(int x) {
		int oldValue = this.x;
		this.x = x;
		this.pcs.firePropertyChange("x", oldValue, x);
	}

	@Override
	public String toString() {
		return BeanEditor.getBeanDesc(this).name;
	}
}
