package org.scenarium.operator.test;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

//import lib.javacv.javacv;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class CaLiCCo extends EvolvedOperator {

	BufferedImage image;

	@Override
	public void birth() {
		this.image = new BufferedImage(10, 30, BufferedImage.TYPE_INT_BGR);
	}

	public BufferedImage process(Integer i, BufferedImage img) {
		Graphics2D g2d = (Graphics2D) this.image.getGraphics();
		g2d.setColor(Color.YELLOW);
		g2d.drawArc(5, 10, 2, 2, 0, 360);
		return this.image;
	}

	@Override
	public void death() {
		// TODO Auto-generated method stub

	}

}