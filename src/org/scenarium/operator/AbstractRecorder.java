/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.basic.PathInfo;
import org.beanmanager.tools.FxUtils;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

import javafx.scene.input.MouseButton;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;

@BeanPropertiesInheritanceLimit
public abstract class AbstractRecorder extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Record directory for datas")
	@PathInfo(directory = true)
	@NotChangeableAtRuntime
	private File recordDirectory;
	@PropertyInfo(index = 1, info = "Indicate if the block has to record datas")
	private boolean recording = true;

	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private boolean recordingPropertyChanged = false;
	private Circle circle;

	@Override
	public Region getNode() {
		this.circle = new Circle();
		updateCircleColor();
		this.circle.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				setRecording(!this.recording);
				this.recordingPropertyChanged = true;
				e.consume();
			}
		});
		this.circle.setOnMouseClicked(e -> e.consume());
		StackPane sp = new StackPane(this.circle);
		sp.setPickOnBounds(false);
		this.circle.radiusProperty().bind(sp.widthProperty().divide(4));
		return sp;
	}

	private void updateCircleColor() {
		boolean recording = this.recording;
		FxUtils.runLaterIfNeeded(() -> {
			if (this.circle == null)
				return;
			int red = recording ? 1 : 0;
			Stop[] stops = new Stop[] { new Stop(0, new Color(red, 0, 0, 50 / 255.0)), new Stop(1, new Color(red, 0, 0, 5 / 255.0)) };
			this.circle.setFill(new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops));
		});
	}

	@Override
	public boolean needToBeSaved() {
		if (this.recordingPropertyChanged) {
			this.recordingPropertyChanged = false;
			return true;
		}
		return false;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public File getRecordDirectory() {
		return this.recordDirectory;
	}

	public void setRecordDirectory(File recordDirectory) {
		File oldRecordDirectory = this.recordDirectory;
		this.recordDirectory = recordDirectory;
		this.pcs.firePropertyChange("recordDirectory", oldRecordDirectory, recordDirectory);
	}

	public boolean isRecording() {
		return this.recording;
	}

	public void setRecording(boolean isRecording) {
		runLater(() -> {
			if (this.recording == isRecording)
				return;
			boolean oldRecording = this.recording;
			this.recording = isRecording;
			this.pcs.firePropertyChange("recording", oldRecording, this.recording);
			restart();
			updateCircleColor();
		});
	}
}
