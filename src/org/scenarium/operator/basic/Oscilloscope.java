/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import javax.vecmath.Point2i;

import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.RemoteBlock;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.scene.layout.Region;

public class Oscilloscope extends EvolvedOperator implements EvolvedVarArgsOperator, VisuableSchedulable {
	private double[][][] datas = new double[0][][];
	private long[] oldTs = new long[0];
	// private long[] baseTime = new long[0];
	private long baseTime = -1;
	@PropertyInfo(nullable = false, info = "Curve viewer")
	@BeanInfo(alwaysExtend = true, inline = true)
	private Viewer viewer;
	private int intervalTime = 3;

	public Oscilloscope() {
		setViewer(new Viewer());
		this.viewer.setPosition(new Point2i(0, 0));
		this.viewer.setDimension(new Point2i(640, 480));
	}

	@Override
	public void initStruct() throws Exception {
		// if(viewer != null)
		this.viewer.initStruct();
	}

	@Override
	public void birth() throws Exception {
		// if (viewer == null)
		// createViewer();
		this.viewer.birth();
	}

	@ParamInfo(in = "in")
	public void process(Object... values) {
		// long ellapsedTime = 0;
		if (values.length != this.datas.length) {
			double[][][] newDatas = new double[values.length][][];
			long[] newTs = new long[values.length];
			long[] newBT = new long[values.length];
			int i = 0;
			for (; i < Math.min(this.datas.length, newDatas.length); i++) {
				newDatas[i] = this.datas[i];
				newTs[i] = this.oldTs[i];
				// newBT[i] = baseTime[i];
				// long dt = oldTs[i] - baseTime[i];
				// if(dt > ellapsedTime)
				// ellapsedTime = dt;
			}
			for (; i < newBT.length; i++)
				newBT[i] = -1;
			this.datas = newDatas;
			this.oldTs = newTs;
			// baseTime = newBT;
		}
		for (int i = 0; i < values.length; i++) {
			Object value = values[i];
			double dValue = value instanceof Double ? (double) value
					: value instanceof Float ? (float) value
							: value instanceof Long ? (long) value
									: value instanceof Integer ? (int) value : value instanceof Short ? (short) value : value instanceof Byte ? (byte) value : Double.NaN;
			if (value == null || Double.isNaN(dValue))
				continue;
			double[][] oldDatas = this.datas[i];
			long newTs = getTimeStamp(i);
			if (oldDatas != null && this.oldTs[i] > newTs) {
				oldDatas = null;
				if (newTs < this.baseTime)
					this.baseTime = newTs;
			} else if (this.baseTime == -1)
				this.baseTime = newTs;
			this.oldTs[i] = newTs;
			int oldSize = oldDatas == null ? 0 : oldDatas[0].length;
			double time = (newTs - this.baseTime) / 1000.0;
			double beginTime = time - this.intervalTime;
			int firstIndex = 0;
			if (oldDatas != null) {
				double[] oldTs = oldDatas[0];
				while (oldTs[firstIndex] < beginTime && firstIndex != oldTs.length - 1)
					firstIndex++;
			}
			double[][] newData = new double[2][oldSize + 1 - firstIndex];
			if (oldSize != 0) {
				System.arraycopy(oldDatas[0], firstIndex, newData[0], 0, oldSize - firstIndex);
				System.arraycopy(oldDatas[1], firstIndex, newData[1], 0, oldSize - firstIndex);
			}
			newData[0][oldSize - firstIndex] = time;
			newData[1][oldSize - firstIndex] = dValue;
			this.datas[i] = newData;
			double[] d = newData[0];
			if (d[0] > d[d.length - 1])
				System.err.println("error");
		}

		int curveCpt = 0;
		for (int i = 0; i < this.datas.length; i++)
			if (this.datas[i] != null)
				curveCpt++;
		Curved[] curves = new Curved[curveCpt];
		curveCpt = 0;
		for (int i = 0; i < this.datas.length; i++)
			if (this.datas[i] != null)
				curves[curveCpt++] = new Curved(this.datas[i]);
		try {
			this.viewer.process(new CurveSeries(curves));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// private void createViewer() {
	// String beanName = BeanEditor.getBeanDesc(this).name + "-Viewer";
	// BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(Viewer.class, beanName);
	// if (beanDesc == null) {
	// setViewer(new Viewer());
	// viewer.setPosition(new Point2i(0, 0));
	// viewer.setDimension(new Point2i(640, 480));
	// BeanEditor.registerBean(viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.defaultDir);
	// } else
	// viewer = (Viewer) beanDesc.bean;
	// }

	@Override
	public void death() throws Exception {
		this.datas = new double[0][][];
		this.baseTime = -1;
		// if (viewer != null)
		this.viewer.death();
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	public int getIntervalTime() {
		return this.intervalTime;
	}

	@Override
	public Region getNode() {
		// if (viewer == null)
		// createViewer();
		return this.viewer.getNode();
	}

	public Viewer getViewer() {
		return this.viewer;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return Double.class.isAssignableFrom(additionalInput) || Float.class.isAssignableFrom(additionalInput) || Long.class.isAssignableFrom(additionalInput)
				|| Integer.class.isAssignableFrom(additionalInput) || Short.class.isAssignableFrom(additionalInput) || Byte.class.isAssignableFrom(additionalInput)
				|| double.class.isAssignableFrom(additionalInput) || float.class.isAssignableFrom(additionalInput) || long.class.isAssignableFrom(additionalInput)
				|| int.class.isAssignableFrom(additionalInput) || short.class.isAssignableFrom(additionalInput) || byte.class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean needToBeSaved() {
		return this.viewer.needToBeSaved();
	}

	@Override
	public void paint() {
		this.viewer.paint();
	}

	@Override
	public void setAnimated(boolean animated) {
		this.viewer.setAnimated(animated);
	}

	public void setIntervalTime(int intervalTime) {
		this.intervalTime = intervalTime;
	}

	public void setViewer(Viewer viewer) {
		// if (this.viewer != null)
		// BeanEditor.unregisterBean(this.viewer, true);
		if (viewer == null)
			return;
		this.viewer = viewer;
		viewer.setRemoteBlock(new RemoteBlock() {
			@Override
			public void addBlockNameChangeListener(BeanRenameListener listener) {
				Oscilloscope.this.addBlockNameChangeListener(listener);
			}

			@Override
			public String getBlockName() {
				return Oscilloscope.this.getBlockName();
			}

			@Override
			public void removeBlockNameChangeListener(BeanRenameListener listener) {
				Oscilloscope.this.removeBlockNameChangeListener(listener);
			}

			@Override
			public int getNbOutput() {
				return Oscilloscope.this.getNbOutput();
			}

			@Override
			public int getOutputIndex(String outputName) {
				return Oscilloscope.this.getOutputIndex(outputName);
			}

			@Override
			public boolean triggerOutput(Object[] outputValue, long timeStamp) {
				return Oscilloscope.this.triggerOutput(outputValue, timeStamp);
			}

			@Override
			public boolean updateOutputs(String[] names, Class<?>[] types) {
				return Oscilloscope.this.updateOutputs(names, types);
			}

			@Override
			public void runLater(Runnable runnable) {
				Oscilloscope.this.runLater(runnable);
			}
		});
		// if (viewer != null)
		// BeanEditor.registerBean(viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.defaultDir);
	}
}
