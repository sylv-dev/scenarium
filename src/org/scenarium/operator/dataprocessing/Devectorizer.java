/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import java.util.ArrayList;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.LinkChangeListener;

@BlockInfo(info = "Devectorize an array to get elements or sub part of this one")
public class Devectorizer extends EvolvedOperator implements EvolvedVarArgsOperator, LinkChangeListener {
	private Class<?> type;
	// private int nbOutput;
	@PropertyInfo(index = 2, info = "Pattern to split the input vector. All elements of the pattern must be separated with sema. The three possible elements are:\n"
			+ "\t-index of an element to get as output, formatted as an integer\n" + "\t-range of indexes to get as outputs, formatted as two integer separated with a -\n"
			+ "\t-range of indexes to get as an output array, formatted as two integer separated with a - which starts with a \"[\" and ends with a \"]\"\n" + "Exemple: 1, 2-4, [5-6]")
	private String pattern = "0-2";
	private Object[] computedPatterns = new Object[0];

	public Devectorizer() {
		setPattern(this.pattern);
	}

	public static void main(String[] args) {
		new Devectorizer().setPattern("0-2");
	}

	@Override
	public void initStruct() {
		if (this.type != null) {
			String inputsName = getOutputLinkToInputName()[0];
			ArrayList<String> names = new ArrayList<>();
			ArrayList<Class<?>> types = new ArrayList<>();
			for (int i = 0; i < this.computedPatterns.length; i++)
				if (this.computedPatterns[i] instanceof Integer) {
					names.add(inputsName + "-" + this.computedPatterns[i]);
					types.add(this.type);
				} else {
					names.add(inputsName + "-" + this.computedPatterns[i]);
					types.add(this.type.arrayType());
				}
			updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
		}

	}

	@Override
	public void birth() throws Exception {}

	@ParamInfo(in = "Array")
	public void process(Object... objects) {
		Object vector = objects[0];
		Object[] outputs = generateOuputsVector();
		if (outputs != null) {
			Class<?> type = vector.getClass().getComponentType(); // Sinon on remote j'ai plus le type...
			if (type == double.class) {
				double[] data = (double[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						double[] outputArray = new double[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == float.class) {
				float[] data = (float[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						float[] outputArray = new float[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == long.class) {
				long[] data = (long[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						long[] outputArray = new long[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == int.class) {
				int[] data = (int[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						int[] outputArray = new int[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == short.class) {
				short[] data = (short[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						short[] outputArray = new short[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == char.class) {
				char[] data = (char[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						char[] outputArray = new char[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == byte.class) {
				byte[] data = (byte[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						byte[] outputArray = new byte[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else if (type == boolean.class) {
				boolean[] data = (boolean[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						boolean[] outputArray = new boolean[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			} else {
				Object[] data = (Object[]) vector;
				for (int i = 0; i < this.computedPatterns.length; i++) {
					Object cp = this.computedPatterns[i];
					if (cp instanceof Integer)
						outputs[i] = data[(Integer) cp];
					else {
						Interval interval = (Interval) cp;
						int end = interval.end + 1;
						int begin = interval.begin;
						Object[] outputArray = new Object[end - begin];
						if (end > data.length)
							end = data.length;
						if (begin < end) {
							System.arraycopy(data, begin, outputArray, 0, end - begin);
							outputs[i] = outputArray;
						}
					}
				}
			}
			triggerOutput(outputs, getTimeStamp(0));
		}
	}

	@Override
	public void death() throws Exception {}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return inputsType.length == 0;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) { // bizare
		return inputsType.length != 0 && additionalInput.isArray();
	}

	@Override
	public void linkChanged(int indexOfInput) {
		if (indexOfInput == 0) {
			this.type = getOutputLinkToInputType()[0];
			if (this.type == double[].class)
				this.type = double.class;
			else if (this.type == float[].class)
				this.type = float.class;
			else if (this.type == long[].class)
				this.type = long.class;
			else if (this.type == int[].class)
				this.type = int.class;
			else if (this.type == short[].class)
				this.type = short.class;
			else if (this.type == char[].class)
				this.type = char.class;
			else if (this.type == byte[].class)
				this.type = byte.class;
			else if (this.type == boolean[].class)
				this.type = boolean.class;
			else if (this.type == null)
				this.type = null;
			else if (this.type.isArray())
				this.type = this.type.getComponentType();
			initStruct();
		}
	}

	// @Override
	// public int getNbOutput() {
	// return nbOutput;
	// }
	//
	// public void setNbOutput(int nbOutput) {
	// this.nbOutput = nbOutput;
	// initStruct();
	// }

	public String getPattern() {
		return this.pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
		// Build patterns
		try {
			ArrayList<Object> computedPattern = new ArrayList<>();
			for (String subPattern : pattern.split(",")) {
				subPattern = subPattern.trim();
				int separator = subPattern.indexOf("-");
				if (separator != -1) {
					String begin = subPattern.substring(0, separator);
					String end = subPattern.substring(separator + 1);
					boolean isSubArray = begin.startsWith("[") && end.endsWith("]");
					if (isSubArray) {
						begin = begin.substring(1);
						end = end.substring(0, end.length() - 1);
						computedPattern.add(new Interval(Integer.parseInt(begin), Integer.parseInt(end)));
					} else {
						int endIndex = Integer.parseInt(end) + 1;
						for (int i = Integer.parseInt(begin); i < endIndex; i++)
							computedPattern.add(i);
					}
				} else
					computedPattern.add(Integer.parseInt(subPattern));
			}
			if (computedPattern.isEmpty()) {
				this.computedPatterns = new Object[0];
				return;
			}
			// Sort patterns
			computedPattern.sort((a, b) -> Integer.compare(a instanceof Integer ? (Integer) a : (((Interval) a).begin + ((Interval) a).end) / 2,
					b instanceof Integer ? (Integer) b : (((Interval) b).begin + ((Interval) b).end) / 2));
			// Detect collision
			Object previousPattern = computedPattern.get(0);
			for (int i = 1; i < computedPattern.size(); i++) {
				Object nextPattern = computedPattern.get(i);
				int endPreviousPattern = previousPattern instanceof Integer ? (Integer) previousPattern : ((Interval) previousPattern).end;
				int beginNextPattern = nextPattern instanceof Integer ? (Integer) nextPattern : ((Interval) nextPattern).begin;
				if (endPreviousPattern >= beginNextPattern) {
					System.err.println(getBlockName() + ": Collision between " + (previousPattern instanceof Integer ? "Element" : "Array") + ": " + previousPattern + " end "
							+ (nextPattern instanceof Integer ? "Element" : "Array") + ": " + nextPattern);
					this.computedPatterns = new Object[0];
					return;
				}
				previousPattern = nextPattern;
			}
			this.computedPatterns = computedPattern.toArray(new Object[] { computedPattern.size() });
		} catch (NumberFormatException e) {
			System.err.println(getBlockName() + ": Malformed pattern");
			this.computedPatterns = new Object[0];
		}
		initStruct();
	}
}

class Interval {
	public final int begin;
	public final int end;

	public Interval(int begin, int end) {
		this.begin = begin;
		this.end = end;
	}

	@Override
	public String toString() {
		return "[" + this.begin + "-" + this.end + "]";
	};
}