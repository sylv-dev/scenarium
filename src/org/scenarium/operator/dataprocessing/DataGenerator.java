/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import static org.objectweb.asm.Opcodes.ACC_PRIVATE;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.CHECKCAST;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.IFNULL;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INVOKEINTERFACE;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.PUTFIELD;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V10;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.LoadModuleListener;
import org.beanmanager.editors.DynamicAnnotationBean;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.BooleanProperty;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class DataGenerator extends EvolvedOperator implements UpdatableViewBean, BeanRenameListener, DynamicAnnotationBean, LoadModuleListener {
	private static final WeakHashMap<Class<?>, Class<? extends DataContainer>> CONTAINERS_CLASSES = new WeakHashMap<>();
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	@PropertyInfo(index = 0, nullable = false, info = "Type of the generated data")
	@DynamicPossibilities(possibleChoicesMethod = "getDataTypes")
	private String dataType = double.class.getName();
	@PropertyInfo(index = 1, info = "The dimension of the generated data")
	@NumberInfo(min = 0)
	private int dimension = 0;
	@PropertyInfo(index = 2, nullable = false, savable = false, info = "Value of the data")
	@BeanInfo(inline = true, alwaysExtend = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getType")
	private DataContainer valueContainer;
	@PropertyInfo(index = 3, unit = "ms")
	@NumberInfo(min = 0)
	private int stepTime = 50;
	private Timer timer;
	private boolean isWarning;
	private Class<?> baseType;
	private PropertyEditor<?> editor;

	public static void main(String[] args) {
		try {
			parseType("double");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public DataGenerator() {
		updateType();
		BeanManager.addWeakRefModuleLoadedListener(this);
	}

	public static Class<?> parseType(final String className) throws ClassNotFoundException {
		switch (className) {
		case "boolean":
			return boolean.class;
		case "byte":
			return byte.class;
		case "short":
			return short.class;
		case "int":
			return int.class;
		case "long":
			return long.class;
		case "float":
			return float.class;
		case "double":
			return double.class;
		case "char":
			return char.class;
		case "void":
			return void.class;
		default:
			return BeanManager.getClassFromDescriptor(className.contains(".") ? className : "java.lang.".concat(className));
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	@Override
	public void birth() {
		onResume(() -> startTimer());
		onPause(() -> death());
		startTimer();
	}

	private void startTimer() {
		this.timer = new Timer();
		this.isWarning = false;
		onStart(() -> this.timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				if (DataGenerator.this.valueContainer != null) {
					Object val = DataGenerator.this.valueContainer.getObjectValue();
					if (val != null) {
						triggerOutput(new Object[] { val });
						if (DataGenerator.this.isWarning) {
							setWarning(null);
							DataGenerator.this.isWarning = false;
						}
					} else if (!DataGenerator.this.isWarning) {
						setWarning("No data set");
						DataGenerator.this.isWarning = true;
					}
				}
			}
		}, 0, this.stepTime));
	}

	@SuppressWarnings("unchecked")
	private Class<? extends DataContainer> createAndLoadValueClass() {
		try {
			this.baseType = parseType(this.dataType);
		} catch (ClassNotFoundException ex) { // Si déchargement de module
			// ex.printStackTrace();
			// System.err.println(getClass().getSimpleName() + " No definition for the class with the specified name: " + dataType + " can be found");
			setDataType(double.class.getName());
		}
		Class<?> type = this.baseType;
		for (int i = 0; i < this.dimension; i++)
			type = type.arrayType();
		this.editor = PropertyEditorManager.findEditor(type, "");
		Class<? extends DataContainer> c = CONTAINERS_CLASSES.get(type);
		if (c != null)
			return c;

		c = (Class<? extends DataContainer>) new ValueContainerClassLoader(getHigherLevelClassLoader(type.getClassLoader(), getClass().getClassLoader()))
				.createValueContainerClass(getClassContainerFromType(this.baseType), type);
		CONTAINERS_CLASSES.put(type, c);
		// Class.forName(c.getName());
		return c;

	}

	private static ClassLoader getHigherLevelClassLoader(ClassLoader cl1, ClassLoader cl2) {
		ClassLoader parent = cl1;
		while (parent != null && parent.equals(cl2))
			parent = parent.getParent();
		return parent == null ? cl2 : cl1;
	}

	@Override
	public void death() {
		if (this.timer != null) {
			this.timer.cancel();
			this.timer.purge();
			this.timer = null;
		}
	}

	private String getClassContainerFromType(Class<?> type) {
		String base = getClass().getPackage().getName() + "." + type.getSimpleName();
		if (this.dimension != 0)
			base += this.dimension + "D";
		return base + "Container";
	}

	public Class<?>[] getType() {
		return this.dataType == null || this.dataType.isEmpty() ? new Class<?>[] {} : new Class<?>[] { createAndLoadValueClass() };
	}

	@Override
	public void initStruct() {
		removeBlockNameChangeListener(this); // TODO ifpresent ici
		addBlockNameChangeListener(this);
		if (this.valueContainer != null) {
			createAndLoadValueClass();
			if (this.baseType != null) {
				Class<?> type = this.baseType;
				for (int i = 0; i < this.dimension; i++)
					type = type.arrayType();
				updateOutputs(new String[] { getBlockName() }, new Class[] { type });
				return;
			}
		}
		updateOutputs(new String[0], new Class[0]);
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		// runLater(() -> initStruct());
		initStruct();
	}

	public void process() {}

	public String getDataType() {
		return this.dataType;
	}

	public String[] getDataTypes() {
		ArrayList<String> dataTypes = new ArrayList<>();
		PropertyEditorManager.forEachEditor((type, editor) -> {
			PropertyEditor<?> pe;
			if (!type.equals(Runnable.class) && !type.equals(BooleanProperty.class) && !type.equals(EncodingMessageProperties[].class) && !type.isPrimitive()
					&& (pe = PropertyEditorManager.findEditor(type, "", null, null)) != null && pe.hasCustomEditor())
				dataTypes.add(BeanManager.getDescriptorFromClass(type));
			Class<?> primitiveClass = BeanManager.toPrimitive(type);
			if (!primitiveClass.equals(type))
				dataTypes.add(primitiveClass.getName());
		});
		Collections.sort(dataTypes);
		return dataTypes.toArray(new String[dataTypes.size()]);
	}

	public void setDataType(String dataType) {
		if (dataType.isEmpty())
			return;
		String oldDataType = this.dataType;
		this.dataType = dataType;
		this.pcs.firePropertyChange("DataType", oldDataType, this.dataType);
		runLater(() -> updateType());
	}

	public int getDimension() {
		return this.dimension;
	}

	public void setDimension(int dimension) {
		int oldDimension = this.dimension;
		this.dimension = dimension;
		this.pcs.firePropertyChange("Dimension", oldDimension, this.dimension);
		runLater(() -> updateType());
	}

	public int getStepTime() {
		return this.stepTime;
	}

	public void setStepTime(int stepTime) {
		int oldStepTime = this.stepTime;
		this.stepTime = stepTime;
		this.pcs.firePropertyChange("StepTime", oldStepTime, this.stepTime);
		restartLater();
	}

	@PropertyInfo(viewable = false)
	public String getValue() {
		if (this.valueContainer != null)
			this.editor.setValueFromObj(this.valueContainer.getObjectValue());
		return this.editor.getAsText();
	}

	public void setValue(String value) {
		this.editor.setAsText(value);
		Object v = this.editor.getValue();
		if (this.valueContainer != null && (v != null || !this.baseType.isPrimitive())) {
			this.valueContainer.setValueAsObject(v);
			this.editor.setValueFromObj(this.valueContainer.getObjectValue());
		}
	}

	public DataContainer getValueContainer() {
		return this.valueContainer;
	}

	public void setValueContainer(DataContainer valueContainer) {
		if (valueContainer == null)
			return;
		updateView();
		DataContainer oldValueContainer = this.valueContainer;
		valueContainer.setPropertyChangeHandler(() -> this.editor.setValueFromObj(valueContainer.getObjectValue()));
		this.valueContainer = valueContainer;
		this.pcs.firePropertyChange("ValueContainer", oldValueContainer, this.valueContainer);
	}

	private void updateType() {
		if (this.dataType != null && !this.dataType.isEmpty())
			try {
				Class<? extends DataContainer> c = createAndLoadValueClass();
				if (this.valueContainer == null || this.valueContainer.getObjectValue() == null || !this.valueContainer.getObjectValue().getClass().equals(c))
					setValueContainer(c.getConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				setValueContainer(null);
				e.printStackTrace();
			}
		else
			setValueContainer(null);
		initStruct();
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "ValueContainer", false);
	}

	class ValueContainerClassLoader extends ClassLoader {
		private static final String PROPERTYCHANGEHANDLERNAME = "propertyChangeHandler";

		public ValueContainerClassLoader(ClassLoader parent) {
			super(parent);
		}

		private void createGetter(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType) {
			Type type = Type.getType(propertyType);
			String methodName = "get" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, methodName, "()" + type, null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, classInternalName, propertyName, type.getDescriptor());
			mv.visitInsn(type.getOpcode(IRETURN));
			mv.visitMaxs(0, 0);
			mv.visitEnd();
		}

		private void createProperty(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType) {
			FieldVisitor fv = cw.visitField(ACC_PRIVATE, propertyName, Type.getType(propertyType).getDescriptor(), null, null);
			if (fv != null)
				fv.visitEnd();
			fv = cw.visitField(ACC_PRIVATE, "propertyChangeHandler", Type.getType(Runnable.class).getDescriptor(), null, null);
			if (fv != null)
				fv.visitEnd();

			createValueContainerGetter(cw, classInternalName, propertyName, propertyType);
			createValueContainerSetter(cw, classInternalName, propertyName, propertyType);
			createSetter(cw, classInternalName, PROPERTYCHANGEHANDLERNAME, Runnable.class, false);
			createSetter(cw, classInternalName, propertyName, propertyType, true);
			createGetter(cw, classInternalName, propertyName, propertyType);
		}

		private void createSetter(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType, boolean propertyChangeHandler) {
			Type type = Type.getType(propertyType);
			String typeDescriptor = type.getDescriptor();
			String methodName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, methodName, "(" + typeDescriptor + ")V", null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(type.getOpcode(ILOAD), 1);
			mv.visitFieldInsn(PUTFIELD, classInternalName, propertyName, typeDescriptor);
			if (propertyChangeHandler) {
				// Add propertyChangeHandler
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, classInternalName, PROPERTYCHANGEHANDLERNAME, Type.getType(Runnable.class).getDescriptor());
				Label lookup = new Label();
				mv.visitJumpInsn(IFNULL, lookup);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, classInternalName, PROPERTYCHANGEHANDLERNAME, Type.getType(Runnable.class).getDescriptor());
				mv.visitMethodInsn(INVOKEINTERFACE, Type.getInternalName(Runnable.class), "run", "()V", true);
				mv.visitLabel(lookup);
			}
			mv.visitInsn(RETURN);
			mv.visitMaxs(0, 0);
			mv.visitEnd();
		}

		private void createToString(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType) {
			Type typeString = Type.getType(String.class);
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "toString", "()" + typeString, null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, Type.getInternalName(Object.class), "getClass", "()" + Type.getType(Class.class), false);
			mv.visitMethodInsn(INVOKEVIRTUAL, Type.getInternalName(Class.class), "getSimpleName", "()" + typeString, false);
			mv.visitInsn(typeString.getOpcode(IRETURN));
			mv.visitMaxs(0, 0);
			mv.visitEnd();
		}

		public Class<?> createValueContainerClass(String className, Class<?> propertyType) {
			ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
			String objectInternalName = Type.getInternalName(Object.class);
			String classInternalName = className.replace('.', '/');
			cw.visit(V10, ACC_PUBLIC, className.replace('.', '/'), null, objectInternalName, new String[] { Type.getInternalName(DataContainer.class) });
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESPECIAL, objectInternalName, "<init>", "()V", false);
			mv.visitInsn(RETURN);
			mv.visitMaxs(0, 0);
			mv.visitEnd();
			createProperty(cw, classInternalName, "value", propertyType);
			createToString(cw, classInternalName, "value", propertyType);
			cw.visitEnd();
			byte[] classBytes = cw.toByteArray();
			return defineClass(className, classBytes, 0, classBytes.length);
		}

		private void createValueContainerSetter(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType) {
			String methodName = "setValueAsObject";
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, methodName, "(" + Type.getType(Object.class).getDescriptor() + ")V", null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitVarInsn(ALOAD, 1);
			Class<?> wrapperTypeClass = BeanManager.toWrapper(propertyType);
			mv.visitTypeInsn(CHECKCAST, Type.getInternalName(wrapperTypeClass));
			if (wrapperTypeClass != propertyType)
				mv.visitMethodInsn(INVOKEVIRTUAL, Type.getInternalName(wrapperTypeClass), propertyType.getName() + "Value", "()" + Type.getType(propertyType), false);
			mv.visitFieldInsn(PUTFIELD, classInternalName, propertyName, Type.getType(propertyType).getDescriptor());
			mv.visitInsn(RETURN);
			mv.visitMaxs(0, 0);
			mv.visitEnd();
		}

		private void createValueContainerGetter(ClassWriter cw, String classInternalName, String propertyName, Class<?> propertyType) {
			Type type = Type.getType(propertyType);
			Type typeObj = Type.getType(Object.class);
			String methodName = "getObjectValue";
			MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, methodName, "()" + typeObj, null, null);
			mv.visitVarInsn(ALOAD, 0);
			mv.visitFieldInsn(GETFIELD, classInternalName, propertyName, Type.getType(propertyType).getDescriptor());
			if (propertyType.isPrimitive()) {
				Class<?> wrapperTypeClass = BeanManager.toWrapper(propertyType);
				mv.visitMethodInsn(INVOKESTATIC, Type.getInternalName(wrapperTypeClass), "valueOf", "(" + type + ")" + Type.getType(wrapperTypeClass), false);
			}
			mv.visitInsn(typeObj.getOpcode(IRETURN));
			mv.visitMaxs(0, 0);
			mv.visitEnd();
		}
	}

	@Override
	public void loaded(Module module) {
		fireAnnotationChanged(this, "dataType");
	}

	@Override
	public Runnable modified(Module module) {
		return () -> fireAnnotationChanged(this, "dataType");
	}

	@Override
	public void unloaded(Module module) {
		fireAnnotationChanged(this, "dataType");
	}
}