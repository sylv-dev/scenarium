/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image.conversion;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class ImgDecoder {
	public void birth() {}

	public void death() {}

	@ParamInfo(in = "encodedData", out = "img")
	public BufferedImage process(byte[] encodedData) {
		try {
			return ImageIO.read(new ByteArrayInputStream(encodedData));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
