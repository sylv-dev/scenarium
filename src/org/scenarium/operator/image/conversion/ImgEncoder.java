/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image.conversion;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class ImgEncoder extends EvolvedOperator implements DynamicEnableBean {
	@PropertyInfo(index = 0, info = "Image writter format name")
	@DynamicPossibilities(possibleChoicesMethod = "getWriterFormatNames")
	private String imgFormatName;
	@PropertyInfo(index = 1, info = "Compression quality for jpeg images")
	@NumberInfo(min = 0, max = 1, controlType = ControlType.TEXTFIELD_AND_SLIDER)
	private float quality = 0.5f;

	private ByteArrayOutputStream stream;
	private ImageOutputStream ios;
	private ImageWriter writer;
	private JPEGImageWriteParam iwparam;
	private IIOImage iioi;
	private boolean illegalArgument;

	public ImgEncoder() {}

	public ImgEncoder(String imgFormatName, float quality) {
		this.imgFormatName = imgFormatName;
		this.quality = quality;
	}

	@Override
	public void birth() {
		this.illegalArgument = false;
		if (this.imgFormatName == null || this.imgFormatName.isEmpty()) {
			System.err.println(getBlockName() + ": No image writter format name");
			return;
		}
		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(this.imgFormatName);
		if (iter.hasNext())
			this.writer = iter.next();
		if (isJpgFormat()) {
			this.iwparam = new JPEGImageWriteParam(Locale.getDefault());
			this.iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			this.iwparam.setCompressionQuality(this.quality);
		}
		this.stream = new ByteArrayOutputStream();
		this.iioi = new IIOImage(new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR), null, null);
		try {
			this.ios = ImageIO.createImageOutputStream(this.stream);
			this.writer.setOutput(this.ios);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void death() {
		if (this.writer != null) {
			this.writer.dispose();
			this.writer = null;
		}
		try {
			if (this.ios != null) {
				this.ios.close();
				this.ios = null;
			}
			if (this.stream != null) {
				this.stream.close();
				this.stream = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.iwparam = null;
		this.iioi = null;
	}

	public String getImgFormatName() {
		return this.imgFormatName;
	}

	public float getQuality() {
		return this.quality;
	}

	public String[] getWriterFormatNames() {
		ArrayList<String> writterformats = new ArrayList<>();
		HashSet<Class<?>> set = new HashSet<>();
		for (String writterFormatName : ImageIO.getWriterFormatNames()) {
			Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(writterFormatName);
			while (it.hasNext()) {
				Class<?> writterClass = it.next().getClass();
				if (!set.contains(writterClass)) {
					set.add(writterClass);
					writterformats.add(writterFormatName.toUpperCase());
				}
			}
		}
		return writterformats.toArray(new String[writterformats.size()]);
	}

	private boolean isJpgFormat() {
		return this.imgFormatName != null && (this.imgFormatName.equals("JPG") || this.imgFormatName.equals("JPEG"));
	}

	@ParamInfo(in = "img", out = "encodedData")
	public byte[] process(BufferedImage img) {
		if (this.ios != null)
			try {
				this.stream.reset();
				this.iioi.setRenderedImage(img);
				this.writer.write(null, this.iioi, this.iwparam);
				this.ios.flush();
				return this.stream.toByteArray();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				if (!this.illegalArgument) {
					this.illegalArgument = true;
					System.err.println(getBlockName() + ": " + e.getMessage());
				}
			}
		return null;
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "quality", isJpgFormat());
	}

	public void setImgFormatName(String imgFormatName) {
		this.imgFormatName = imgFormatName;
		setEnable();
		restartLater();
	}

	public void setQuality(float quality) {
		this.quality = quality;
		JPEGImageWriteParam iwparam = this.iwparam;
		if (iwparam != null)
			iwparam.setCompressionQuality(quality);
	}

}
