/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.can;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.struct.Selection;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.LoadedListener;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.DBCMessage;
import org.scenarium.communication.can.dbc.DBCSignal;
import org.scenarium.communication.can.dbc.SignalIdentifier;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class CanDBCDecoder extends EvolvedOperator implements LoadedListener, DynamicEnableBean, UpdatableViewBean {
	@BeanInfo(alwaysExtend = true)
	@PropertyInfo(index = 0, info = "DBC database for decoding datas")
	private CanDBC canDBC;
	@DynamicChoiceBox(possibleChoicesMethod = "getSignalsIdentifier")
	@PropertyInfo(index = 1, info = "Output filters based on available signal in the DBC database")
	private Selection<SignalIdentifier> filters;
	@PropertyInfo(index = 2, info = "Trigger enum as string or as double value")
	private boolean enumAsString;
	private HashMap<Integer, ArrayList<DBCSignal>> canDbcOutput;

	@Override
	public void birth() {
		if (this.canDBC != null)
			this.canDBC.loadIfNeeded();
		updateCanDbcOutput();
	}

	@Override
	public void death() {}

	public CanDBC getCanDBC() {
		return this.canDBC;
	}

	public ArrayList<DBCSignal> getDatasProp(CanTrame canTrame) {
		if (this.canDbcOutput == null)
			return null;
		return this.canDbcOutput.get(canTrame.getId());
	}

	public Selection<SignalIdentifier> getFilters() {
		return this.filters;
	}

	public SignalIdentifier[] getSignalsIdentifier() {
		if (this.canDBC == null)
			return new SignalIdentifier[0];
		ArrayList<SignalIdentifier> si = this.canDBC.getSignalsIdentifier();
		return si.toArray(new SignalIdentifier[si.size()]);
	}

	@Override
	public void initStruct() {
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		if (this.canDBC != null && this.filters != null && !this.filters.getSelected().isEmpty()) {
			this.canDBC.loadIfNeeded();
			ArrayList<SignalIdentifier> selectedOutputs = new ArrayList<>(this.filters.getSelected());
			selectedOutputs.sort((a, b) -> a.getId() < b.getId() ? -1 : a.getId() == b.getId() ? a.getName().compareTo(b.getName()) : 1);
			for (SignalIdentifier canSignalIdentifier : selectedOutputs) {
				DBCMessage message = this.canDBC.getPropById(canSignalIdentifier.getId());
				if (message != null) {
					DBCSignal signal = message.getSignal(canSignalIdentifier.getName());
					if (signal != null) {
						names.add(canSignalIdentifier.getId() + "_" + signal.name);
						types.add(this.enumAsString ? signal.getDataType() : Double.class);
					}
				}
			}
		}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	public boolean isEnumAsString() {
		return this.enumAsString;
	}

	@Override
	public void loaded() {
		updateFilters();
		setEnable();
		updateView();
	}

	private void updateFilters() {
		runLater(() -> {
			ArrayList<Object> toRemove = new ArrayList<>();
			if (this.canDBC == null || !this.canDBC.hasMessage() || this.filters == null || this.filters.getSelected().isEmpty())
				this.filters = null;
			else {
				ArrayList<SignalIdentifier> variablesIdentifier = this.canDBC.getSignalsIdentifier();
				for (SignalIdentifier selection : this.filters.getSelected())
					if (!variablesIdentifier.contains(selection))
						toRemove.add(selection);
				if (!toRemove.isEmpty()) {
					HashSet<SignalIdentifier> sel = this.filters.getSelected();
					sel.removeAll(toRemove);
					this.filters = new Selection<>(sel, SignalIdentifier.class);
				}
			}
			initStruct();
		});
	}

	public void process(CanTrame canTrame) {
		if (this.canDBC == null)
			return;
		ArrayList<DBCSignal> datasProp = getDatasProp(canTrame);
		if (datasProp == null)
			return;
		Object[] outputs = generateOuputsVector();
		for (DBCSignal canDBCProp : datasProp)
			outputs[getOutputIndex(canTrame.getId() + "_" + canDBCProp.name)] = this.enumAsString && canDBCProp.isEnum() ? canDBCProp.decodeAsString(canTrame.getData())
					: canDBCProp.decode(canTrame.getData());
		triggerOutput(outputs, getTimeStamp(0));
	}

	public void setCanDBC(CanDBC canDBC) {
		if (this.canDBC != null)
			this.canDBC.removeOpenListener(this);
		this.canDBC = canDBC;
		initStruct();
		// fireStructChanged();
		if (canDBC != null)
			canDBC.addOpenListener(this);
		loaded();
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "filters", this.canDBC != null && this.canDBC.hasMessage());
	}

	public void setEnumAsString(boolean enumAsString) {
		runLater(() -> {
			this.enumAsString = enumAsString;
			initStruct();
		});
	}

	public void setFilters(Selection<SignalIdentifier> filters) {
		this.filters = filters;
		updateFilters();
	}

	private void updateCanDbcOutput() {
		this.canDbcOutput = new HashMap<>();
		if (this.canDBC == null || !this.canDBC.hasMessage()) {
			this.canDbcOutput = null;
			return;
		}
		HashSet<SignalIdentifier> outfilt = this.filters == null ? new HashSet<>() : this.filters.getSelected();
		for (SignalIdentifier canSignalIdentifier : outfilt) {
			DBCMessage canDBCId = this.canDBC.getPropById(canSignalIdentifier.getId());
			ArrayList<DBCSignal> canFilterDBCProps = this.canDbcOutput.get(canSignalIdentifier.getId());
			if (canFilterDBCProps == null) {
				canFilterDBCProps = new ArrayList<>();
				this.canDbcOutput.put(canSignalIdentifier.getId(), canFilterDBCProps);
			}
			canFilterDBCProps.add(canDBCId.getSignal(canSignalIdentifier.getName()));
		}
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "filters", false);
	}
}
