/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.serial;

public enum DataBits {
	DATABITS_5(5), DATABITS_6(6), DATABITS_7(7), DATABITS_8(8), DATABITS_9(9);

	private final int value;

	private DataBits(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
