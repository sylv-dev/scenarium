/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.operator.communication.serial.DataType;

@BeanPropertiesInheritanceLimit
public abstract class AbstractNetwork extends EvolvedOperator implements BeanRenameListener {
	@PropertyInfo(index = 100, info = "Specify the data input type")
	protected DataType inDataType = DataType.BYTE;
	@PropertyInfo(index = 101, info = "Specify the data output type")
	protected DataType outDataType = DataType.BYTE;
	@PropertyInfo(index = 102, info = "Generate an output that send true when the connection is established and false when the connection is lost")
	private boolean connectionFlag = false;
	@PropertyInfo(index = 103, info = "Explicitly displays in the console all operations performed")
	protected boolean verbose = false;

	private Object[] outputValues;
	private long[] timeStamps;

	public AbstractNetwork() {}

	public AbstractNetwork(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose) {
		this.inDataType = inDataType;
		this.outDataType = outDataType;
		this.connectionFlag = connectionFlag;
		this.verbose = verbose;
	}

	protected void connectionEstablished(boolean established) {
		if (this.verbose && established)
			System.out.println(getBlockName() + ": Connection " + (established ? "established" : "lost"));
		if (this.connectionFlag) {
			int flagIndex = getOutputIndex("flag");
			if (flagIndex != -1) {
				for (int i = 0; i < this.outputValues.length; i++)
					this.outputValues[i] = null;
				this.outputValues[flagIndex] = established;
				this.timeStamps[flagIndex] = System.currentTimeMillis();
				triggerOutput(this.outputValues, this.timeStamps);
				this.outputValues[flagIndex] = null;
			}
		}
	}

	public DataType getInDataType() {
		return this.inDataType;
	}

	public DataType getOutDataType() {
		return this.outDataType;
	}

	@Override
	public void initStruct() {
		if (this.inDataType != DataType.DISABLE && isInputAvailable())
			updateInputs(new String[] { "in" }, new Class[] { this.inDataType == DataType.BYTE ? byte[].class : this.inDataType == DataType.STRING ? String.class : Object.class });
		else
			updateInputs(new String[0], new Class[0]);

		int nbOutput;
		if (this.outDataType != DataType.DISABLE && isOutputAvailable()) {
			if (this.connectionFlag) {
				updateOutputs(new String[] { getBlockName(), "flag" },
						new Class[] { this.outDataType == DataType.BYTE ? byte[].class : this.outDataType == DataType.STRING ? String.class : Object.class, Boolean.class });
				nbOutput = 2;
			} else {
				updateOutputs(new String[] { getBlockName() }, new Class[] { this.outDataType == DataType.BYTE ? byte[].class : this.outDataType == DataType.STRING ? String.class : Object.class });
				nbOutput = 1;
			}
		} else if (this.connectionFlag) {
			updateOutputs(new String[] { "flag" }, new Class[] { Boolean.class });
			nbOutput = 1;
		} else {
			updateOutputs(new String[0], new Class[0]);
			nbOutput = 0;
		}
		if (nbOutput != 0) {
			this.outputValues = new Object[nbOutput];
			this.timeStamps = new long[nbOutput];
		}
		removeBlockNameChangeListener(this);
		addBlockNameChangeListener(this);
	}

	public boolean isConnectionFlag() {
		return this.connectionFlag;
	}

	protected boolean isInputAvailable() {
		return true;
	}

	protected boolean isOutputAvailable() {
		return true;
	}

	public boolean isVerbose() {
		return this.verbose;
	}

	public void setConnectionFlag(boolean connectionFlag) {
		this.connectionFlag = connectionFlag;
		restartAndReloadStructLater();
	}

	public void setInDataType(DataType inDataType) {
		this.inDataType = inDataType;
		restartAndReloadStructLater();
	}

	public void setOutDataType(DataType outDataType) {
		this.outDataType = outDataType;
		restartAndReloadStructLater();
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	protected void triggerData(Object data) {
		this.outputValues[0] = data;
		this.timeStamps[0] = System.currentTimeMillis();
		triggerOutput(this.outputValues, this.timeStamps);
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		initStruct();
	}
}
