/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math;

public class PolyFitting implements SmoothingFunction {
	private int degree = 2;
	private double[] result;
	// private int nbComputedPoints = -1;

	// private double[] xs;
	// private double[] ys;

	@Override
	public SmoothingFunction addPoint(double x, double y, double w) {
		throw new UnsupportedOperationException("Pas implémenté pour le moment...");
	}

	@Override
	public PolyFitting clone() {
		PolyFitting scs = parametricClone();
		if (this.result != null)
			scs.result = this.result.clone();
		return scs;
	}

	@Override
	public double evaluate(double x) {
		return PolynomialsTools.evaluatePoly(this.result, x);
	}

	@Override
	public double[] getData() {
		double[] data = new double[2 + this.result.length];
		data[0] = 0;
		data[1] = this.degree;
		for (int i = 0; i < data.length; i++)
			data[i + 1] = this.result[i];
		return null;
	}

	@Override
	public double[] getDefinitionInterval() {
		return new double[] { Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY };
	}

	public int getDegree() {
		return this.degree;
	}

	@Override
	public SmoothingFunction getParallelFunction(double distance) {
		return null;
	}

	@Override
	public double[] getPoly() {
		return this.result;
	}

	@Override
	public double[] getTangente(double x) {
		return PolynomialsTools.tangent(this.result, x);
	}

	private static void gjDivide(double[][] a, int i, int j, int m) {
		for (int q = j + 1; q < m; q++)
			a[i][q] /= a[i][j];
		a[i][j] = 1;
	}

	private static void gjEchelonize(double[][] a) {
		int n = a.length;
		int m = a[0].length;
		int i = 0;
		int j = 0;
		int k;
		double[] temp;
		while (i < n && j < m) {
			k = i;
			while (k < n && a[k][j] == 0)
				k++;
			if (k < n) {
				if (k != i) {
					temp = a[i];
					a[i] = a[k];
					a[k] = temp;
				}
				if (a[i][j] != 1)
					gjDivide(a, i, j, m);
				gjEliminate(a, i, j, n, m);
				i++;
			}
			j++;
		}
	}

	private static void gjEliminate(double[][] wp, int i, int j, int n, int m) {
		for (int k = 0; k < n; k++)
			if (k != i && wp[k][j] != 0) {
				for (int q = j + 1; q < m; q++)
					wp[k][q] -= wp[k][j] * wp[i][q];
				wp[k][j] = 0;
			}
	}

	@Override
	public void loadData(double[] data) {
		this.degree = (int) data[1];
		this.result = new double[data.length - 2];
		for (int i = 0; i < data.length; i++)
			this.result[i] = data[i + 2];
	}

	@Override
	public PolyFitting parametricClone() {
		PolyFitting scs = new PolyFitting();
		scs.setDegree(this.degree);
		return scs;
	}

	/** Returns a polynomial that seeks to minimize the square of the total distance between the set of points and the polynomial.
	 *
	 * @param y2
	 *
	 * @param pt
	 *
	 * @return A polynomial */
	@Override
	public void resolve(double[] xs, double[] ys) {
		// this.xs = xs;
		// this.ys = ys;
		int p = this.degree + 1;
		int n = xs.length;
		int rs = 2 * p - 1;
		double[][] m = new double[p][p + 1];
		double[] mpc = new double[rs];
		mpc[0] = n;
		for (int j = 0; j < n; j++) {
			double x = xs[j];
			double y = ys[j];
			for (int r = 1; r < rs; r++)
				mpc[r] += Math.pow(x, r);
			m[0][p] += y;
			for (int r = 1; r < p; r++)
				m[r][p] += Math.pow(x, r) * y;
		}
		for (int r = 0; r < p; r++)
			for (int c = 0; c < p; c++)
				m[r][c] = mpc[r + c];
		gjEchelonize(m);
		this.result = new double[p];
		for (int j = 0; j < p; j++)
			this.result[j] = m[j][p];
		// nbComputedPoints = n;
	}

	/** Returns a polynomial that seeks to minimize the square of the total distance between the set of weighted points and the polynomial.
	 *
	 * @param pt
	 *
	 * @return A polynomial */
	@Override
	public void resolve(double[] xs, double[] ys, double[] ws) {
		int p = this.degree + 1;
		int n = xs.length;
		int rs = 2 * p - 1;
		double[][] m = new double[p][p + 1];
		double[] mpc = new double[rs];
		mpc[0] = n;
		double cum = 0;
		for (double v : ws)
			cum += v;
		mpc[0] = cum;
		for (int j = 0; j < n; j++) {
			double x = xs[j];
			double y = ys[j];
			double w = ws[j];
			for (int r = 1; r < rs; r++)
				mpc[r] += w * Math.pow(x, r);
			m[0][p] += w * y;
			for (int r = 1; r < p; r++)
				m[r][p] += w * Math.pow(x, r) * y;
		}
		for (int r = 0; r < p; r++)
			for (int c = 0; c < p; c++)
				m[r][c] = mpc[r + c];
		gjEchelonize(m);
		this.result = new double[p];
		for (int j = 0; j < p; j++)
			this.result[j] = m[j][p];
	}

	// @Override
	// public int getNbComputedPoints() {
	// return nbComputedPoints;
	// }

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public void setPoly(double[] result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "PolyFit degree: " + this.degree;
	}

	@Override
	public void xTranslate(double delta) {
		this.result[0] += delta;
	}

	@Override
	public void yTranslate(double delta) {
		if (this.degree == 1)
			this.result[0] += this.result[1] + delta;
		else if (this.degree == 2) {
			this.result[0] += this.result[2] * delta * delta + this.result[1] * delta;
			this.result[1] += 2 * this.result[2] * delta;
		}
	}
}
