/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math.noise;

public class PerlinNoise1D extends PerlinNoise {

	public static void main(String[] args) {
		PerlinNoise1D pn = new PerlinNoise1D(1, 1 / 1000.0, 2, 6, 60 / 1000.0, 0);
		double min = Double.MAX_VALUE;
		double max = -Double.MAX_VALUE;
		int percentInRange = 0;
		for (int i = 0; i < 10000000; i++) {
			double val = pn.getValue(Math.random() * 1000000000);
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
			if (val >= -1 && val <= 1)
				percentInRange++;
			// System.out.println(val);
		}
		System.out.println(min + " -> " + max + ": " + percentInRange / 10000000.0);
	}

	public PerlinNoise1D() {
		super();
	}

	public PerlinNoise1D(double amplitude, double frequency, double lacunarity, int octaveCount, double persistence, int seed) {
		super(amplitude, frequency, lacunarity, octaveCount, persistence, seed);
	}

	public double getValue(double x) {
		double value = 0;
		double signal = 0;
		double curPersistence = 1;
		double nx;
		int seed;
		x *= this.frequency;
		for (int curOctave = 0; curOctave < this.octaveCount; curOctave++) {

			// Make sure that these doubleing-point values have the same range as a 32-
			// bit integer so that we can pass them to the coherent-noise functions.
			nx = makeInt32Range(x);
			// Get the coherent-noise value from the input value and add it to the
			// final result.
			seed = this.seed + curOctave & 0xffffffff;
			signal = gradientCoherentNoise2D(nx, seed, this.noiseQuality);
			value += signal * curPersistence;

			// Prepare the next octave.
			x *= this.lacunarity;
			curPersistence *= this.persistence;
		}
		return value * this.amplitude;
	}

	private double gradientCoherentNoise2D(double x, int seed, NoiseQuality noiseQuality) {
		// Create a unit-length cube aligned along an integer boundary. This cube
		// surrounds the input point.
		int x0 = x > 0.0 ? (int) x : (int) x - 1;
		int x1 = x0 + 1;

		// Map the difference between the coordinates of the input value and the
		// coordinates of the cube's outer-lower-left vertex onto an S-curve.
		double xs = 0;
		switch (noiseQuality) {
		case QUALITY_FAST:
			xs = x - x0;
			break;
		case QUALITY_STD:
			xs = sCurve3(x - x0);
			break;
		case QUALITY_BEST:
			xs = sCurve5(x - x0);
			break;
		default:
			throw new IllegalArgumentException("NoiseQuality: " + noiseQuality + " is not supported");
		}
		// Now calculate the noise values at each vertex of the cube. To generate
		// the coherent-noise value at the input point, interpolate these eight
		// noise values using the S-curve value as the interpolant (trilinear
		// interpolation.)
		double n0, n1;
		n0 = gradientNoise2D(x, x0, seed);
		n1 = gradientNoise2D(x, x1, seed);
		return linearInterp(n0, n1, xs);
	}

	double gradientNoise2D(double fx, int ix, int seed) {
		// Randomly generate a gradient vector given the integer coordinates of the
		// input value. This implementation generates a random number and uses it
		// as an index into a normalized-vector lookup table.
		int vectorIndex = X_NOISE_GEN * ix + SEED_NOISE_GEN * seed & 0xffffffff;
		vectorIndex ^= vectorIndex >> SHIFT_NOISE_GEN;
		vectorIndex &= 0xff;
		double xvGradient = this.gRandomVectors[vectorIndex << 2];
		// Set up us another vector equal to the distance between the two vectors
		// passed to this function.
		double xvPoint = fx - ix;
		// Now compute the dot product of the gradient vector with the distance
		// vector. The resulting value is gradient noise. Apply a scaling value
		// so that this noise value ranges from -1.0 to 1.0.
		return xvGradient * xvPoint * 2.12;
	}
}
