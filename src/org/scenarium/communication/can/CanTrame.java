/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can;

import java.io.Serializable;
import java.util.Arrays;

public class CanTrame implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int ADRESSE_MAX_CAN_EXT = 0b11111111111111111111111111111;
	public static final int ADRESSE_MAX_CAN_STD = 0b11111111111;
	public static final int CAN_A_ID_LENGHT = 2;
	public static final int CAN_B_ID_LENGHT = 4;
	public static final int CAN_DATA_MAX_LENGHT = 8;
	public static final int TRAME_MAX_SIZE = CAN_B_ID_LENGHT + CAN_DATA_MAX_LENGHT;
	private final byte[] trame;

	public static void main(String[] args) {
		CanTrame ct = new CanTrame(ADRESSE_MAX_CAN_EXT, false, new byte[] { 0x76, 0x76, (byte) 0xFF, 0x76, 0x62 });
		System.out.println(ct.getId() + " " + ct.isExtendedFrame());
	}

	public CanTrame(byte[] trame) {
		if (trame.length < CAN_A_ID_LENGHT)
			throw new IllegalArgumentException("Size of trame must be at least 11 bits (2 byte)");
		else if (trame.length > CAN_B_ID_LENGHT + CAN_DATA_MAX_LENGHT)
			throw new IllegalArgumentException("Size of trame must be at most 29 + 8*8 bits (29 + 64 byte)");
		this.trame = trame;
		if (!isExtendedFrame()) {
			if (getId() > ADRESSE_MAX_CAN_STD)
				throw new IllegalArgumentException("Id: " + getId() + " is greater than the maximum size for standard frame: " + ADRESSE_MAX_CAN_STD);
		} else if (getId() > ADRESSE_MAX_CAN_EXT)
			throw new IllegalArgumentException("Id: " + getId() + " is greater than the maximum size for extended frame: " + ADRESSE_MAX_CAN_EXT);
	}

	public CanTrame(int id, boolean extendedFrame, byte[] data) {
		if (id < 0 || id > (extendedFrame ? ADRESSE_MAX_CAN_EXT : ADRESSE_MAX_CAN_STD))
			throw new IllegalArgumentException("Id: " + id + " larger than " + (extendedFrame ? "29" : "11") + " bits specified in " + (extendedFrame ? "CAN2.0B" : "CAN2.0A"));
		if (data.length < 0 || data.length > CAN_DATA_MAX_LENGHT)
			throw new IllegalArgumentException("Size of data must be 64 bits (" + CAN_DATA_MAX_LENGHT + " bytes)");
		int index = 0;
		byte[] trame;
		if (extendedFrame) {
			trame = new byte[CAN_B_ID_LENGHT + data.length];
			trame[index++] = (byte) ((id >> 24 & 0x7F) + (extendedFrame ? 1 << 7 : 0));
			trame[index++] = (byte) (id >> 16 & 0xFF);
		} else
			trame = new byte[CAN_A_ID_LENGHT + data.length];
		trame[index++] = (byte) (id >> 8 & 0XFF);
		trame[index++] = (byte) (id & 0XFF);
		for (int i = 0; i < data.length; i++)
			trame[index++] = data[i];
		this.trame = trame;
	}

	@Override
	public CanTrame clone() {
		return new CanTrame(this.trame.clone());
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.trame);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CanTrame)
			return Arrays.equals(this.trame, ((CanTrame) obj).getTrame());
		return false;
	}

	public byte[] getData() {
		int idLength = isExtendedFrame() ? CAN_B_ID_LENGHT : CAN_A_ID_LENGHT;
		byte[] datas = new byte[this.trame.length - idLength];
		System.arraycopy(this.trame, idLength, datas, 0, datas.length);
		return datas;
	}

	public int getId() {
		return isExtendedFrame() ? (this.trame[3] & 0xFF) << 0 | (this.trame[2] & 0xFF) << 8 | (this.trame[1] & 0xFF) << 16 | (this.trame[0] & 0x7F) << 24
				: (this.trame[1] & 0xFF) << 0 | (this.trame[0] & 0x7F) << 8;
	}

	public byte[] getTrame() {
		return this.trame;
	}

	public boolean isExtendedFrame() {
		return (this.trame[0] & 0x80) != 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Id: ");
		int i = 0;
		sb.append(getId());
		sb.append("\tData: ");
		byte[] data = getData();
		for (; i < data.length; i++)
			sb.append(String.format("%02X", data[i]));
		return sb.toString();
	}
}
