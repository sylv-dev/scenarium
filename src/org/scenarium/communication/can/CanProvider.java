/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can;

public abstract class CanProvider {
	private boolean isRunning = false;
	protected CANReceiver receiver;

	public void close() {
		this.isRunning = false;
		release();
		this.receiver = null;
	}

	public abstract void connect() throws CanException;

	public abstract Object[] getAvailableChannel();

	public void init(CANReceiver canReceiver) throws CanException {
		this.receiver = canReceiver;
		this.isRunning = true;
		connect();
	}

	public abstract void release();;

	public void restart() {
		if (this.isRunning) {
			CANReceiver oldReceiver = this.receiver;
			close();
			try {
				init(oldReceiver);
			} catch (CanException e) {
				System.err.println(e.getMessage());
			}
		}
	}

	public abstract void sendTrame(CanTrame canTrame);
}
