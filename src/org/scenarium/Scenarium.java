/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import javax.vecmath.Point2i;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.tools.Logger;
import org.scenarium.display.AlertUtil;
import org.scenarium.display.LoadDrawerListener;
import org.scenarium.display.RenderFrame;
import org.scenarium.display.SplashScreen;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.LoadingException;
import org.scenarium.filemanager.scenario.DataFlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenariomanager.LoadScenarioListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.filemanager.scenariomanager.ScheduleChangeListener;
import org.scenarium.struct.ScenariumProperties;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.SchedulerInterface;
import org.scenarium.timescheduler.SchedulerPropertyChangeListener;
import org.scenarium.timescheduler.SchedulerState;
import org.scenarium.timescheduler.TimerScheduler;
import org.scenarium.updater.Updater;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Scenarium extends Application implements MainApp, SchedulerPropertyChangeListener, ScheduleChangeListener {
	// TODO Bug Linux Si Q sur diagramdrawer, je perd la barre des menu...
	// TODO Bug si j'ai un block CorCPP sans propriété je crash

	// TODO TICKET faire report + ticket pour LibJImageBug (que quand je coupe le soft en debug)
	// TODO TICKET faire ticket pour https://github.com/javafxports/openjdk-jfx/issues/578#event-2708100170
	// TODO TICKET mauvais fps sur linux

	// TODO EP1.2 Module
	// RMI avec module
	// créer un thread si pas de thread javafx

	// TODO EP1.4 Revoir les readonly, editable, inline avec par exemple le block ibeo
	// TODO EP1.5 Sous diagrammes

	// TODO EP2 Pouvoir changer le nom des entrées sur le recorder ainsi que le type de recorder (ex: Raw/avi, csv/binaire)
	// TODO EP2 Faire plusieurs sorties pour les scénario (video + son)
	// TODO EP2 Ajouter offset sur scenario evenement et begin sur autre
	// TODO EP2 changer serveur client avec enum et désactiver des propriétés si nécessaire
	// TODO EP2 Faire des listener pour toutes les propriété de mes blocks (viewer entre autres)
	// TODO EP2 gérer le trigger vectorizer/simulink avec règle logique en propriété
	// TODO EP2 method sur editor pour savoir si l'entrée est valide (edition du nom d'un block)
	// TODO EP2 min max sur Point2i pour crop par exemple
	// TODO EP2 ctrl espace ou espace sur player
	// TODO EP2 Afficher les icones des erreur et les cause dans les tooltip
	// TODO EP2 Mettre les annotations d'entrées de bloc sur les variables
	// TODO EP2 maj supr et supr sur clic droit block
	// TODO EP2 Mettre Icone barre des taches pour les tools
	// TODO EP2 Barre de chargement pour les scenario en dessous des blocs
	// TODO EP2 Inline sur un tableau ou sur un arbre de bean

	// TODO EP2 ValuesFx GeographicalDrawer roi
	// TODO EP2 Finir les viewer de courbes
	// TODO EP2 Que faire du streamScheduler? interaction avec scenario scenariumScheduler
	// TODO EP2 Ctrl+z sur les diagrammes
	// TODO EP2 Chargement directement des .psf sans diagramme ne charge que le premier
	// TODO EP2 Modif de log!!! -> enlever les System.lineSeparator() qui peuvent être problèmatique..., passer plutot par les putstring et getstring
	// ->enlever writeBytes de PropertyStreamRecorder et SerializableObjectStreamRecorder, remplacer par putstring getstring
	// TODO EP2 Moteur d'unité

	// TODO EP3 Couleur si raw uniquement -> Faire un format custom RGB, gérer le yuv dans le format raster
	// TODO EP3 Faire un error/warning viewer pour bilan temporelle
	// TODO EP3 Faire un CAN analyzer et finir de tester l'encoder CAN
	// TODO EP3 Rec manipulator time and data selection
	// TODO EP3 TimeStamp haute précision
	// TODO EP3 Transtypage double -> int et autre
	// TODO EP3 Ajouter plugin manager pour charger, décharger, voir un dépot internet
	// TODO EP3 Ajouter indicateur sur la vitesse réelle quand on rejoue un scénario en MONOCORE

	// TODO EP4 mettre le bon type de sortie/entré des canencoder/decoder, mais ca fait que j'ai des double, int, etc pas compatibles (pas temps que je gère pas les int -> double et autre)
	// TODO EP4 Prendre le menu de gluon: http://docs.gluonhq.com/charm/4.4.1/

	public static final String VERSION = "0.0.5.17";
	public static final boolean DEBUG_MODE = false;
	public static final boolean DEBUG_RMI = false;
	private static final int JAVA_MINIMUM_VERSION = 13;
	private static final String JAVA_DOWNLOAD_WEBPAGE = "https://bell-sw.com/";// "http://www.oracle.com/technetwork/java/javase/downloads";
	private static final javafx.scene.image.Image OPERATOR_INST_ICON = new javafx.scene.image.Image(Scenarium.class.getResourceAsStream("/Operator.gif"));
	public static boolean guiMode = true;
	public static boolean backgroundLoading = true;
	public static File defaultScenarioFile = null;
	public static Consumer<Scenarium> onStartup;
	public static AtomicBoolean isJavaFxThreadRunning = new AtomicBoolean(false);
	private static String localDir;
	public static Scenarium instance;
	private static NoGuiMainThread noGuiMainThread = null;

	private RenderFrame mainFrame;
	private DataLoader dataLoader;
	private Scheduler scheduler;
	private SplashScreen splashScreen;
	private long launchingTime;

	private static void checkVersionMode(String[] args) {
		if (args != null && args.length == 1 && args[0].equals("-version")) {
			System.out.println(VERSION);
			System.exit(0);
		}
	}

	public static String getLocalDir() {
		if (localDir == null) {
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			int i = 0;
			boolean isException;
			do
				try {
					new File(Class.forName(stack[i].getClassName()).getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
					isException = false;
				} catch (Exception e) {
					isException = true;
					i++;
				}
			while (isException);
			try {
				for (; i < stack.length; i++)
					localDir = new File(Class.forName(stack[i].getClassName()).getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
			} catch (Exception e) {}
			if (localDir == null)
				localDir = System.getProperty("user.dir");
		}
		return localDir;
	}

	public static void main(String[] args) {
		getLocalDir(); // problème windows
		checkVersionMode(args);
		Logger.setLevel(Logger.SAVE_OR_LOAD_FILE, true);
		Logger.setLevel(Logger.RMI, DEBUG_RMI);
		Logger.setLevel(Logger.BENCHMARK, false);
		Logger.setLevel(Logger.MODULE, false);
		Logger.setLevel(Logger.LIBRARYLOADING, false);
		launch(args);
	}

	private static void checkJavaVersion(HostServices hostServices) {
		if (Runtime.version().feature() < JAVA_MINIMUM_VERSION) {
			String message = "This program requires at least java version " + JAVA_MINIMUM_VERSION + ".\nYour version is: " + Runtime.version();
			if (guiMode) {
				Hyperlink hl = new Hyperlink(JAVA_DOWNLOAD_WEBPAGE);
				hl.setOnAction(e -> hostServices.showDocument(JAVA_DOWNLOAD_WEBPAGE));
				hl.setText("JDK download page");
				Alert alert = new Alert(AlertType.ERROR, message, ButtonType.FINISH);
				alert.getDialogPane().setContent(new VBox(new Label(message), hl));
				alert.showAndWait();
			} else
				System.err.println(message);
			System.exit(-1);
		}
	}

	public static void waitForJavaFXThreadStart() {
		if (!isJavaFxThreadRunning.getAndSet(true)) {
			CountDownLatch cdl = new CountDownLatch(1);
			Platform.startup(() -> cdl.countDown());
			Platform.setImplicitExit(false); // utile pour remote viewer
			try {
				cdl.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean addToSchedule(Scenario scenario) {
		int schedulerType = scenario.getSchedulerType();
		boolean needNewScheduler = this.scheduler == null || !this.scheduler.isOfKind(schedulerType);
		if (needNewScheduler) {
			Scheduler newScheduler = Scheduler.createScheduler(schedulerType, scenario.canReverse());
			boolean running = false;
			if (this.scheduler != null) {
				this.scheduler.death();
				this.scheduler.removePropertyChangeListener(this);
				newScheduler.copyProperty(this.scheduler);
				running = this.scheduler.isRunning();
			}
			this.scheduler = newScheduler;
			scenario.setScheduler(new SchedulerInterface(this.scheduler));
			if (this.scheduler != null) {
				File schedulerFile = new File(Scenarium.getLocalDir() + File.separator + "SchedulerConfig.txt");
				if (schedulerFile.exists())
					new BeanManager(this.scheduler, Scenarium.getLocalDir()).load(schedulerFile);
			}
			if (!this.scheduler.setTimePointer(this.scheduler.getSpeed() < 0 ? this.scheduler.getEffectiveStop() : this.scheduler.getEffectiveStart()))
				this.scheduler.refresh();
			this.scheduler.addPropertyChangeListener(this);
			if (this.scheduler instanceof TimerScheduler)
				((TimerScheduler) this.scheduler).addScheduleElement(scenario, -1);
			if (running)
				this.scheduler.start(); // Bug étrange... si true true, il ne démarre pas
			// scheduler.setRunning(running, true);
		} else {
			this.scheduler.clean();
			scenario.setScheduler(new SchedulerInterface(this.scheduler));
			if (this.scheduler instanceof TimerScheduler)
				((TimerScheduler) this.scheduler).addScheduleElement(scenario, -1);
			if (!this.scheduler.setTimePointer(this.scheduler.getSpeed() < 0 ? this.scheduler.getEffectiveStop() : this.scheduler.getEffectiveStart()))
				this.scheduler.refresh();
		}
		return needNewScheduler;
	}

	@Override
	public void addToScheduleAndShow(Scenario scenario) {
		boolean schedulerChanged = addToSchedule(scenario);
		scenario.addScheduleChangeListener(() -> {
			Scenario sce = this.dataLoader.getScenario();
			removeToSchedule(sce);
			if (addToSchedule(sce)) {
				TheaterPanel tp = this.mainFrame.getRenderPane().getTheaterPane();
				if (schedulerChanged)
					tp.setScheduler(this.scheduler);
			}
		});
		Runnable runnable = () -> {
			// System.out.println("startup time: " + (System.currentTimeMillis() - launchingTime));
			if (this.mainFrame == null) {
				if (this.splashScreen != null) {
					this.splashScreen.close();
					this.splashScreen = null;
				}
				if (guiMode) {
					this.mainFrame = new RenderFrame(new Stage(), Scenarium.this, this.dataLoader, true); // 370ms 150 ms minimum
					TheaterPanel tp = this.mainFrame.getRenderPane().getTheaterPane();
					tp.setScheduler(this.scheduler);
					scenario.setTheaterPanel(tp);
				}
			} else if (this.mainFrame != null) {
				this.mainFrame.reload(true);
				TheaterPanel tp = this.mainFrame.getRenderPane().getTheaterPane();
				if (schedulerChanged)
					tp.setScheduler(this.scheduler);
				scenario.setTheaterPanel(tp);
				tp.repaint(true);
			}
			updateRecentScenario();
			Logger.log(Logger.BENCHMARK, "startup time: " + (System.currentTimeMillis() - this.launchingTime));
		};
		if (!guiMode || Platform.isFxApplicationThread())
			runnable.run();
		else
			Platform.runLater(runnable);
	}

	public DataLoader getDataLoader() {
		return this.dataLoader;
	}

	public RenderFrame getMainFrame() {
		return this.mainFrame;
	}

	@Override
	public Point2i getMainFrameDimension() {
		if (this.mainFrame == null)
			return null;
		return this.mainFrame.getDimention();
	}

	@Override
	public Point2i getMainFramePosition() {
		if (this.mainFrame == null)
			return null;
		return this.mainFrame.getPosition();
	}

	public Scheduler getScheduler() {
		return this.scheduler;
	}

	@Override
	public ToolBarDescriptor[] getVisibleToolsDesc() {
		if (this.mainFrame == null)
			return null;
		return this.mainFrame.getVisibleToolsDesc().toArray(new ToolBarDescriptor[0]);
	}

	public boolean goToNextScenario(boolean reverse) {
		return this.dataLoader.goToNextScenario(reverse);
	}

	@Override
	public void loadingOperation(ReadOnlyDoubleProperty progressProperty) {
		if (this.mainFrame == null)
			return;
		this.mainFrame.getRenderPane().loadingOperation(progressProperty);
	}

	@Override
	public void removeToSchedule(Scenario scenario) {
		if (this.scheduler instanceof TimerScheduler)
			((TimerScheduler) this.scheduler).removeScheduleElement(scenario);
		scenario.removeScheduleChangeListener(this);
	}

	@Override
	public void scheduleChange() {
		Scenario scenario = this.dataLoader.getScenario();
		removeToSchedule(scenario);
		addToScheduleAndShow(scenario);
	}

	@Override
	public void start(Stage splashStage) throws Exception {
		checkJavaVersion(getHostServices());
		this.launchingTime = System.currentTimeMillis();
		ModuleManager.loadEmbeddedAndInternalModules(); // Charger les modules après loadSofwareConfig sinon j'ai pas les externs
		// System.out.println("startup time0: " + (System.currentTimeMillis() - launchingTime));
		BeanManager.switchDefaultBeanLocation(getLocalDir() + File.separator + "Operator" + File.separator);
		BeanEditor.setIconeProvider(obj -> OperatorManager.isOperator(obj) ? OPERATOR_INST_ICON : null);
		// System.out.println("startup time1: " + (System.currentTimeMillis() - launchingTime));
		isJavaFxThreadRunning.set(true);
		// System.setProperty("prism.lcdtext", "false");

		this.dataLoader = new DataLoader(this);
		ScenarioManager.addLoadScenarioListener(new LoadScenarioListener() {

			@Override
			public void unloaded(Class<? extends Scenario> scenarioClass) {
				if (Scenarium.this.dataLoader.getScenario().getClass().equals(scenarioClass))
					loadDefaultScenario(false);
			}

			@Override
			public void loaded(Class<? extends Scenario> drawerClass) {}
		});
		DrawerManager.addLoadDrawerListener(new LoadDrawerListener() {

			@Override
			public void unloaded(Class<? extends TheaterPanel> drawerClass) {
				if (Scenarium.this.mainFrame != null && Scenarium.this.mainFrame.getRenderPane().getClass().equals(drawerClass))
					loadDefaultScenario(false);
			}

			@Override
			public void loaded(Class<? extends TheaterPanel> drawerClass) {}
		});
		File scenarioFile;
		if (defaultScenarioFile == null) {
			Parameters parameters = getParameters();
			List<String> args = parameters != null ? parameters.getRaw() : null;
			scenarioFile = args != null && args.size() == 1 ? new File(args.get(0)) : null;
		} else
			scenarioFile = defaultScenarioFile;
		Object scenarioSource = null;
		try {
			scenarioSource = this.dataLoader.loadSofwareConfig();
		} catch (Exception e) {
			if (e instanceof IOException && guiMode)
				new Alert(AlertType.INFORMATION, "Enjoy Scenarium Software ;)").showAndWait();
			else
				e.printStackTrace();
		}
		Runtime.getRuntime().addShutdownHook(new Thread(() -> { // Après loadSofwareConfig sinon je peux sauvegarder un truc vierge
			if (this.dataLoader != null)
				this.dataLoader.saveSoftwareConfig();
		}));
		try {
			ModuleManager.loadExternalModules();
		} catch (IllegalArgumentException e) {
			if (guiMode)
				AlertUtil.show(e, false);
			else
				e.printStackTrace();
		}
		boolean loadDefaultScenario = scenarioFile == null && scenarioSource == null;
		if (!loadDefaultScenario)
			try {
				// System.out.println("startup time2: " + (System.currentTimeMillis() - launchingTime));
				this.dataLoader.secureLoad(scenarioFile != null ? scenarioFile : scenarioSource, false, backgroundLoading);
			} catch (LoadingException e) {
				String error = "Cannot load the scenario :\n" + (scenarioFile != null ? scenarioFile : scenarioSource);
				if (guiMode) {
					Alert alert = new Alert(AlertType.ERROR, error + "\n");
					// Size lenght bug code: 8087981, to remove after
					alert.getDialogPane().getChildren().stream().filter(node -> node instanceof Label).forEach(node -> ((Label) node).setMinHeight(Region.USE_PREF_SIZE));
					alert.showAndWait();
				} else
					System.err.println(error);
				loadDefaultScenario = true;
			}
		if (loadDefaultScenario)
			loadDefaultScenario(false);

		ReadOnlyDoubleProperty pp = this.dataLoader.getScenario().getProgressProperty();
		if (pp != null && guiMode)
			this.splashScreen = new SplashScreen(splashStage, pp);
		// if (scheduler == null)
		// scheduler = Scheduler.createScheduler(Scheduler.TIMERSCHEDULER, this, true);
		Updater updater = Updater.getInstance();
		updater.cleanFiles();
		if (ScenariumProperties.get().isCheckUpdatesAtStarup() && updater.isUpdatable()) {
			Task<Void> task = new Task<>() {
				@Override
				public Void call() {
					if (updater.isNewVersion())
						Platform.runLater(() -> {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("New version available");
							alert.setHeaderText("New version available");
							alert.setContentText("Update Scenarium to: " + updater.getLastVersion() + " ?");
							if (alert.showAndWait().get() == ButtonType.OK) {
								Stage dialog = new Stage(StageStyle.UTILITY);
								dialog.initModality(Modality.WINDOW_MODAL);
								dialog.initOwner(splashStage);
								dialog.setTitle("Downloading Scenarium version: " + updater.getLastVersion());
								ProgressBar pb = new ProgressBar();
								pb.setProgress(0);
								pb.setPrefSize(400, 30);
								Label text = new Label("0.00%");
								updater.addProgressListener(progress -> Platform.runLater(() -> {
									pb.setProgress(progress);
									text.setText(String.format("%.2f", progress * 100) + "%");
								}));
								dialog.setScene(new Scene(new StackPane(pb, text)));
								dialog.show();
								Task<Void> task = new Task<>() {
									@Override
									public Void call() {
										try {
											File newJarFile = updater.update();
											Platform.runLater(() -> {
												if (newJarFile != null) {
													dialog.close();
													Alert restartAlert = new Alert(AlertType.CONFIRMATION);
													restartAlert.setTitle("Scenarium needs to restart");
													restartAlert.setHeaderText("Scenarium needs to restart to complete the installation");
													restartAlert.setContentText("Restart to complete installation ?");
													if (restartAlert.showAndWait().get() == ButtonType.OK)
														updater.restart(newJarFile);
													newJarFile.delete();
													dialog.close();
												} else {
													dialog.close();
													Alert cancelAlert = new Alert(AlertType.INFORMATION);
													cancelAlert.setTitle("Update canceled");
													cancelAlert.setHeaderText("Update canceled");
													cancelAlert.setContentText("Update canceled");
													cancelAlert.showAndWait();
												}
											});
										} catch (Exception e) {
											Platform.runLater(() -> {
												Alert downloadAlert = new Alert(AlertType.ERROR);
												downloadAlert.setTitle("Error while downloading Scenarium update");
												downloadAlert.setHeaderText("Error while downloading Scenarium update");
												downloadAlert.setContentText("Error while downloading Scenarium update: " + e.getMessage());
												downloadAlert.showAndWait();
												e.printStackTrace();
												dialog.close();
											});
										}
										return null;
									}
								};
								Thread t1 = new Thread(task);
								t1.setPriority(Thread.MIN_PRIORITY);
								t1.start();
							}
						});
					return null;
				}
			};
			Thread t = new Thread(task);
			t.setPriority(Thread.MIN_PRIORITY);
			t.start();
		}
		instance = this;
		if (onStartup != null)
			onStartup.accept(this);
	}

	@Override
	public void stateChanged(SchedulerState state) {
		if (state == SchedulerState.ENDREACHED)
			new Thread(() -> {
				if (!goToNextScenario(this.scheduler.getSpeed() < 0))
					this.scheduler.pause();
				else
					this.scheduler.start();
			}).start();
	}

	@Override
	public void updateRecentScenario() {
		if (this.mainFrame != null)
			this.mainFrame.updateRecentScenario();
	}

	private void loadDefaultScenario(boolean backgroundLoading) {
		try {
			this.dataLoader.secureLoad(DataFlowDiagram.class, false, backgroundLoading);
		} catch (LoadingException e) {
			e.printStackTrace();
		}
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This is not a blocking method.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	public static void runTask(Runnable runnable) {
		if (guiMode && !Platform.isFxApplicationThread())
			try {
				Platform.runLater(() -> runnable.run());
			} catch (IllegalStateException e) {
				getNoGuiThread(runnable).runTask(runnable);
			}
		else
			getNoGuiThread(runnable).runTask(runnable);
	}

	/** Run the specified Runnable synchronously with other Scenarium tasks. This method blocks until the execution of the runnable.
	 * @param runnable the Runnable whose run method will be executed synchronously Scenarium. */
	public static void runTaskAndWait(Runnable runnable) {
		if (guiMode) {
			if (Platform.isFxApplicationThread())
				runnable.run();
			else
				try {
					CountDownLatch cdl = new CountDownLatch(1);
					Platform.runLater(() -> {
						runnable.run();
						cdl.countDown();
					});
					try {
						cdl.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (IllegalStateException e) {
					getNoGuiThread(runnable).runTaskAndWait(runnable);
				}
		} else
			getNoGuiThread(runnable).runTaskAndWait(runnable);
	}

	private static NoGuiMainThread getNoGuiThread(Runnable runnable) {
		if (noGuiMainThread == null) {
			noGuiMainThread = new NoGuiMainThread();
			noGuiMainThread.start();
		}
		return noGuiMainThread;
	}
}