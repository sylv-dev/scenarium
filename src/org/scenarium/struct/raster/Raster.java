/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct.raster;

public interface Raster {
	public static final int GRAY = 0;
	public static final int RGB = 1;
	public static final int BGR = 2;
	public static final int YUV = 3;

	public int getWidth();

	public int getHeight();

	public int getDepth();

	public int getType();

	public String getStringType();

	public int getNbChannel();

	public boolean isOnRaster(int x, int y);

	public boolean isOnRaster(int x, int y, int z);

	public Object getData();
}