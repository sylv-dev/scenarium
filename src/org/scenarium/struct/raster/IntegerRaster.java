/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct.raster;

import java.util.Arrays;

public class IntegerRaster implements Raster {
	public static final int ARGB8 = 4;
	public static final int ABGR8 = 5;

	private final int[] raster;
	private final int width, height, depth, widDep;

	private final int type;

	public IntegerRaster(int width, int height, int type) {
		this(width, height, type, null);
	}

	public IntegerRaster(int width, int height, int type, int[] pixels) {
		if (width < 0)
			throw new IllegalArgumentException("The width of the raster must be >= 0");
		if (height < 0)
			throw new IllegalArgumentException("The height of the raster must be >= 0");
		if (type < GRAY || type > ABGR8)
			throw new IllegalArgumentException("The type of the raster does not exists");
		this.width = width;
		this.height = height;
		this.depth = type == RGB || type == BGR || type == YUV ? 3 : 1;
		this.type = type;
		this.widDep = width * this.depth;
		if (pixels == null)
			this.raster = new int[width * height * this.depth];
		else {
			if (pixels.length != getSize())
				throw new IllegalArgumentException("The size of the pixel array" + pixels.length + " do not corresponds to the size of this kind of image: " + getSize());
			this.raster = pixels;
		}
	}

	public void set(int x, int y, int z, int value) {
		this.raster[y * this.widDep + x * this.depth + z] = value;
	}

	public void setSecure(int x, int y, int z, int value) {
		if (isOnRaster(x, y, z))
			this.raster[y * this.widDep + x * this.depth + z] = value;
	}

	public void clear() {
		Arrays.fill(this.raster, 0);
	}

	public void decrement(int x, int y, int z, int dec) {
		this.raster[y * this.widDep + x * this.depth + z] -= dec;
	}

	public int get(int x, int y, int z) {
		return this.raster[y * this.widDep + x * this.depth + z];
	}

	@Override
	public int[] getData() {
		return this.raster;
	}

	@Override
	public int getDepth() {
		return this.depth;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public int getType() {
		return this.type;
	}

	@Override
	public String getStringType() {
		if (this.type == GRAY)
			return "GRAY32";
		else if (this.type == RGB)
			return "RGB32";
		else if (this.type == BGR)
			return "BGR32";
		else if (this.type == YUV)
			return "YCBCR32";
		else if (this.type == ARGB8)
			return "ARGB8";
		else if (this.type == ABGR8)
			return "ABGR8";
		return "UNKNOW";
	}

	public int getIntValue(int x, int y, int z) {
		return this.raster[y * this.widDep + x * this.depth + z] & 0xFF;
	}

	public int getIntValueSecure(int x, int y, int z) {
		if (x < 0 || x >= this.width || y < 0 || y >= this.height)
			throw new IllegalArgumentException("out of bound");
		return this.raster[y * this.widDep + x * this.depth + z] & 0xFF;
	}

	public int getSize() {
		return this.widDep * this.height;
	}

	public void increment(int x, int y, int z, int inc) {
		this.raster[y * this.widDep + x * this.depth + z] += inc;
	}

	@Override
	public boolean isOnRaster(int x, int y) {
		return x >= 0 && x < this.width && y >= 0 && y < this.height;
	}

	@Override
	public boolean isOnRaster(int x, int y, int z) {
		return x >= 0 && x < this.width && y >= 0 && y < this.height && z >= 0 && z < this.depth;
	}

	public boolean isSameTypeAndSize(IntegerRaster r) {
		return r != null && this.width == r.getWidth() && this.height == r.getHeight() && this.type == r.getType();
	}

	@Override
	public IntegerRaster clone() {
		IntegerRaster r = new IntegerRaster(this.width, this.height, this.type);
		System.arraycopy(this.raster, 0, r.raster, 0, this.raster.length);
		return r;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + this.width + "*" + this.height + "*" + this.depth + "_Hash:" + hashCode();
	}

	@Override
	public int getNbChannel() {
		return this.type >= 4 ? 3 : getDepth();
	}
}
