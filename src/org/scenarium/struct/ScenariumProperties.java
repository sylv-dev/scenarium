/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.vecmath.Point2d;

import org.beanmanager.BeanManager;
import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.filemanager.scenariomanager.ScenarioDescriptor;

public class ScenariumProperties {
	private static ScenariumProperties scenariumProperties;
	private ScenarioDescriptor[] recentScenarios;
	private ScenarioDescriptor[] playList;
	private int playListIndex = 0;
	private boolean synchronization = false;
	private File scenarioChooserPath;
	private File recordChooserPath;
	private File playListChooserPath;
	private int videoFrequencie = 40;
	private String operator;
	private String lookAndFeel;
	private boolean askBeforeQuit = false;
	public boolean checkUpdatesAtStarup = true;
	private String language;
	private int[][] displayMode;
	private boolean hiddenFieldVisible = false;
	private boolean expertFieldVisible = false;
	private Point2d mainFramePosition;
	private Point2d mainFrameDimension;
	private boolean mainFrameAlwaysOnTop;
	private ToolBarDescriptor[] visibleToolsDesc;
	private boolean pageFlipping = false;
	private boolean showHiddenProperties = false;
	private boolean showExpertProperties = true;
	private String mapTempPath;
	private Path[] externModules;

	private ScenariumProperties() {
		this.mapTempPath = System.getProperty("java.io.tmpdir");
		if (!this.mapTempPath.endsWith(File.separator))
			this.mapTempPath += File.separator;
		this.mapTempPath += "mapCache" + File.separator;
	}

	public static ScenariumProperties get() {
		if (scenariumProperties == null)
			scenariumProperties = new ScenariumProperties();
		return scenariumProperties;
	}

	public int[][] getDisplayMode() {
		return this.displayMode;
	}

	public String getLanguage() {
		return this.language;
	}

	public String getLookAndFeel() {
		return this.lookAndFeel;
	}

	public Point2d getMainFrameDimension() {
		return this.mainFrameDimension;
	}

	public Point2d getMainFramePosition() {
		return this.mainFramePosition;
	}

	public String getMapTempPath() {
		return this.mapTempPath;
	}

	public String getOperator() {
		return this.operator;
	}

	public ScenarioDescriptor[] getPlayList() {
		return this.playList;
	}

	public File getPlayListChooserPath() {
		return this.playListChooserPath;
	}

	public int getPlayListIndex() {
		return this.playListIndex;
	}

	public ScenarioDescriptor[] getRecentScenarios() {
		return this.recentScenarios;
	}

	public File getRecordChooserPath() {
		return this.recordChooserPath;
	}

	public File getScenarioChooserPath() {
		return this.scenarioChooserPath;
	}

	public int getVideoFrequencie() {
		return this.videoFrequencie;
	}

	public ToolBarDescriptor[] getVisibleToolsDesc() {
		return this.visibleToolsDesc;
	}

	public boolean isAskBeforeQuit() {
		return this.askBeforeQuit;
	}

	public boolean isCheckUpdatesAtStarup() {
		return this.checkUpdatesAtStarup;
	}

	public boolean isExpertFieldVisible() {
		return this.expertFieldVisible;
	}

	public boolean isHiddenFieldVisible() {
		return this.hiddenFieldVisible;
	}

	public boolean isMainFrameAlwaysOnTop() {
		return this.mainFrameAlwaysOnTop;
	}

	public boolean isPageFlipping() {
		return this.pageFlipping;
	}

	public boolean isShowExpertProperties() {
		return this.showExpertProperties;
	}

	public boolean isShowHiddenProperties() {
		return this.showHiddenProperties;
	}

	public boolean isSynchronization() {
		return this.synchronization;
	}

	public void setAskBeforeQuit(boolean askBeforeQuit) {
		this.askBeforeQuit = askBeforeQuit;
	}

	public void setCheckUpdatesAtStarup(boolean checkUpdatesAtStarup) {
		this.checkUpdatesAtStarup = checkUpdatesAtStarup;
	}

	public void setDisplayMode(int[][] displayMode) {
		this.displayMode = displayMode;
	}

	public void setExpertFieldVisible(boolean expertFieldVisible) {
		this.expertFieldVisible = expertFieldVisible;
	}

	public void setHiddenFieldVisible(boolean hiddenFieldVisible) {
		this.hiddenFieldVisible = hiddenFieldVisible;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}

	public void setMainFrameAlwaysOnTop(boolean mainFrameAlwaysOnTop) {
		this.mainFrameAlwaysOnTop = mainFrameAlwaysOnTop;
	}

	public void setMainFrameDimension(Point2d mainFrameDimension) {
		this.mainFrameDimension = mainFrameDimension;
	}

	public void setMainFramePosition(Point2d mainFramePosition) {
		this.mainFramePosition = mainFramePosition;
	}

	public void setMapTempPath(String mapTempPath) {
		Path parentPath = Paths.get(mapTempPath).getParent();
		if (parentPath != null && Files.exists(parentPath)) {
			if (!mapTempPath.endsWith(File.separator))
				mapTempPath += File.separator;
			this.mapTempPath = mapTempPath;
		}
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setPageFlipping(boolean pageFlipping) {
		this.pageFlipping = pageFlipping;
	}

	public void setPlayList(ScenarioDescriptor[] playList) {
		this.playList = playList;
	}

	public void setPlayListChooserPath(File playListChooserPath) {
		this.playListChooserPath = playListChooserPath;
	}

	public void setPlayListIndex(int playListIndex) {
		this.playListIndex = playListIndex;
	}

	public void setRecentScenarios(ScenarioDescriptor[] recentScenarios) {
		this.recentScenarios = recentScenarios;
	}

	public void setRecordChooserPath(File recordChooserPath) {
		this.recordChooserPath = recordChooserPath;
	}

	public void setScenarioChooserPath(File scenarioChooserPath) {
		if (scenarioChooserPath != null && !scenarioChooserPath.isDirectory())
			scenarioChooserPath = scenarioChooserPath.getParentFile();
		this.scenarioChooserPath = scenarioChooserPath;
	}

	public void setShowExpertProperties(boolean showExpertProperties) {
		this.showExpertProperties = showExpertProperties;
		BeanManager.isExpertFieldVisible = showExpertProperties;
	}

	public void setShowHiddenProperties(boolean showHiddenProperties) {
		this.showHiddenProperties = showHiddenProperties;
		BeanManager.isHiddenFieldVisible = showHiddenProperties;
	}

	public void setSynchronization(boolean synchronization) {
		this.synchronization = synchronization;
	}

	public void setVideoFrequencie(int videoFrequencie) {
		this.videoFrequencie = videoFrequencie;
	}

	public void setVisibleToolsDesc(ToolBarDescriptor[] visibleToolsDesc) {
		this.visibleToolsDesc = visibleToolsDesc;
	}

	public Path[] getExternModules() {
		return this.externModules;
	}

	public void setExternModules(Path[] externModules) {
		this.externModules = externModules;
	}
}
