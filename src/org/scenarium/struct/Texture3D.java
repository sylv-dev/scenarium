/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import javax.vecmath.Color3f;

import org.scenarium.filemanager.datastream.input.BufferedImageInputStream;
import org.scenarium.filemanager.datastream.output.BufferedImageOutputStream;

public class Texture3D extends OldObject3D implements Externalizable {
	private static final long serialVersionUID = 1L;
	public BufferedImage img;

	public Texture3D(BufferedImage img, float[] texCoord) {
		super(OldObject3D.TEXTURE, texCoord, new Color3f(1, 1, 1), 3);
		if (this.data == null || this.data.length != 8)
			throw new IllegalArgumentException("texCoord must be non null and with a size of 8");
		if (img == null)
			throw new IllegalArgumentException("img must be non null");
		this.img = img;
	}

	@Override
	public Texture3D clone() {
		BufferedImage bi = this.img;
		ColorModel cm = bi.getColorModel();
		return new Texture3D(new BufferedImage(cm, bi.copyData(null), cm.isAlphaPremultiplied(), null), this.data);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		BufferedImageInputStream biis = new BufferedImageInputStream();
		biis.setDataInput(in);
		this.img = biis.pop();
	}

	@Override
	public String toString() {
		return "ElementType: Texture" + " Data size: " + this.data.length;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		BufferedImageOutputStream bios = new BufferedImageOutputStream();
		bios.setDataOutput(out);
		bios.push(this.img);

	}
}
