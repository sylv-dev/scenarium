/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.awt.GLCanvas;

public class Camera implements MouseWheelListener, KeyListener, MouseListener, MouseMotionListener {
	private final GLCanvas canvas;
	private double distance;
	private double angleX;
	private double angleY;
	private double x;
	private double y;
	private final Point anchorPoint = new Point();
	private boolean translate;
	private boolean rotate;

	public Camera(GLCanvas canvas, double distance, double x, double y, double angleX, double angleY, double angleZ) {
		this.canvas = canvas;
		canvas.addMouseWheelListener(this);
		canvas.addKeyListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addMouseListener(this);
		this.distance = distance;
		this.x = x;
		this.y = y;
		this.angleX = angleX;
		this.angleY = angleY;
	}

	public double getAngleX() {
		return this.angleX;
	}

	public double getAngleY() {
		return this.angleY;
	}

	public double getDistance() {
		return this.distance;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		rotation(e.getKeyCode());
		this.canvas.display();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (this.translate)
			translate((this.anchorPoint.x - e.getX()) / (float) this.canvas.getWidth() * this.distance, (-this.anchorPoint.y + e.getY()) / (float) this.canvas.getHeight() * this.distance);
		else if (this.rotate)
			rotate(this.anchorPoint.x - e.getX(), -this.anchorPoint.y + e.getY());
		this.canvas.display();
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			this.anchorPoint.setLocation(e.getX() + this.x * this.canvas.getWidth() / this.distance, e.getY() - this.y * this.canvas.getHeight() / this.distance);
			this.translate = true;
			this.rotate = false;
		} else if (SwingUtilities.isLeftMouseButton(e)) {
			this.anchorPoint.setLocation(e.getX() - this.angleY, e.getY() - this.angleX);
			this.rotate = true;
			this.translate = false;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.translate = false;
		this.rotate = false;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		zoom(e.getWheelRotation() > 0);
		this.canvas.display();
	}

	public void rotate(int i, int j) {
		this.angleY = -i;
		this.angleX = j;
	}

	public void rotation(int keyCode) {
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
			this.angleY += 5;
			break;
		case KeyEvent.VK_RIGHT:
			this.angleY -= 5;
			break;
		case KeyEvent.VK_UP:
			this.angleX += 5;
			break;
		case KeyEvent.VK_DOWN:
			this.angleX -= 5;
			break;
		default:
			throw new IllegalArgumentException("KeyCode: " + keyCode + " is not supported");
		}
	}

	public void setAngleX(double angleX) {
		this.angleX = angleX;
	}

	public void setAngleY(double angleY) {
		this.angleY = angleY;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void translate(double dx, double dy) {
		this.x = dx;
		this.y = dy;
	}

	public void zoom(boolean pos) {
		if (pos)
			this.distance += this.distance / 10.f;
		else
			this.distance -= this.distance / 10.f;
	}
}
