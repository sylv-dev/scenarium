/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.lang.reflect.Array;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.ArrayInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.ColorProvider;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.drawer.imagedrawers.ByteImageDrawer;
import org.scenarium.display.drawer.imagedrawers.DoubleImageDrawer;
import org.scenarium.display.drawer.imagedrawers.FloatImageDrawer;
import org.scenarium.display.drawer.imagedrawers.IntegerImageDrawer;
import org.scenarium.display.drawer.imagedrawers.ShortImageDrawer;
import org.scenarium.operator.image.ImageType;
import org.scenarium.operator.image.conversion.TypeConverter;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.raster.BufferedImageStrategy;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.IntegerRaster;
import org.scenarium.struct.raster.Raster;
import org.scenarium.struct.raster.RasterStrategy;
import org.scenarium.timescheduler.Scheduler;

import javafx.beans.InvalidationListener;
import javafx.geometry.Dimension2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;

//TODO Tjs un problème sur affichage du text sombre ou pas
public class ImageDrawer extends GeometricDrawer implements StackableDrawer {
	public static final int GRAY = 0;
	public static final int RGB = 1;
	public static final int BGR = 2;
	public static final int YUV = 3;
	public static final int ARGB = 4;
	public static final int ABGR = 5;
	public static final int INT_RGB = 6;
	public static final int INT_BGR = 7;
	private static final HashMap<Class<?>, ImageDrawerInfo> IMAGE_DRAWERS = new HashMap<>();
	private static final String RASTER_FILTERS = "Raster";
	private static final String RASTER = "Raster";
	private static final String VALUE = "Value";
	private static final int[] GRAY_COLOR_MAP;
	static {
		GRAY_COLOR_MAP = new int[256];
		for (int i = 0; i < GRAY_COLOR_MAP.length; i++)
			GRAY_COLOR_MAP[i] = 0xff000000 | i << 16 | i << 8 | i;
	}
	// Element to draw image
	private PixelBuffer<IntBuffer> pixelBuffer;
	private ImageView imageView;
	private Rectangle2D imageRegion;

	// Filter of Theater
	private boolean filterRaster = true;
	private boolean filterValue = true;
	private final ArrayList<Point2i> selectedCells = new ArrayList<>();
	private final int[] viewWindow = new int[2 * 2];

	protected int[] getViewWindow() {
		return this.viewWindow.clone();
	}

	// Level of details
	protected int lodGrid = 5;
	protected int lodRasterValue1 = 15;
	protected int lodRasterValue1l = 20;
	protected int lodRasterValue1s = 30;
	protected int lodRasterValue3 = 25;
	protected int lodRasterValue3l = 35;
	protected int lodRasterValue3s = 70;
	// Image of Theater
	public boolean computeRaster = true;
	private boolean isRasterDrawn;
	private ColorProvider colorProvider = new ColorProvider();
	@PropertyInfo(index = 0, info = "Line width for each point of the point cloud")
	@NumberInfo(min = 1)
	private double pointCloudLineWidth = 1;
	@PropertyInfo(index = 1, info = "Line size for each point of the point cloud")
	@NumberInfo(min = 0)
	private double pointCloudLineSize = 5;
	@PropertyInfo(index = 2, info = "Normalize the drawn raster")
	private boolean normalize;

	protected boolean needToRefreshGc;
	private Number[] range;
	private boolean selectAreaRectDrawn = false;

	static {
		IMAGE_DRAWERS.put(byte[].class, new ImageDrawerInfo(ByteImageDrawer::drawFullImage, ByteImageDrawer::drawImage, ByteImageDrawer::getRGBValue, ByteImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(short[].class, new ImageDrawerInfo(ShortImageDrawer::drawFullImage, ShortImageDrawer::drawImage, ShortImageDrawer::getRGBValue, ShortImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(int[].class, new ImageDrawerInfo(IntegerImageDrawer::drawFullImage, IntegerImageDrawer::drawImage, IntegerImageDrawer::getRGBValue, IntegerImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(float[].class, new ImageDrawerInfo(FloatImageDrawer::drawFullImage, FloatImageDrawer::drawImage, FloatImageDrawer::getRGBValue, FloatImageDrawer::getMinMax));
		IMAGE_DRAWERS.put(double[].class, new ImageDrawerInfo(DoubleImageDrawer::drawFullImage, DoubleImageDrawer::drawImage, DoubleImageDrawer::getRGBValue, DoubleImageDrawer::getMinMax));
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		getAdditionalDrawableElement();
		this.imageView = new ImageView();
		InvalidationListener il = b -> {
			int width = (int) getWidth();
			int height = (int) getHeight();
			if (width <= 0 || height <= 0)
				return;
			this.pixelBuffer = new PixelBuffer<>(width, height, IntBuffer.wrap(new int[width * height]), PixelFormat.getIntArgbPreInstance());
			this.imageRegion = new Rectangle2D(0, 0, width, height);
			this.imageView.setImage(new WritableImage(this.pixelBuffer));
			paintImmediately(false);
		};
		il.invalidated(null);
		widthProperty().addListener(il);
		heightProperty().addListener(il);
		this.imageView.fitWidthProperty().bind(widthProperty());
		this.imageView.fitHeightProperty().bind(heightProperty());
		getChildren().add(0, this.imageView);
	}

	@Override
	public void scale1AndReplace() {
		super.scale1AndReplace();
		updateTransform(1, 0, 0);
	}

	@Override
	public Dimension2D getDimension() {
		Object de = getDrawableElement();
		if (de instanceof RasterStrategy)
			return ((RasterStrategy) getDrawableElement()).getDimension();
		else if (de instanceof Raster)
			return new Dimension2D(((Raster) de).getWidth(), ((Raster) de).getHeight());
		else if (de instanceof BufferedImageStrategy)
			return ((BufferedImageStrategy) de).getDimension();
		else
			return new Dimension2D(((BufferedImage) de).getWidth(), ((BufferedImage) de).getHeight());
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		super.setDrawableElement(drawableElement);
		this.range = null;
	}

	@Override
	protected void paint(Object de) {
		Object dataElement = de instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) de).getDrawElement() : de;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			int imgType = image.getType();
			if (imgType == BufferedImage.TYPE_BYTE_BINARY || imgType == BufferedImage.TYPE_BYTE_INDEXED || imgType == BufferedImage.TYPE_USHORT_555_RGB || imgType == BufferedImage.TYPE_USHORT_565_RGB
					|| imgType == BufferedImage.TYPE_CUSTOM) {
				paint(new TypeConverter(imgType == BufferedImage.TYPE_BYTE_BINARY ? ImageType.TYPE_BYTE_GRAY : ImageType.TYPE_INT_ARGB).process(image));
				return;
			}
		}

		int imageWidth, imageHeight, imageType;
		Object imageData;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			DataBuffer db = image.getRaster().getDataBuffer();
			imageData = db instanceof DataBufferByte ? ((DataBufferByte) db).getData()
					: db instanceof DataBufferShort ? ((DataBufferShort) db).getData() : db instanceof DataBufferUShort ? ((DataBufferUShort) db).getData() : ((DataBufferInt) db).getData();
			switch (image.getType()) {
			case BufferedImage.TYPE_BYTE_GRAY:
			case BufferedImage.TYPE_USHORT_GRAY:
				imageType = GRAY;
				break;
			case BufferedImage.TYPE_3BYTE_BGR:
				imageType = BGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR_PRE:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_INT_ARGB:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_ARGB_PRE:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_BGR:
				imageType = INT_BGR;
				break;
			case BufferedImage.TYPE_INT_RGB:
				imageType = INT_RGB;
				break;
			default:
				imageType = 0;
			}
		} else {
			Raster image = (Raster) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			imageData = image.getData();
			switch (image.getType()) {
			case Raster.GRAY:
				imageType = GRAY;
				break;
			case Raster.BGR:
				imageType = BGR;
				break;
			case Raster.RGB:
				imageType = RGB;
				break;
			case Raster.YUV:
				imageType = YUV;
				break;
			case IntegerRaster.ARGB8:
				imageType = ARGB;
				break;
			case IntegerRaster.ABGR8:
				imageType = ABGR;
				break;
			default:
				imageType = 0;
			}
		}
		ImageDrawerInfo idi = IMAGE_DRAWERS.get(imageData.getClass());
		if (this.range == null && this.normalize && Array.getLength(imageData) != 0)
			this.range = idi.getMinMaxProvider().getMinMax(imageData, imageType);
		if (this.pixelBuffer != null && (this.filterRaster || this.isRasterDrawn))
			this.pixelBuffer.updateBuffer(pb -> {
				double scale = getScale();
				double tx = getxTranslate();
				double ty = getyTranslate();
				int viewerHeight = (int) getHeight();
				int viewerWidth = (int) getWidth();
				// occurs when the size of the node changed but the resize event is not yet occurred. The next resize event while refresh the view.
				if (!isValidTransform(viewerWidth, viewerHeight, imageWidth, imageHeight, scale, tx, ty))
					return this.imageRegion;
				int[] viewerData = pb.getBuffer().array();
				if (!this.filterRaster) {
					Arrays.fill(viewerData, 0);
					this.isRasterDrawn = false;
					return this.imageRegion;
				}
				if (imageWidth == 0 || imageHeight == 0 || viewerHeight == 0 || viewerWidth == 0)
					Arrays.fill(viewerData, 0);
				else {
					if (scale == 1 && imageWidth == viewerWidth && imageHeight == viewerHeight && tx == 0 && ty == 0)
						idi.getFirst().drawImage(imageData, imageType, viewerData, this.range);
					else
						try {
							double invScale = 1 / scale;
							double xTranslate = -tx * invScale;
							double yTranslate = -ty * invScale;
							int pos = viewerWidth - 1 + (viewerHeight - 1) * viewerWidth;
							int nbToSkipX = 0;
							double boundX = viewerWidth * invScale + xTranslate;
							if (boundX > imageWidth)
								nbToSkipX = (int) Math.round((boundX - imageWidth) / invScale);
							int width = viewerWidth - nbToSkipX;
							int endY = 0;
							double boundY = viewerHeight * invScale + yTranslate;
							if (boundY > imageHeight)
								endY = (int) ((boundY - imageHeight) * scale);
							int startX = (int) Math.ceil(-xTranslate * scale);
							int startY = (int) Math.ceil(-yTranslate * scale);
							if (startY > 0)
								Arrays.fill(viewerData, 0, startY * viewerWidth, 0);
							int height = viewerHeight;
							if (endY != 0) {
								height -= endY;
								pos -= endY * viewerWidth;
								Arrays.fill(viewerData, pos, pos + endY * viewerWidth, 0);
							}
							double startXOnRaster = (width - 1) * invScale + xTranslate;
							double startYOnRaster = (height - 1) * invScale + yTranslate;
							idi.getSecond().drawImage(imageData, imageType, imageWidth, imageHeight, viewerData, Math.max(0, startX), Math.max(0, startY), width, height, startXOnRaster,
									startYOnRaster, invScale, viewerWidth, this.range);
						} catch (ArrayIndexOutOfBoundsException e) {
							System.err.println(
									"An error occured while drawing image.\n\tImage: " + imageWidth + "x" + imageHeight + "(" + Array.getLength(imageData) + ")" + " t: " + imageType + "\n\tViewer: "
											+ viewerWidth + "x" + viewerHeight + "(" + viewerData.length + ")" + "\n\tTransformation: scale: " + scale + " translation: (" + tx + "," + ty + ")");
							e.printStackTrace();
						}
					this.isRasterDrawn = true;
				}
				drawAdditionalElements(dataElement, imageData, imageWidth, imageHeight, imageType, idi);
				return this.imageRegion;
			});
		else
			drawAdditionalElements(dataElement, imageData, imageWidth, imageHeight, imageType, idi);
	}

	private void drawAdditionalElements(Object dataElement, Object imageData, int imageWidth, int imageHeight, int imageType, ImageDrawerInfo idi) {
		double scale = getScale();
		boolean viewWindowsComputed = false;
		// Draw values
		GraphicsContext g = this.gc;
		Object[] adds = getAdditionalDrawableElement();
		boolean needToRefresh = this.needToRefreshGc || adds != null && adds.length != 0 || this.filterROI || this.selectAreaRectDrawn && this.selectAreaRect == null;
		this.needToRefreshGc = false;
		boolean clearRectDone = false;
		ImageValueProvider ivp = idi.getImageValueProvider();
		Class<?> elementType = imageType == ARGB || imageType == ABGR || imageType == INT_BGR || imageType == INT_RGB ? byte.class : imageData.getClass().getComponentType();
		int nbChannel = dataElement instanceof BufferedImage
				? ((BufferedImage) dataElement).getType() == BufferedImage.TYPE_BYTE_GRAY || ((BufferedImage) dataElement).getType() == BufferedImage.TYPE_USHORT_GRAY ? 1 : 3
				: ((Raster) dataElement).getNbChannel();
		int lod = getLevelOfDetails(elementType, nbChannel);
		if (this.filterValue && scale > lod) {
			g.setFont(new Font(getFontScaleFactor(elementType, nbChannel) * scale));
			g.setFill(Color.BLACK);
			this.needToRefreshGc = true;
			if (!clearRectDone) {
				clearScreen(g);
				clearRectDone = true;
			}
			if (!viewWindowsComputed) {
				computeViewWindow();
				viewWindowsComputed = true;
			}
			int xTranslate = (int) getxTranslate();
			int yTranslate = (int) getyTranslate();
			Affine oldTransform = g.getTransform();
			g.setTransform(1, 0, 0, 1, 0, 0);
			g.setTextAlign(TextAlignment.CENTER);
			g.setTextBaseline(VPos.CENTER);
			double min;
			double max;
			double deltaRange;
			if (this.range != null) {
				min = this.range[0].doubleValue();
				max = this.range[1].doubleValue();
				deltaRange = min == max ? 1 : max - min;
			} else if (elementType == int.class) {
				min = 0;
				max = Integer.MAX_VALUE * 2L + 1L;
				deltaRange = max - min;
			} else if (elementType == short.class) {
				min = 0;
				max = Short.MAX_VALUE * 2 + 1;
				deltaRange = max - min;
			} else if (elementType == byte.class) {
				min = 0;
				max = Byte.MAX_VALUE * 2 + 1;
				deltaRange = max - min;
			} else {
				min = 0;
				max = 1;
				deltaRange = 1;
			}
			for (int i = this.viewWindow[0]; i <= this.viewWindow[2]; i++)
				for (int j = this.viewWindow[1]; j <= this.viewWindow[3]; j++)
					if (nbChannel == 1) {
						double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
						g.setFill((value - min) / deltaRange > 0.5 ? Color.BLACK : Color.WHITE);
						g.fillText(formatFloatValue(elementType, value, false), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + 0.5) + yTranslate));
					} else if (nbChannel == 3) {
						float gap = 1.0f / (nbChannel + 1);
						if (imageType != YUV) {
							double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
							g.setFill((value - min) / deltaRange > 0.5 ? new Color(0.5, 0, 0, 1) : Color.RED);
							g.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
							value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).doubleValue();
							g.setFill((value - min) / deltaRange > 0.5 ? new Color(0, 0.5, 0, 1) : Color.LIME);
							g.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
							value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).doubleValue();
							g.setFill((value - min) / deltaRange > 0.5 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
							g.fillText(formatFloatValue(elementType, value, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
						} else {
							double y = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
							g.setFill((y - min) / deltaRange > 0.5 ? Color.BLACK : Color.WHITE);
							g.fillText(formatFloatValue(elementType, y, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
							double u = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).doubleValue();
							g.fillText(formatFloatValue(elementType, u, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
							double v = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).doubleValue();
							g.fillText(formatFloatValue(elementType, v, true), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
						}
					}
			g.setTransform(oldTransform);
			needToRefresh = true;
		}

		if (!clearRectDone && needToRefresh)
			clearScreen(g);
		if (needToRefresh) {
			double eps = 0.5 / scale;
			if (this.filterGrid && scale > this.lodGrid) {
				if (!viewWindowsComputed)
					computeViewWindow();
				g.setStroke(Color.BLACK);
				g.setLineWidth(1 / getScale());
				int beginX = this.viewWindow[0];
				int endX = this.viewWindow[2] + 1;
				int beginY = this.viewWindow[1];
				int endY = this.viewWindow[3] + 1;
				int end = endY;
				for (int i = beginX; i <= endX; i++)
					g.strokeLine(i + eps, beginY + eps, i + eps, end + eps);
				end = endX;
				for (int i = beginY; i <= endY; i++)
					g.strokeLine(beginX + eps, i + eps, end + eps, i + eps);
			} else if (this.filterBorder) {
				g.setStroke(Color.BLACK);
				g.setLineWidth(1 / getScale());
				g.strokeRect(0, 0, this.sWid, this.sHei);
			}
			if (this.filterROI) {
				g.setStroke(Color.RED);
				g.strokeRect(this.roi[0] + eps, this.roi[1] + eps, this.roi[2] + eps, this.roi[3] + eps);
			}
			if (adds != null) {
				this.colorProvider.resetIndex();
				g.setLineWidth(this.pointCloudLineWidth / getScale());
				double lineSize = this.pointCloudLineSize;
				for (Object add : adds)
					if (add instanceof Point2d[]) {
						g.setStroke(this.colorProvider.getNextColor());
						for (Point2d point : (Point2d[]) add) {
							double u = point.x;
							double v = point.y;
							g.strokeLine(u + lineSize, v, u - lineSize, v);
							g.strokeLine(u, v - lineSize, u, v + lineSize);
						}
					}
			}
			if (this.selectAreaRect != null) {
				this.selectAreaRectDrawn = true;
				g.setStroke(Color.LIME);
				g.setLineWidth(1 / getScale());
				int x = this.selectAreaRect[0].x;
				int y = this.selectAreaRect[0].y;
				int width = this.selectAreaRect[1].x + 1;
				int height = this.selectAreaRect[1].y + 1;
				g.strokeRect(x + eps, y + eps, width, height);
			} else
				this.selectAreaRectDrawn = false;
		}
	}

	private static int getLevelOfDetails(Class<?> type, int nbChannel) {
		if (type == double.class || type == float.class || type == int.class)
			return nbChannel == 1 ? 20 : 40;
		if (type == short.class)
			return 25;
		return nbChannel == 1 ? 15 : 25;
	}

	private static double getFontScaleFactor(Class<?> type, int nbChannel) {
		if (type == double.class || type == float.class || type == int.class)
			return nbChannel == 1 ? 0.3 : 0.16;
		if (type == short.class)
			return 0.25;
		return nbChannel == 1 ? 0.4 : 0.25;
	}

	private static String formatFloatValue(Class<?> type, double value, boolean oneLine) {
		String text;
		if (type == double.class || type == float.class || type == int.class) {
			if (Double.isFinite(value)) {
				text = String.format("%.3e", value);
				if (!oneLine) {
					int eIndex = text.indexOf("e");
					if (eIndex != -1)
						text = text.substring(0, eIndex) + System.lineSeparator() + text.substring(eIndex);
				}
			} else if (value == Double.POSITIVE_INFINITY)
				text = Character.valueOf((char) 8734).toString();
			else if (value == Double.NEGATIVE_INFINITY)
				text = "-" + Character.valueOf((char) 8734).toString();
			else
				text = Double.toString(value);
		} else
			text = Long.toString((long) value);
		return text;
	}

	protected void clearScreen(GraphicsContext g) {
		Affine oldTransform = g.getTransform();
		g.setTransform(1, 0, 0, 1, 0, 0);
		g.clearRect(0, 0, getWidth(), getHeight());
		g.setTransform(oldTransform);
	}

	private void computeViewWindow() {
		double xTranslate = -getxTranslate();
		double yTranslate = -getyTranslate();
		double scale = getScale();
		int value = (int) (xTranslate / scale);
		this.viewWindow[0] = value >= 0 ? value : 0;
		value = (int) (yTranslate / scale);
		this.viewWindow[1] = value >= 0 ? value : 0;
		value = (int) ((xTranslate + getWidth() - 1) / scale);
		this.viewWindow[2] = value < this.sWid ? value : this.sWid - 1;
		value = (int) ((yTranslate + getHeight() - 1) / scale);
		this.viewWindow[3] = value < this.sHei ? value : this.sHei - 1;
	}

	// public static <T> void addCloner(Class<T> _class, Object src, Object val) {
	// imageDrawers.get(_class).getFirst().drawImage(src, 1, null);
	// }

	protected int getLodRasterValue() {
		Object dataElement = getDrawableElement();
		if (dataElement instanceof BufferedStrategy<?>)
			dataElement = ((BufferedStrategy<?>) dataElement).getDrawElement();
		return (dataElement instanceof ByteRaster ? ((ByteRaster) dataElement).getDepth() : ((BufferedImage) dataElement).getColorModel().getPixelSize() / 8) == 1 ? this.lodRasterValue1
				: this.lodRasterValue3;
	}

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();
		TreeNode<BooleanProperty> rasNode = new TreeNode<>(new BooleanProperty(RASTER_FILTERS, true));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(RASTER, true)));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(VALUE, true)));
		this.theaterFilter.addChild(rasNode);
	}

	@Override
	protected double adjustScale(double scale) {
		return scale > this.lodGrid ? Math.round(scale) : scale;
	}

	@Override
	protected void updateTransform(double scale, double tx, double ty) {
		if (scale != getScale() || tx != getxTranslate() || ty != getyTranslate())
			this.needToRefreshGc = true;
		super.updateTransform(scale, tx, ty);
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return this.lodGrid;
	}

	@Override
	public float getZoomScale() {
		return getLodRasterValue() + 1;
	}

	@Override
	protected boolean isAreaSelectionPoint(int x, int y) {
		return true;
	}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		if (this.selectArea && !isCtrlDown)
			this.selectedCells.clear();
		this.needToRefreshGc = true;
	}

	public double getPointCloudLineWidth() {
		return this.pointCloudLineWidth;
	}

	public void setPointCloudLineWidth(double pointCloudLineWidth) {
		this.pointCloudLineWidth = pointCloudLineWidth;
		repaint(false);
	}

	public double getPointCloudLineSize() {
		return this.pointCloudLineSize;
	}

	public void setPointCloudLineSize(double pointCloudLineSize) {
		this.pointCloudLineSize = pointCloudLineSize;
		repaint(false);
	}

	public boolean isNormalize() {
		return this.normalize;
	}

	public void setNormalize(boolean normalize) {
		this.normalize = normalize;
		if (!normalize)
			this.range = null;
		repaint(false);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 2, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getPointCloudsColor() {
		return this.colorProvider.getColors();
	}

	public void setPointCloudsColor(Color[] pointCloudColor) {
		this.colorProvider = new ColorProvider(pointCloudColor);
		repaint(false);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		this.needToRefreshGc = true;
		if (filterPath[filterPath.length - 1].equals(RASTER))
			this.filterRaster = value;
		else if (filterPath[filterPath.length - 1].equals(VALUE))
			this.filterValue = value;
		else
			return super.updateFilterWithPath(filterPath, value);
		repaint(false);
		return true;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false
				: Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput) || Point2d[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}
}

@FunctionalInterface
interface NominalDrawer {
	public void drawImage(Object src, int srcType, int[] dest, Number[] range);
}

@FunctionalInterface
interface Drawer {
	public void drawImage(Object src, int srcType, int srcWid, int srcHei, int[] dest, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale,
			int frameWidht, Number[] range);
}

@FunctionalInterface
interface ImageValueProvider {
	public Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z);
}

@FunctionalInterface
interface MinMaxProvider {
	public Number[] getMinMax(Object imageData, int imageType);
}

class ImageDrawerInfo {
	private final NominalDrawer fullImageDrawer;
	private final Drawer drawer;
	private final ImageValueProvider imageValueProvider;
	private final MinMaxProvider minMaxProvider;

	public ImageDrawerInfo(NominalDrawer first, Drawer second, ImageValueProvider imageValueProvider, MinMaxProvider minMaxProvider) {
		this.fullImageDrawer = first;
		this.drawer = second;
		this.imageValueProvider = imageValueProvider;
		this.minMaxProvider = minMaxProvider;
	}

	public NominalDrawer getFirst() {
		return this.fullImageDrawer;
	}

	public Drawer getSecond() {
		return this.drawer;
	}

	public ImageValueProvider getImageValueProvider() {
		return this.imageValueProvider;
	}

	public MinMaxProvider getMinMaxProvider() {
		return this.minMaxProvider;
	}
}
