/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.GeographicalDrawableObject2D;
import org.scenarium.struct.GeographicalDrawableObject3D;

public abstract class GeographicalMarker implements GeographicalDrawableObject2D, GeographicalDrawableObject3D {
	public final GeographicCoordinate coordinate;

	public GeographicalMarker(GeographicCoordinate coordinate) {
		if (coordinate == null)
			throw new IllegalArgumentException("The coordinate of the " + GeographicalMarker.class + " cannot be null");
		this.coordinate = coordinate;
	}
}
