/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class MapQuestOSMTileSource implements TileSource {
	private static final int NB_SERVERS = 4;
	private int servCpt = 1;

	@Override
	public int getMaxZoom() {
		return 19;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "MQOSM";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		this.servCpt = this.servCpt % NB_SERVERS + 1;
		return "http://otile" + Integer.toString(this.servCpt) + ".mqcdn.com/tiles/1.0.0/osm/" + zoom + "/" + tilex + "/" + tiley + ".png";
	}
}
