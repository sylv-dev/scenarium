/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class MapQuestOSMAerialTileSource implements TileSource {
	private static final int NB_SERVERS = 4;
	private int servCpt = 1;

	@Override
	public int getMaxZoom() {
		return 19;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "MQOSMA";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		this.servCpt = this.servCpt % NB_SERVERS + 1;
		return "http://oatile" + Integer.toString(this.servCpt) + ".mqcdn.com/tiles/1.0.0/sat/" + zoom + "/" + tilex + "/" + tiley + ".png";
	}
}
