/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import javax.vecmath.Point2i;

import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.RenderFrame;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.curve.Curvei;
import org.scenarium.timescheduler.Scheduler;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;

/** Panel du teatre d'operation */
public abstract class GeometricDrawer extends TheaterPanel {
	protected static final String GENERALFILTERS = "General";
	protected static final String BORDER = "Border";
	protected static final String GRID = "Grid";
	// filters of Theater
	public boolean filterROI = false;
	protected boolean filterBorder = true;
	protected boolean filterGrid = true;
	// Selected Element
	protected Point2i[] selectAreaRect;
	protected Point2i mousTruePos = new Point2i();
	// Selection
	private final Point2i selectAreaOrigine = new Point2i();
	protected boolean selectArea = false;

	protected boolean dragg = false;
	private Point2D mousDraggAnchorPoint;
	protected boolean zoomOnMouse = true;
	protected GraphicsContext gc;

	public GeometricDrawer() {}

	protected void adaptViewToScenario() {
		fitDocument();
	}

	protected abstract double adjustScale(double scale);

	protected void clearScreen(GraphicsContext g, Color background) {
		Affine oldTransform = g.getTransform();
		g.setTransform(1, 0, 0, 1, 0, 0);
		g.setFill(background);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setTransform(oldTransform);
	}

	protected void drawCurve(Curve curve, GraphicsContext g) {
		double scale = getScale();
		if (curve instanceof Curvei) {
			int[][] data = ((Curvei) curve).getData();
			if (data[0].length == 0)
				return;
			int oldX = data[0][0];
			int oldY = data[1][0];
			for (int i = 1; i < data[0].length; i++) {
				int x = data[0][i];
				int y = data[1][i];
				g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
				oldX = x;
				oldY = y;
			}
		} else {
			double[][] data = ((Curved) curve).getData();
			if (data[0].length == 0)
				return;
			double oldX = data[0][0];
			double oldY = data[1][0];
			for (int i = 1; i < data[0].length; i++) {
				double x = data[0][i];
				double y = data[1][i];
				g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
				oldX = x;
				oldY = y;
			}
		}
	}

	protected void drawCurve(Curve curve, GraphicsContext g, Color c1, Color c2, double scale) {
		if (curve instanceof Curvei) {
			Curvei curvei = (Curvei) curve;
			int[][] data = curvei.getData();
			int[] weights = curvei.getValues();
			if (weights == null) {
				g.setFill(c1);
				drawCurve(curvei, g);
				return;
			}
			if (data[0].length == 0)
				return;
			int oldX = data[0][0];
			int oldY = data[1][0];
			int maxValue = weights[0];
			int minValue = weights[0];
			for (int x = 0; x < weights.length; x++) {
				if (weights[x] > maxValue)
					maxValue = weights[x];
				if (weights[x] < minValue)
					minValue = weights[x];
			}
			double ratio = maxValue - minValue;
			if (ratio == 0) {
				g.setFill(c1);
				for (int i = 1; i < data[0].length; i++) {
					int x = data[0][i];
					int y = data[1][i];
					g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
					oldX = x;
					oldY = y;
				}
			}
			for (int i = 1; i < data[0].length; i++) {
				int x = data[0][i];
				int y = data[1][i];
				float note = (float) ((weights[i] - minValue) / ratio);
				if (note < 0)
					note = 0;
				if (note > 1)
					note = 1;
				float invNote = 1 - note;
				double red = note * c1.getRed() + invNote * c2.getRed();
				if (red > 1)
					red = 1;
				else if (red < 0)
					red = 0;
				double green = note * c1.getGreen() + invNote * c2.getGreen();
				if (green > 1)
					green = 1;
				else if (green < 0)
					green = 0;
				double blue = note * c1.getBlue() + invNote * c2.getBlue();
				if (blue > 1)
					blue = 1;
				else if (blue < 0)
					blue = 0;
				g.setFill(new Color(red, green, blue, 1));
				g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
				oldX = x;
				oldY = y;
			}

		} else {
			Curved curved = (Curved) curve;
			double[][] data = curved.getData();
			double[] weights = curved.getValues();
			if (weights == null) {
				g.setFill(c1);
				drawCurve(curved, g);
				return;
			}
			if (data[0].length == 0)
				return;
			double oldX = data[0][0];
			double oldY = data[1][0];
			double maxValue = weights[0];
			double minValue = weights[0];
			for (int x = 0; x < weights.length; x++) {
				if (weights[x] > maxValue)
					maxValue = weights[x];
				if (weights[x] < minValue)
					minValue = weights[x];
			}
			double ratio = maxValue - minValue;
			if (ratio == 0) {
				g.setFill(c1);
				for (int i = 1; i < data[0].length; i++) {
					double x = data[0][i];
					double y = data[1][i];
					g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
					oldX = x;
					oldY = y;
				}
			}
			for (int i = 1; i < data[0].length; i++) {
				double x = data[0][i];
				double y = data[1][i];
				float note = (float) ((weights[i] - minValue) / ratio);
				if (note < 0)
					note = 0;
				if (note > 1)
					note = 1;
				float invNote = 1 - note;
				double red = note * c1.getRed() + invNote * c2.getRed();
				if (red > 1)
					red = 1;
				else if (red < 0)
					red = 0;
				double green = note * c1.getGreen() + invNote * c2.getGreen();
				if (green > 1)
					green = 1;
				else if (green < 0)
					green = 0;
				double blue = note * c1.getBlue() + invNote * c2.getBlue();
				if (blue > 1)
					blue = 1;
				else if (blue < 0)
					blue = 0;
				g.setFill(new Color(red, green, blue, 1));
				g.strokeLine((int) (oldX * scale), (int) (oldY * scale), (int) (x * scale), (int) (y * scale));
				oldX = x;
				oldY = y;
			}
		}
	}

	@Override
	public void fitDocument() {
		double scale = Math.min(getWidth() / this.sWid, getHeight() / this.sHei);
		if (scale == 0)
			return;
		scale = adjustScale(scale);
		updateTransform(scale, -(int) ((scale * this.sWid - getWidth()) / 2.f), -(int) ((scale * this.sHei - getHeight()) / 2.f));
	}

	public abstract float getIntelligentZoomThreshold();

	public float[] getRoi() {
		return this.roi;
	}

	public double getScale() {
		if (this.gc == null)
			return -1;
		return this.gc.getTransform().getMxx();
	}

	@Override
	public String[] getStatusBarInfo() {
		double scale = getScale();
		return new String[] { "u: " + (int) Math.floor((this.mousTruePos.x - getxTranslate()) / scale), "v: " + (int) Math.floor((this.mousTruePos.y - getyTranslate()) / scale) };
	}

	public double getxTranslate() {
		if (this.gc == null)
			return -1;
		return this.gc.getTransform().getTx();
	}

	public double getyTranslate() {
		if (this.gc == null)
			return -1;
		return this.gc.getTransform().getTy();
	}

	public abstract float getZoomScale();

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		Canvas canvas = new Canvas();
		getChildren().add(canvas);
		canvas.widthProperty().bind(widthProperty());
		canvas.heightProperty().bind(heightProperty());
		this.gc = canvas.getGraphicsContext2D();
		setOnScroll(e -> {
			if (e.isControlDown() && e.getDeltaY() != 0)
				zoom(e.getDeltaY() < 0, e.getX(), e.getY());
		});
		addEventFilter(MouseEvent.MOUSE_MOVED, e -> { // Filter sinon mousTruePos après getStatusBarInfo
			this.mousTruePos.x = (int) e.getX();
			this.mousTruePos.y = (int) e.getY();
		});
		addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
			this.mousDraggAnchorPoint = null;
			this.selectArea = false;
			if (this.dragg) { // Previously in MOUSE_CLICKED but when dragging and releasing outside of the windows, the dragg does not stop.
				this.dragg = false;
				setCursor(Cursor.DEFAULT);
				e.consume();
			}
		});
		addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
			MouseButton button = e.getButton();
			if (e.isConsumed() && button == MouseButton.PRIMARY) {
				this.selectArea = false;
				this.selectAreaRect = null;
				return;
			}
			if (button == MouseButton.PRIMARY && this.filterROI) {
				updateROI();
				e.consume();
			} else if (button == MouseButton.SECONDARY) {
				Affine transform = this.gc.getTransform();
				this.mousDraggAnchorPoint = new Point2D(e.getX() - transform.getTx(), e.getY() - transform.getTy());
			} else if (button == MouseButton.PRIMARY) {
				this.selectArea = false;
				this.selectAreaRect = null;
				if (isAreaSelectionPoint((int) e.getX(), (int) e.getY())) {
					this.selectArea = true;
					this.selectAreaOrigine.set((int) Math.floor((e.getX() - getxTranslate()) / getScale()), (int) Math.floor((e.getY() - getyTranslate()) / getScale()));
					e.consume();
				}
				repaint(false);
			}
		});

		addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
			MouseButton button = e.getButton();
			if (button == MouseButton.PRIMARY && this.filterROI) {
				updateROI();
				e.consume();
			} else if (button == MouseButton.PRIMARY && this.selectArea) {
				if (this.selectAreaRect == null)
					this.selectAreaRect = new Point2i[] { new Point2i(), new Point2i() };
				int x = (int) Math.floor((e.getX() - getxTranslate()) / getScale());
				int y = (int) Math.floor((e.getY() - getyTranslate()) / getScale());
				if (x - this.selectAreaOrigine.x < 0) {
					this.selectAreaRect[0].x = x;
					this.selectAreaRect[1].x = this.selectAreaOrigine.x - x;
				} else {
					this.selectAreaRect[0].x = this.selectAreaOrigine.x;
					this.selectAreaRect[1].x = x - this.selectAreaOrigine.x;
				}
				if (y - this.selectAreaOrigine.y < 0) {
					this.selectAreaRect[0].y = y;
					this.selectAreaRect[1].y = this.selectAreaOrigine.y - y;
				} else {
					this.selectAreaRect[0].y = this.selectAreaOrigine.y;
					this.selectAreaRect[1].y = y - this.selectAreaOrigine.y;
				}
				selectAreaChanged(e.isControlDown());
				e.consume();
				repaint(false);
			} else if (button == MouseButton.SECONDARY || this.mousDraggAnchorPoint != null) {
				if (this.mousDraggAnchorPoint != null) {
					if (!this.dragg)
						setCursor(Cursor.MOVE);
					updateTransform(this.gc.getTransform().getMxx(), e.getX() - this.mousDraggAnchorPoint.getX(), e.getY() - this.mousDraggAnchorPoint.getY());
					e.consume();
				}
				this.dragg = true;
			}
			this.mousTruePos.x = (int) e.getX();
			this.mousTruePos.y = (int) e.getY();
		});
		// addEventFilter(MouseEvent.MOUSE_CLICKED, e -> { // addEventHandler avant, mais changer pour add input/output diagram IO
		// if (dragg) {
		// dragg = false;
		// setCursor(Cursor.DEFAULT);
		// e.consume();
		// }
		// });
		addEventHandler(KeyEvent.KEY_PRESSED, e -> {
			if (e.isConsumed())
				return;
			boolean zoomIn = true;
			KeyCode keyCode = e.getCode();
			switch (keyCode) {
			case A:
				adaptViewToScenario();
				e.consume();
				break;
			case Z:
				scale1AndReplace();
				e.consume();
				break;
			case Q:
				scale1AndReplace();
				container.adaptSizeToDrawableElement();
				// sizeToScene();
				e.consume();
				break;
			case UP:
				zoomIn = false;
			case DOWN:
				if (e.isShiftDown()) {
					zoom(zoomIn);
					e.consume();
					break;
				}
			case RIGHT:
			case LEFT:
				if (e.isControlDown()) {
					double tx = getxTranslate();
					double ty = getyTranslate();
					tx += keyCode == KeyCode.RIGHT ? -1 : keyCode == KeyCode.LEFT ? 1 : 0;
					ty += keyCode == KeyCode.DOWN ? -1 : keyCode == KeyCode.UP ? 1 : 0;
					updateTransform(getScale(), tx, ty);
					e.consume();
				} else if (keyCode == KeyCode.RIGHT)
					if (container instanceof RenderFrame)
						new Thread(() -> ((RenderFrame) container).getRenderPane().getDataLoader().goToNextScenario(false)).start();
				break;
			default:
				return;
			}
		});
	}

	protected abstract boolean isAreaSelectionPoint(int x, int y);

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();
		TreeNode<BooleanProperty> filtersMap = new TreeNode<>(new BooleanProperty(GENERALFILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BORDER, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GRID, true)));
		this.theaterFilter.addChild(filtersMap);
	}

	public void scale1AndReplace() {
		updateTransform(1, 0, 0);
	}

	protected abstract void selectAreaChanged(boolean isCtrlDown);

	public void setRoi(float x, float y, float width, float height) {
		this.roi[0] = x;
		this.roi[1] = y;
		this.roi[2] = width;
		this.roi[3] = height;
		repaint(false);
	}

	public void setTransfom(double tx, double ty, double scale) {
		updateTransform(scale, tx, ty);
	}

	protected Point2D toGeometricCoordinate(double x, double y) {
		Affine t = this.gc.getTransform();
		double is = 1 / t.getMxx();
		return new Point2D((x - t.getTx()) * is, (y - t.getTy()) * is);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (filterPath[filterPath.length - 1].equals(GRID))
			this.filterGrid = value;
		else if (filterPath[filterPath.length - 1].equals(BORDER))
			this.filterBorder = value;
		else
			return false;
		repaint(false);
		return true;
	}

	private void updateROI() {
		double scale = getScale();
		this.roi[0] = (float) ((this.mousTruePos.x - getxTranslate()) / scale - this.roi[2] / 2.0f);
		this.roi[1] = (float) ((this.mousTruePos.y - getyTranslate()) / scale - this.roi[3] / 2.0f);
		repaint(false);
	}

	protected void updateTransform(double scale, double tx, double ty) {
		if (scale == getScale() && tx == getxTranslate() && ty == getyTranslate())
			return;
		double halfWid = getWidth() / 2.f;
		double halfHei = getHeight() / 2.f;
		if (tx > halfWid)
			tx = halfWid;
		else if (tx < halfWid - scale * this.sWid)
			tx = halfWid - scale * this.sWid;
		if (ty > halfHei)
			ty = halfHei;
		else if (ty < halfHei - scale * this.sHei)
			ty = halfHei - scale * this.sHei;
		this.gc.setTransform(scale, 0, 0, scale, (int) tx, (int) ty);
		repaint(false);
	}

	protected static boolean isValidTransform(double viewerWidth, double viewerHeight, double scenarioWidth, double scenarioHeight, double scale, double tx, double ty) {
		double halfWid = viewerWidth / 2.f;
		double halfHei = viewerHeight / 2.f;
		if (tx > halfWid)
			return false;
		else if (tx < halfWid - scale * scenarioWidth)
			return false;
		if (ty > halfHei)
			return false;
		else if (ty < halfHei - scale * scenarioHeight)
			return false;
		return true;
	}

	private void zoom(boolean in) {
		zoom(in, getWidth() / 2, getHeight() / 2);
	}

	protected void zoom(boolean in, double x, double y) {
		Affine transform = this.gc.getTransform();
		double previousScale = transform.getMxx();
		double scalew = getWidth() / this.sWid;
		double scaleh = getHeight() / this.sHei;
		double minScale = (scalew < scaleh ? scalew : scaleh) / 2.f;
		double scale = previousScale;
		if (in)
			scale -= scale / 10;
		else
			scale += scale / 10;
		if (scale < minScale)
			scale = minScale;
		else if (scale > 100)
			scale = 100;
		scale = adjustScale(scale);
		double ratio = (scale - previousScale) / previousScale;
		updateTransform(scale, transform.getTx() * (1 + ratio) - ratio * (this.zoomOnMouse ? x : getWidth() / 2), transform.getTy() * (1 + ratio) - ratio * (this.zoomOnMouse ? y : getHeight() / 2));
		if (this.mousDraggAnchorPoint != null) {
			Affine newTransform = this.gc.getTransform(); // TODO vérifier que je vais pas drag après...
			this.mousDraggAnchorPoint = new Point2D(this.mousDraggAnchorPoint.getX() + transform.getTx() - newTransform.getTx(),
					this.mousDraggAnchorPoint.getY() + transform.getTy() - newTransform.getTy());
		}
	}
}
