/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.imagedrawers;

import java.util.Arrays;

import org.scenarium.display.drawer.ImageDrawer;

public class ByteImageDrawer {

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData, Number[] range) {
		byte[] src = (byte[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int i = 0; i < src.length; i++) {
					byte gray = src[i];
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i++) {
					byte gray = (byte) (((src[i] & 0xFF) - min) / deltaRange);
					viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((src[i] & 0xFF) << 16) + ((src[i + 1] & 0xFF) << 8) + (src[i + 2] & 0xFF);
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i] & 0xFF) - min) / deltaRange) & 0xFF) << 16) + (((byte) (((src[i + 1] & 0xFF) - min) / deltaRange) & 0xFF) << 8)
							+ ((byte) (((src[i + 2] & 0xFF) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + ((src[i + 2] & 0xFF) << 16) + ((src[i + 1] & 0xFF) << 8) + (src[i] & 0xFF);
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i + 2] & 0xFF) - min) / deltaRange) & 0xFF) << 16) + (((byte) (((src[i + 1] & 0xFF) - min) / deltaRange) & 0xFF) << 8)
							+ ((byte) (((src[i] & 0xFF) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.ABGR:
			if (range == null)
				for (int i = 0; i < src.length; i += 4)
					viewerData[offset++] = (255 << 24) + ((src[i + 3] & 0xFF) << 16) + ((src[i + 2] & 0xFF) << 8) + (src[i + 1] & 0xFF);
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 4)
					viewerData[offset++] = (255 << 24) + (((byte) (((src[i + 3] & 0xFF) - min) / deltaRange) & 0xFF) << 16) + (((byte) (((src[i + 2] & 0xFF) - min) / deltaRange) & 0xFF) << 8)
							+ ((byte) (((src[i + 1] & 0xFF) - min) / deltaRange) & 0xFF);
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int i = 0; i < src.length; i += 3) {
					int yi = src[i] & 0xFF;
					int ui = src[i + 1] & 0xFF;
					int vi = src[i + 2] & 0xFF;
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int i = 0; i < src.length; i += 3) {
					float yi = (src[i] - min) / deltaRange;
					float ui = (src[i + 1] - min) / deltaRange;
					float vi = (src[i + 2] - min) / deltaRange;
					viewerData[offset++] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster,
			double startYOnRaster, double invScale, int viewerWidth, Number[] range) {
		byte[] src = (byte[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = src[yIndex + (int) xOnRaster];
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							byte gray = (byte) (((src[yIndex + (int) xOnRaster] & 0xFF) - min) / deltaRange);
							viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + gray;
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.RGB:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((src[index] & 0xFF) << 16) + ((src[index + 1] & 0xFF) << 8) + (src[index + 2] & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index] & 0xFF) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index + 2] & 0xFF) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.BGR:
			if (range == null)
				try {
					for (int y = height; y-- != startY;) {
						int yFIndex = y * viewerWidth;
						int yOnRaster = (int) startYOnRaster;
						if (startX > 0)
							Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
						if (yOnRaster < imageHeight) {
							int yIndex = yOnRaster * imageWidth * 3;
							double xOnRaster = startXOnRaster;
							for (int x = width; x-- != startX;) {
								int index = yIndex + 3 * (int) xOnRaster;
								viewerData[yFIndex + x] = (255 << 24) + ((src[index + 2] & 0xFF) << 16) + ((src[index + 1] & 0xFF) << 8) + (src[index] & 0xFF);
								xOnRaster -= invScale;
							}
						}
						startYOnRaster -= invScale;
						if (viewerWidth != width)
							Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					// e.printStackTrace();
					throw e;
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index + 2] & 0xFF) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 1] & 0xFF) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index] & 0xFF) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.ABGR:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 4;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 4 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + ((src[index + 3] & 0xFF) << 16) + ((src[index + 2] & 0xFF) << 8) + (src[index + 1] & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 4;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 4 * (int) xOnRaster;
							viewerData[yFIndex + x] = (255 << 24) + (((byte) (((src[index + 3] & 0xFF) - min) / deltaRange) & 0xFF) << 16)
									+ (((byte) (((src[index + 2] & 0xFF) - min) / deltaRange) & 0xFF) << 8) + ((byte) (((src[index + 1] & 0xFF) - min) / deltaRange) & 0xFF);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		case ImageDrawer.YUV:
			if (range == null)
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							float yi = (src[index] & 0xFF) / 255.0f;
							float ui = (src[index + 1] & 0xFF) / 255.0f;
							float vi = (src[index + 2] & 0xFF) / 255.0f;
							float r = yi + 1.13983f * vi;
							float g = yi - 0.39465f * ui - 0.58060f * vi;
							float b = yi + 2.03211f * ui;
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (r * 255) << 16) + ((byte) (g * 255) << 8) + (byte) (b * 255);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			else {
				int min = (Integer) range[0];
				int max = (Integer) range[1];
				float deltaRange = (min == max ? 1 : max - min) / 255.0f;
				for (int y = height; y-- != startY;) {
					int yFIndex = y * viewerWidth;
					int yOnRaster = (int) startYOnRaster;
					if (startX > 0)
						Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
					if (yOnRaster < imageHeight) {
						int yIndex = yOnRaster * imageWidth * 3;
						double xOnRaster = startXOnRaster;
						for (int x = width; x-- != startX;) {
							int index = yIndex + 3 * (int) xOnRaster;
							float yi = (src[index] - min) / deltaRange;
							float ui = (src[index + 1] - min) / deltaRange;
							float vi = (src[index + 2] - min) / deltaRange;
							viewerData[yFIndex + x] = (255 << 24) + ((byte) (yi + 1.13983f * vi) << 16) + ((byte) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((byte) (yi + 2.03211f * ui) << 16);
							xOnRaster -= invScale;
						}
					}
					startYOnRaster -= invScale;
					if (viewerWidth != width)
						Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
				}
			}
			break;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		byte[] src = (byte[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x] & 0xFF;
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF;
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + 2 - z] & 0xFF;
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + 2 - z] & 0xFF;
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF;
		default:
			throw new IllegalArgumentException("ImageType: " + imageType + " is not supported");
		}
	}

	public static Number[] getMinMax(Object imageData, int imageType) {
		byte[] src = (byte[]) imageData;
		int min = src[0] & 0xFF;
		int max = src[0] & 0xFF;
		for (byte v : src) {
			int val = v & 0xFF;
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
		}
		return new Number[] { min, max };
	}
}
