/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class LanguageManager {
	private static final String ENGLISH = "English";
	private static final String FRENCH = "French";
	private static final String GERMAN = "German";
	private static final String ITALIAN = "Italian";
	public static final ArrayList<String> AVAILABLELANGUAGE = new ArrayList<>(Arrays.asList(ENGLISH, FRENCH, GERMAN, ITALIAN));
	private static final HashMap<String, String> DICTIONNARY = new HashMap<>();

	private LanguageManager() {}

	public static String getText(String text) {
		String lText = DICTIONNARY.get(text);
		return lText != null ? lText : text;
	}

	public static void loadDefaultLanguage() {
		String language = System.getProperty("user.language");
		String languageType = null;
		if (language.equals("fr"))
			languageType = FRENCH;
		else if (language.equals("de"))
			languageType = GERMAN;
		else if (language.equals("it"))
			languageType = ITALIAN;
		else
			languageType = ENGLISH;
		loadLanguage(languageType);
	}

	public static boolean loadLanguage(String languageType) {
		DICTIONNARY.clear();
		DICTIONNARY.put("language", languageType);
		if (!languageType.equals(ENGLISH))
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(LanguageManager.class.getResourceAsStream("/" + languageType + ".txt"), StandardCharsets.UTF_8));
				String line;
				while ((line = br.readLine()) != null) {
					int i = line.indexOf(":");
					DICTIONNARY.put(line.substring(0, i), line.substring(i + 1, line.length()));
				}
			} catch (IOException e) {
				return false;
			}
		return true;
	}
}
