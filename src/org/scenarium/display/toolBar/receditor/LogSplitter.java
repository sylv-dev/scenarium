/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar.receditor;

import java.io.File;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.Scenarium;
import org.scenarium.display.toolbarclass.ExternalTool;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class LogSplitter extends ExternalTool {
	@PropertyInfo(index = 0, info = "List of split timestamp")
	private long[] splitTs;
	@PropertyInfo(index = 1, info = "Output directory of the merged log")
	private File outputDirectory;

	private final File configFile = new File(Scenarium.getLocalDir() + File.separator + getClass().getSimpleName() + ".txt");

	@Override
	public Region getRegion() {
		BeanManager bm = new BeanManager(this, getClass().getSimpleName());
		bm.load(this.configFile);
		Button launchButton = new Button("Merge");
		launchButton.setOnAction(e -> {
			bm.save(this.configFile, true);

		});
		VBox propertiesBox = new VBox(10, bm.getEditor(), launchButton);
		propertiesBox.setPadding(new Insets(10));
		propertiesBox.setAlignment(Pos.CENTER);
		return propertiesBox;
	}

	public long[] getSplitTs() {
		return this.splitTs;
	}

	public void setSplitTs(long[] splitTs) {
		this.splitTs = splitTs;
	}
}
