/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import org.beanmanager.tools.FxUtils;
import org.scenarium.display.toolbarclass.Border;
import org.scenarium.display.toolbarclass.InternalTool;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

public class StatusBar extends InternalTool {
	private long messageTime = 0;
	private boolean remove;
	private int oldInfoSize = -1;
	private ToolBar tb;

	@Override
	public Border getBorder() {
		return Border.BOTTOM;
	}

	@Override
	public Region getRegion() {
		this.tb = new ToolBar();
		this.tb.setStyle("-fx-spacing: 0;");
		this.tb.setPadding(new Insets(0, 2, 0, 2));
		this.renderPane.getTheaterPane().getStatusBarInfo();
		return this.tb;
	}

	public void update(ReadOnlyDoubleProperty progressProperty, boolean removeAfter) {
		FxUtils.runLaterIfNeeded(() -> {
			this.tb.getItems().clear();
			ProgressBar progressBar = new ProgressBar();
			progressBar.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			HBox.setHgrow(progressBar, Priority.ALWAYS);
			this.tb.getItems().add(progressBar);
			DoubleProperty pp = progressBar.progressProperty();
			pp.bind(progressProperty);
			this.oldInfoSize = -1;
		});
	}

	public void update(String... infos) {
		ObservableList<Node> items = this.tb.getItems();
		if (items.size() == 1 && items.get(0) instanceof ProgressBar)
			return;
		if (this.messageTime != 0)
			if (System.currentTimeMillis() - this.messageTime > 3000) {
				if (this.remove)
					this.renderPane.closeTool(StatusBar.class);
				this.messageTime = 0;
			}
		if (this.messageTime == 0) {
			if (this.oldInfoSize != infos.length) {
				items.clear();
				double compWidth = this.tb.getWidth() / infos.length - 4;
				for (int i = 0; i < infos.length; i++) {
					Label l = new Label(infos[i]);
					l.setPrefWidth(compWidth);
					l.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
					HBox.setHgrow(l, Priority.ALWAYS);
					items.add(l);
					if (i != infos.length - 1) {
						Separator s = new Separator();
						s.setMinWidth(2);
						s.setPrefWidth(2);
						items.add(s);
					}
				}
			} else
				for (int i = 0; i < infos.length; i++)
					((Label) items.get(i * 2)).setText(infos[i] == null ? " " : infos[i]);
			this.oldInfoSize = infos.length;
		}
	}

	public void update(String message, boolean error, boolean removeAfter) {
		ObservableList<Node> items = this.tb.getItems();
		if (this.oldInfoSize != 1) {
			items.clear();
			Label labelMessage = new Label(message);
			labelMessage.setAlignment(Pos.CENTER);
			items.add(labelMessage);
		}
		this.remove = removeAfter;
		Label label = (Label) items.get(0);
		label.setText(message);
		if (error)
			label.setTextFill(Color.RED);
		this.messageTime = System.currentTimeMillis();
		this.oldInfoSize = 1;
	}
}
