/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import static org.scenarium.display.LanguageManager.getText;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.scenarium.display.AlertUtil;
import org.scenarium.display.ScenarioInformation;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.OpenListener;
import org.scenarium.filemanager.playlist.PlayListChangeListener;
import org.scenarium.filemanager.playlist.PlayListFileChooserFx;
import org.scenarium.filemanager.playlist.PlayListManager;
import org.scenarium.filemanager.playlist.ScenarioDesc;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioFileChooserFx;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.test.fx.FxTest;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class PlayList extends ExternalTool implements PlayListChangeListener, OpenListener {
	public static void main(String[] args) {
		new FxTest().launchIHM(args, () -> {
			PlayList cfx = new PlayList();
			return cfx.getRegion();
		});
	}

	private ObservableList<ScenarioDesc> data;

	private TableView<ScenarioDesc> table;

	private static MenuItem createMenuItem(String name, String keyCombination, EventHandler<ActionEvent> action) {
		MenuItem menuitem = new MenuItem(getText(name));
		menuitem.setAccelerator(KeyCombination.keyCombination(keyCombination));
		menuitem.setOnAction(action);
		return menuitem;
	}

	private TableView<ScenarioDesc> creatTable() {
		this.table = new TableView<>();
		// data = FXCollections.observableArrayList(new ScenarioDesc(new File("/media/revilloud/DATA1/Log/Sivic/log 4 cam�ra/RecFile_6_20120903_LivicImageSimulateur2_1_oImage.inf")), new
		// ScenarioDesc(new File(
		// "/media/revilloud/DATA1/Log/Sivic/log 4 cam�ra/RecFile_6_20120903_LivicImageSimulateur2_2_oImage.inf")),
		// new ScenarioDesc(new File("/media/revilloud/DATA1/Log/Sivic/log 4 cam�ra/RecFile_6_20120903_LivicImageSimulateur2_3_oImage.inf")), new ScenarioDesc(new File(
		// "/media/revilloud/DATA1/Log/Sivic/log 4 cam�ra/RecFile_6_20120903_LivicImageSimulateur2_4_oImage.inf")));
		final IntegerProperty dragFromIndex = new SimpleIntegerProperty(-1);
		final ArrayList<Integer> dragOriIndexs = new ArrayList<>();
		final ArrayList<Integer> dragCurrIndexs = new ArrayList<>();
		final ArrayList<ScenarioDesc> dragElements = new ArrayList<>();
		final IntegerProperty oldRowIndex = new SimpleIntegerProperty(-1);
		EventHandler<? super DragEvent> dragDone = ev -> {
			if (ev.getGestureSource() != null) {
				ev.acceptTransferModes(TransferMode.MOVE);
				ev.consume();
			}
		};
		this.table.setRowFactory(cb -> {
			TableRow<ScenarioDesc> row = new TableRow<>();
			row.setOnDragDetected(ev -> {
				dragOriIndexs.clear();
				dragElements.clear();
				dragCurrIndexs.clear();
				dragFromIndex.set(row.getIndex());
				ObservableList<Integer> test = this.table.getSelectionModel().getSelectedIndices();
				test.toString();
				dragOriIndexs.addAll(test); // Bug MEGA bizare..., sans toString, plante...
				StringBuilder sb = new StringBuilder();
				for (Integer id : dragOriIndexs) {
					ScenarioDesc ele = this.data.get(id.intValue());
					dragElements.add(ele);
					sb.append(ele.toString() + "\n");
				}
				dragCurrIndexs.addAll(dragOriIndexs);
				ClipboardContent content = new ClipboardContent();
				content.putString(sb.toString());
				row.startDragAndDrop(TransferMode.MOVE).setContent(content);
				oldRowIndex.set(row.getIndex());
				ev.consume();
			});
			row.setOnDragDropped(ev -> {
				Object gs = ev.getGestureSource();
				if (gs != null && gs instanceof TableRow<?> && ((TableRow<?>) gs).getTableView() == this.table) {
					dragFromIndex.set(-1);
					oldRowIndex.set(-1);
					ev.setDropCompleted(true);
					ev.consume();
				} else if (ev.getDragboard().hasFiles())
					dragFileDropped(ev, row.getIndex());
			});
			row.setOnDragOver(ev -> {
				if (ev.getGestureSource() != null) {
					ev.acceptTransferModes(TransferMode.MOVE);
					if (row.getIndex() != oldRowIndex.get()) {
						int id = 0;
						PlayListManager playlist = this.renderPane.getDataLoader().playListManager;
						for (Integer currId : dragCurrIndexs)
							playlist.remove(currId.intValue() - id++, false);
						// data.remove(currId.intValue() - id++);
						int base = row.getIndex() - dragFromIndex.get();
						int previousIndex = -1;
						for (int i = 0; i < dragElements.size(); i++) {
							int newIndex = base + dragOriIndexs.get(i);
							if (newIndex <= previousIndex)
								newIndex = previousIndex + 1;
							else if (newIndex >= playlist.size())
								newIndex = playlist.size();
							playlist.addToPlayList(newIndex, dragElements.get(i).scenarioSource);
							// data.add(newIndex, dragElements.get(i));
							dragCurrIndexs.set(i, newIndex);
							this.table.getSelectionModel().clearSelection();
							for (Integer integer : dragCurrIndexs)
								this.table.getSelectionModel().select(integer);
							previousIndex = newIndex;
							oldRowIndex.set(row.getIndex());
						}
					}
					ev.consume();
				} else if (ev.getDragboard().hasFiles())
					dragOverFile(ev);
			});
			row.setOnDragDone(dragDone);
			row.setOnMouseClicked(event -> {
				if (event.getClickCount() == 2 && !row.isEmpty())
					new Thread(() -> {
						try {
							this.renderPane.getDataLoader().reload(row.getItem().scenarioSource, false, true);
						} catch (Exception e) {
							e.printStackTrace();
							new Alert(AlertType.ERROR, "Cannot load the scenario : " + row.getItem().scenarioSource + "\n").showAndWait();
						}
					}).start();
			});
			return row;
		});

		this.table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		TableColumn<ScenarioDesc, ScenarioDesc> indexCol = new TableColumn<>("index");
		TableColumn<ScenarioDesc, ScenarioDesc> scenarioCol = new TableColumn<>("Scenario");
		TableColumn<ScenarioDesc, ScenarioDesc> durationCol = new TableColumn<>("Duration");
		indexCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		indexCol.setCellFactory(param -> new TableCell<>() {
			protected void updateItem(ScenarioDesc scenarioDesc, boolean empty) {
				super.updateItem(scenarioDesc, empty);
				TableRow<?> tr = getTableRow();
				if (scenarioDesc != null && tr != null) {
					setText(Integer.toString(getTableRow().getIndex()));
					setTextFill(!scenarioDesc.isAvailable ? Color.RED
							: scenarioDesc.scenarioSource == PlayList.this.renderPane.getDataLoader().getScenario().getSource() ? new Color(0, 0.80, 0, 1) : Color.BLACK);
				} else
					setText("");
			}
		});
		indexCol.setSortable(false);

		scenarioCol.setCellFactory(param -> new TableCell<>() {
			protected void updateItem(ScenarioDesc scenarioDesc, boolean empty) {
				super.updateItem(scenarioDesc, empty);
				if (scenarioDesc != null) {
					setText(scenarioDesc.scenarioSource.toString());
					setTextFill(!scenarioDesc.isAvailable ? Color.RED
							: scenarioDesc.scenarioSource == PlayList.this.renderPane.getDataLoader().getScenario().getSource() ? new Color(0, 0.80, 0, 1) : Color.BLACK);
				} else
					setText("");
			}
		});
		scenarioCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

		durationCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		durationCol.setCellFactory(param -> new TableCell<>() {
			protected void updateItem(ScenarioDesc scenarioDesc, boolean empty) {
				super.updateItem(scenarioDesc, empty);
				if (scenarioDesc != null) {
					int l = scenarioDesc.lenght;
					setText(l < 1 ? "-" : Integer.toString(l));
					setTextFill(!scenarioDesc.isAvailable ? Color.RED
							: scenarioDesc.scenarioSource == PlayList.this.renderPane.getDataLoader().getScenario().getSource() ? new Color(0, 0.80, 0, 1) : Color.BLACK);
				} else
					setText("");
			}
		});
		MenuItem removeItem = new MenuItem("Remove");
		removeItem.setOnAction(a -> removeSelection());
		MenuItem infoItem = new MenuItem("Info");
		infoItem.setOnAction(a -> ScenarioInformation.showInfo(this.stage, this.table.getSelectionModel().getSelectedItem().scenarioSource));
		this.table.setContextMenu(new ContextMenu(infoItem, new SeparatorMenuItem(), removeItem));
		this.table.setOnDragOver(ev -> {
			if (ev.getDragboard().hasFiles())
				dragOverFile(ev);
		});
		this.table.setOnDragDropped(ev -> {
			if (ev.getDragboard().hasFiles())
				dragFileDropped(ev, 0);
		});
		this.table.setOnKeyPressed(ev -> {
			if (ev.getCode() == KeyCode.DELETE)
				removeSelection();
		});

		this.table.setPlaceholder(new Label("Insert element here"));
		this.table.getColumns().add(indexCol);
		this.table.getColumns().add(scenarioCol);
		this.table.getColumns().add(durationCol);

		this.table.setItems(this.data);
		this.table.autosize();
		this.table.setPrefWidth(850);
		return this.table;
	}

	@Override
	public void dispose() {
		super.dispose();
		DataLoader dataloader = this.renderPane.getDataLoader();
		dataloader.playListManager.removePlayListChangeListener(this);
		this.renderPane.getDataLoader().removeOpenListener(this);
	}

	private void dragFileDropped(DragEvent ev, int index) {
		PlayListManager playList = this.renderPane.getDataLoader().playListManager;
		if (index < 0)
			index = 0;
		else if (index > playList.size())
			index = playList.size();
		LinkedHashSet<Object> toAdd = new LinkedHashSet<>();
		for (File file : ev.getDragboard().getFiles()) {
			File scenario = file;
			if (scenario.isDirectory())
				toAdd.add(scenario);
			else {
				int fileType = DataLoader.getFileType(scenario);
				if (fileType == DataLoader.UNSUPPORTED)
					continue;
				else if (fileType == DataLoader.SCENARIO) {
					scenario = ScenarioManager.getDescriptorFromScenario(scenario);
					if (scenario == null)
						continue;
				}
				toAdd.add(scenario);
			}
		}
		if (!toAdd.isEmpty())
			playList.addAll(index, toAdd);
		ev.setDropCompleted(true);
		ev.consume();
	}

	private static void dragOverFile(DragEvent ev) {
		for (File file : ev.getDragboard().getFiles())
			if (!file.isDirectory()) {
				int fileType = DataLoader.getFileType(file);
				if (fileType == DataLoader.UNSUPPORTED)
					return;
			}
		ev.acceptTransferModes(TransferMode.MOVE);
		ev.consume();
	}

	private MenuBar getMenuBar() {
		MenuBar menuBar = new MenuBar();
		menuBar.setPadding(new Insets(0));
		EventHandler<ActionEvent> action = e -> menuAction(((KeyCodeCombination) ((MenuItem) e.getSource()).acceleratorProperty().get()).getCode().getName());
		Menu menuFile = new Menu(getText("File"));
		ObservableList<MenuItem> items = menuFile.getItems();
		items.add(createMenuItem("New_Playlist", "Ctrl+N", action));
		items.add(createMenuItem("Open_Playlist", "Ctrl+O", action));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem("Add_Files", "Ctrl+L", action));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem("Save_Playlist", "Ctrl+S", action));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem("File_info", "Ctrl+D", action));
		Menu menuPlayList = new Menu(getText("Playlist"));
		items = menuPlayList.getItems();
		items.add(createMenuItem("Select_all", "Ctrl+A", action));
		items.add(createMenuItem("Select_none", "Ctrl+Z", action));
		items.add(createMenuItem("Invert_selection", "Ctrl+I", action));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem("Remove_selected", "Ctrl+Delete", action));
		items.add(createMenuItem("Clear_playlist", "Ctrl+C", action));
		items.add(createMenuItem("Remove_missing_files", "Ctrl+P", action));
		Menu menuSort = new Menu(getText("Sort"));
		items = menuSort.getItems();
		items.add(createMenuItem("Sort_by_name", "Ctrl+T", action));
		items.add(createMenuItem("Sort_by_path", "Ctrl+Q", action));
		items.add(new SeparatorMenuItem());
		items.add(createMenuItem("Reverse_list", "Ctrl+R", action));
		items.add(createMenuItem("Randomize_list", "Ctrl+H", action));
		menuBar.getMenus().addAll(menuFile, menuPlayList, menuSort);
		return menuBar;
	}

	@Override
	public VBox getRegion() {
		DataLoader dataLoader = this.renderPane.getDataLoader();
		PlayListManager playList = dataLoader.playListManager;
		playList.addPlayListChangeListener(this);
		this.renderPane.getDataLoader().addOpenListener(this);
		this.data = FXCollections.observableArrayList();
		playListChange();
		creatTable();
		VBox vBox = new VBox(getMenuBar(), this.table);
		return vBox;
	}

	private void menuAction(String keyCode) {
		DataLoader dataLoader = this.renderPane.getDataLoader();
		PlayListManager playList = dataLoader.playListManager;
		switch (keyCode) {
		case "N":
			playList.clear();
			break;
		case "O":
			File playListFile = PlayListFileChooserFx.fileChooser(this.stage, dataLoader, true, null);
			if (playListFile != null)
				try {
					playList.load(playListFile, -1);
				} catch (IOException e) {
					new Alert(AlertType.ERROR, "The playlit is corrupted File:+\n" + e.getMessage()).showAndWait();
				}
			break;
		case "L":
			List<File> scenarioFile = ScenarioFileChooserFx.showOpenMultipleDialog(this.stage, null);
			if (scenarioFile != null)
				playList.addAll(-1, new ArrayList<Object>(scenarioFile));
			break;
		case "S":
			playListFile = PlayListFileChooserFx.fileChooser(this.stage, dataLoader, false, null);
			if (playListFile != null)
				try {
					playList.save(playListFile);
				} catch (IOException e) {
					AlertUtil.show(e, "Cannot save the playlist: " + playListFile.getAbsolutePath(), true);
				}
			break;
		case "D":
			ScenarioDesc sd = this.table.getSelectionModel().getSelectedItem();
			if (sd != null)
				ScenarioInformation.showInfo(this.stage, sd.scenarioSource);
			else
				new Alert(AlertType.INFORMATION, "No selected file").showAndWait();
			break;
		case "A":
			this.table.getSelectionModel().selectAll();
			break;
		case "Z":
			this.table.getSelectionModel().clearSelection();
			break;
		case "I":
			TableViewSelectionModel<ScenarioDesc> sm = this.table.getSelectionModel();
			for (int i = 0; i < this.data.size(); i++)
				if (sm.isSelected(i))
					sm.clearSelection(i);
				else
					sm.select(i);
			break;
		case "Delete":
			removeSelection();
			break;
		case "C":
			playList.clear();
			break;
		case "P":
			playList.removeMissingFile();
			break;
		case "T":
			playList.sortByName();
			playListChange();
			break;
		case "Q":
			playList.sortByPath();
			break;
		case "R":
			playList.reverse();
			break;
		case "H":
			playList.shuffle();
			break;
		default:
			break;
		}
	}

	@Override
	public void opened(Scenario oldScenario, Scenario scenario) {
		this.table.refresh();
	}

	@Override
	public void playListChange() {
		this.data.clear();
		for (Object scenarioSource : this.renderPane.getDataLoader().playListManager.getPlayList())
			this.data.add(new ScenarioDesc(scenarioSource));
	}

	private void removeSelection() {
		ArrayList<Object> toRemove = new ArrayList<>();
		for (ScenarioDesc scenarioDesc : this.table.getSelectionModel().getSelectedItems())
			toRemove.add(scenarioDesc.scenarioSource);
		this.renderPane.getDataLoader().playListManager.removeAll(toRemove);
		this.table.getSelectionModel().clearSelection();
	}
}
