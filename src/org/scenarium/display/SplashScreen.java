/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SplashScreen {
	private final Stage splashStage;
	private final ProgressBar progressBar;

	public SplashScreen(Stage splashStage, ReadOnlyDoubleProperty progressProperty) {
		this.splashStage = splashStage;
		this.progressBar = new ProgressBar();
		this.progressBar.progressProperty().bind(progressProperty);
		Scene splashScene = new Scene(new VBox(new ImageView(new Image(getClass().getResourceAsStream("/scenarium.png"))), this.progressBar));
		splashStage.setScene(splashScene);
		splashStage.initStyle(StageStyle.UNDECORATED);
		splashStage.show();
	}

	public void close() {
		this.splashStage.close();
	}
}
